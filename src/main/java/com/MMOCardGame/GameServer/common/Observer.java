package com.MMOCardGame.GameServer.common;

public interface Observer<E> {
    void update(E object);
}
