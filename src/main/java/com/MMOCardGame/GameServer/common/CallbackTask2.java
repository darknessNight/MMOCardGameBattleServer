package com.MMOCardGame.GameServer.common;

import java.io.Serializable;

public interface CallbackTask2<T, U> extends Serializable {
    void run(T obj1, U obj2);
}
