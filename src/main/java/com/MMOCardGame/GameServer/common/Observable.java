package com.MMOCardGame.GameServer.common;

public interface Observable<E> {
    void addObserver(Observer<E> observer);

    void removeObserver(Observer<E> observer);

    int countObservers();

    void addObserverAtBegin(Observer<E> gamePhaseChangedObserver);
}
