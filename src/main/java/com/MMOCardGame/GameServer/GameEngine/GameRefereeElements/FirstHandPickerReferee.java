package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GamePhaseChanged;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateTools;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardChangeType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Observer;

import java.util.Collection;
import java.util.List;

public class FirstHandPickerReferee extends FirstHandChangeReferee {
    Observer<GamePhaseChanged> phaseChangeObserver;

    public FirstHandPickerReferee(GameState gameState, Collection<UserConnection> connections) {
        super(gameState, connections);
        phaseChangeObserver = this::phaseEnded;
        gameState.getPhasesStack().addObserver(phaseChangeObserver);
    }

    private void phaseEnded(GamePhaseChanged gamePhaseChanged) {
        if (gamePhaseChanged.getCurrent().getFirst() == GamePhase.FirstHandDraw) {
            drawFirstHandForAllPlayers();
            gameState.getPhasesStack().removeObserver(phaseChangeObserver);
        }
    }

    private void drawFirstHandForAllPlayers() {
        gameState.getListOfActiveUsersIds().forEach(this::randomCardsForUser);
    }

    private void randomCardsForUser(int player) {
        List<Card> cardsOfPlayer = GameStateTools.getPlayerCardsFromLibrary(player, gameState);
        List<Integer> selectedCards = randomCardsPicker.getRandomCardsWithMinLinks(cardsOfPlayer, GameConstants.getStartingHandSize());
        moveCardsToHand(selectedCards);
        sendRandomCardForPlayer(player, selectedCards);
    }

    private void sendRandomCardForPlayer(int player, List<Integer> selectedCards) {
        HandChanged messageToPlayer = getFullHandChangedMessage(selectedCards, player);
        HandChanged messageToOthers = getNotificationHandChangedMessage(player, selectedCards, CardChangeType.Draw);
        sendMessageToPlayerAndOthers(player, messageToPlayer, messageToOthers);
    }

}
