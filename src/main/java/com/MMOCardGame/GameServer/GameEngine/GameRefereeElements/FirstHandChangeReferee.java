package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateTools;
import com.MMOCardGame.GameServer.GameEngine.ProtoMessagesHelper;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardChangeType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Servers.UserConnection;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class FirstHandChangeReferee extends GameRefereeNotifier {
    protected transient RandomCardsPicker randomCardsPicker;
    protected List<Integer> selectedCards;
    protected List<Integer> removedCards;
    protected int player;

    public FirstHandChangeReferee(GameState gameState, Collection<UserConnection> connections) {
        super(gameState, connections);
        randomCardsPicker = new RandomCardsPicker();
    }

    public FirstHandChangeReferee(GameState gameState, Map<Integer, UserConnection> connections, RandomCardsPicker randomCardsPicker) {
        super(gameState, connections);
        this.randomCardsPicker = randomCardsPicker;
    }

    public void setRandomCardsPicker(RandomCardsPicker randomCardsPicker) {
        this.randomCardsPicker = randomCardsPicker;
    }

    protected void moveCardsToHand(List<Integer> selectedCards) {
        selectedCards.forEach(gameState::moveCardToHand);
    }

    protected List<Card> moveAllCardsToLibrary(int player) {
        List<Card> cardsInHand = GameStateTools.getPlayerCardsFromPosition(player, gameState, CardPosition.Position.Hand);
        cardsInHand.forEach(card -> gameState.moveCardToLibrary(card.getInstanceId()));
        return cardsInHand;
    }

    protected HandChanged getFullHandChangedMessage(List<Integer> selectedCards, int player) {
        return getFullHandChangedMessage(selectedCards, player, CardChangeType.Draw);
    }

    private HandChanged getFullHandChangedMessage(List<Integer> selectedCards, int player, CardChangeType type) {
        return ProtoMessagesHelper.getHandChangedDrawFromCards(selectedCards, player, type);
    }

    protected void moveCardsToLibrary(List<Integer> cardInstancesList) {
        cardInstancesList.forEach(gameState::moveCardToLibrary);
    }

    public void acceptPlayerHand(HandChanged handChange) {
        gameState.getPlayer(handChange.getPlayer()).acceptHand();
        gameState.endPhase(handChange.getPlayer());
    }

    public void notifyOthers() {
        sendInfoAboutRemove();
        sendInfoAboutDraw();
    }

    protected void sendInfoAboutDraw() {
        HandChanged messageToOthers = getNotificationHandChangedMessage(player, selectedCards, CardChangeType.Draw);
        HandChanged messageToPlayer = getFullHandChangedMessage(selectedCards, player, CardChangeType.Draw);
        sendMessageToPlayer(player, messageToPlayer);
        sendMessageToAllPlayersExcept(player, messageToOthers);
    }

    protected HandChanged getNotificationHandChangedMessage(int player, List<Integer> selectedCards, CardChangeType type) {
        return ProtoMessagesHelper.getHandChangedDrawWithSize(selectedCards.size(), player, type);
    }

    protected void sendInfoAboutRemove() {
        HandChanged messageToOthers = getNotificationHandChangedMessage(player, removedCards, CardChangeType.Remove);
        HandChanged messageToPlayer = getFullHandChangedMessage(removedCards, player, CardChangeType.Remove);
        sendMessageToPlayer(player, messageToPlayer);
        sendMessageToAllPlayersExcept(player, messageToOthers);

    }

    public HandChanged getHandChangedMessage() {
        return getFullHandChangedMessage(selectedCards, player);
    }

    protected List<Integer> moveNewCardsToHandAndGetThem(int count) {
        List<Card> cardsOfPlayer = GameStateTools.getPlayerCardsFromLibrary(player, gameState);
        List<Integer> selectedCards = randomCardsPicker.getRandomCards(cardsOfPlayer, count);
        moveCardsToHand(selectedCards);
        return selectedCards;
    }
}
