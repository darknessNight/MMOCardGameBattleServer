package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.ReactionStackElement;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ReactionStackController {
    private final Collection<UserConnection> connections;
    private final GameState gameState;
    private final Logger logger = LoggerFactory.getLogger(ReactionStackController.class);

    public ReactionStackController(GameState gameState, Collection<UserConnection> connections) {
        this.gameState = gameState;
        this.connections = connections;
    }

    public void doAction() {
        ReactionStackElement element = gameState.getReactionStack().getNextAction();
        if (element != null) {
            List<Integer> canceledActions = new ArrayList<>();
            while (element.isCanceled()) {
                gameState.getReactionStack().doAction();
                canceledActions.add(element.getId());
                element = gameState.getReactionStack().getNextAction();
            }
            StackActionCompleted stackAction = StackActionCompleted.newBuilder().setStackId(element.getId()).addAllDeletedStackActions(canceledActions).build();
            sendMessageToAllConnections(stackAction);
            gameState.getReactionStack().doAction();
        }
    }

    private <U> void sendMessageToAllConnections(U message) {
        connections.forEach(userConnection -> userConnection.sendMessage(message));
    }

    private <U> void sendMessageToAllConnectionsBut(int player, U message) {
        connections.forEach(userConnection -> {
            if (userConnection.getUserId() != player)
                userConnection.sendMessage(message);
        });
    }

    public CardUsed pushCardAction(Card card, int player, CardCosts cardCosts) {
        return pushCardActionWithDecisionAndTargets(card, player, -1, cardCosts);
    }

    public CardUsed pushCardActionWithTargets(Card card, int player, CardCosts cardCosts) {
        return pushCardActionWithDecisionAndTargets(card, player, -1, cardCosts);
    }

    public CardUsed pushCardActionWithDecision(Card card, int player, int decisionId, CardCosts cardCosts) {
        return pushCardActionWithDecisionAndTargets(card, player, decisionId, cardCosts);
    }

    public CardUsed pushCardActionWithDecisionAndTargets(Card card, int player, int decisionId, CardCosts cardCosts) {
        CardPosition position = gameState.getCardPosition(card.getInstanceId());
        CardAbilityTask task = getCardAbilityTasks(card, decisionId, position);
        if(task==null)
            return CardUsed.newBuilder().setStackId(-1).build();
        var result = pushTaskToReactionStackAndNotifyPlayers(card, player, cardCosts, task);
        gameState.getPhasesStack().startReactionPhases();
        return result;
    }

    private CardUsed pushTaskToReactionStackAndNotifyPlayers(Card card, int player, CardCosts cardCosts, CardAbilityTask task) {
        ReactionStackElement element = gameState.getReactionStack().pushAction(() ->{
            try{task.run(gameState);}catch(Exception e){ logger.error("Error in standardAction", e);}
            }, () -> {
            try{task.runCanceled(gameState);}catch(Exception e){ logger.error("Error in cancelAction", e);}
        }, player);
        return notifyPlayersAboutNewCardActionReactionStackElement(card, player, cardCosts, task, element);
    }

    private CardUsed notifyPlayersAboutNewCardActionReactionStackElement(Card card, int player, CardCosts cardCosts, CardAbilityTask task, ReactionStackElement element) {
        CardUsed message = CardUsed.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setStackId(element.getId())
                .setCardAbilityId(task.getAbilityId())
                .setCardCosts(cardCosts)
                .addAllTransactionSelects(card.getCardScript().getSelectedOptionsDuringTransaction(task.getAbilityId()))
                .build();
        sendMessageToAllConnectionsBut(player, message);
        return message;
    }

    private CardAbilityTask getCardAbilityTasks(Card card, int decisionId, CardPosition position) {
        if (position.getPosition() == CardPosition.Position.Hand)
            return card.getCardScript().playAction(gameState, decisionId);
        else
            return card.getCardScript().abilityAction(gameState, decisionId);
    }

    public void pushTriggerAction(CardAbilityTask task, Card card, TriggerType triggerType) {
        gameState.getPhasesStack().startReactionPhasesInsteadTriggerPhase(triggerType, card.getInstanceId());
        ReactionStackElement element = gameState.getReactionStack().pushAction(() -> task.run(gameState), () -> task.runCanceled(gameState), card.getInstanceId() * 100 + triggerType.ordinal());
        notifyPlayersAboutNewTriggerReactionStackElement(card, element, triggerType, new ArrayList<>());
    }

    public void pushTriggerAction(CardAbilityTask task, Card card, TriggerType triggerType, Collection<TargetSelectingTransaction> transactions) {
        gameState.getPhasesStack().startReactionPhasesInsteadTriggerPhase(triggerType, card.getInstanceId());
        ReactionStackElement element = gameState.getReactionStack().pushAction(() -> task.run(gameState), () -> task.runCanceled(gameState), card.getInstanceId() * 100 + triggerType.ordinal());
        notifyPlayersAboutNewTriggerReactionStackElement(card, element, triggerType, transactions);
    }

    private void notifyPlayersAboutNewTriggerReactionStackElement(Card card, ReactionStackElement element, TriggerType type, Collection<TargetSelectingTransaction> transactions) {
        TriggerActivated message = TriggerActivated.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setStackId(element.getId())
                .setTriggerTypeValue(type.ordinal())
                .addAllTransaction(transactions)
                .build();
        sendMessageToAllConnections(message);
    }
}

