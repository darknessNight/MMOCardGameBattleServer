package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public class RandomCardsPicker {
    Random random = ThreadLocalRandom.current();

    public List<Integer> getRandomCardsWithMinLinks(List<Card> cardsOfPlayer, int cardsCount) {
        Set<Integer> result = new HashSet<>();
        int linksNumber = getLinksNumber(cardsOfPlayer);
        cardsCount = min(cardsCount, cardsOfPlayer.size());
        fillWithLinks(cardsOfPlayer, result, linksNumber);
        fillWithRandomCards(cardsOfPlayer, cardsCount, result);
        return new ArrayList<>(result);
    }

    private void fillWithLinks(List<Card> cardsOfPlayer, Set<Integer> result, int linksNumber) {
        List<Integer> links = cardsOfPlayer.stream().filter(card -> card.getType() == CardType.Link)
                .map(Card::getInstanceId)
                .collect(Collectors.toList());
        while (result.size() < linksNumber) {
            int card = links.get(random.nextInt(links.size()));
            result.add(card);
        }
    }

    private void fillWithRandomCards(List<Card> cardsOfPlayer, int cardsCount, Set<Integer> result) {
        while (result.size() < cardsCount) {
            int card = getRandomCard(cardsOfPlayer);
            result.add(card);
        }
    }

    private int getLinksNumber(Collection<Card> cardsOfPlayer) {
        int minVal = min(cardsOfPlayer.size(), GameConstants.getMinLinksInFirstHand());
        return random.nextInt(min(cardsOfPlayer.size(), GameConstants.getMaxLinksInFirstHand()) - minVal)
                + minVal;
    }

    private int min(int a1, int a2) {
        return a1 < a2 ? a1 : a2;
    }

    public List<Integer> getRandomCards(List<Card> cardsOfPlayer, int cardsCount) {
        Set<Integer> result = new HashSet<>();
        cardsCount = min(cardsCount, cardsOfPlayer.size());
        fillWithRandomCards(cardsOfPlayer, cardsCount, result);
        return new ArrayList<>(result);
    }

    public int getRandomCard(List<Card> cardsOfPlayer) {
        return cardsOfPlayer.get(random.nextInt(cardsOfPlayer.size())).getInstanceId();
    }
}
