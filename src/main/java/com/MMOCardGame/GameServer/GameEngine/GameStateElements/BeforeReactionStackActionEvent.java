package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import lombok.Data;

@Data
public class BeforeReactionStackActionEvent {
    boolean canceled = false;
    ReactionStackElement stackElement = null;

    public BeforeReactionStackActionEvent(ReactionStackElement stackElement) {
        this.stackElement = stackElement;
    }
}
