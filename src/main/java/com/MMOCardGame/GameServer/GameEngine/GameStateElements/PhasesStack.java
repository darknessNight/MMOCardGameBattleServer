package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GamePhaseChanged;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.Observer;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@EqualsAndHashCode(exclude = {"phaseChangedObservable"})
@ToString
public class PhasesStack implements Serializable {
    private Map<Integer, Integer> orderPlayersMap = new HashMap<>();
    private Map<Integer, Integer> playersOrderMap = new HashMap<>();
    private Stack<Pair<GamePhase, Integer>> phasesStack = new Stack<>();
    private List<Integer> playersOrder;
    private transient ObservableNotifier<GamePhaseChanged> phaseChangedObservable = new ObservableNotifier<>();
    @Setter
    private boolean stopNotifying = false;
    private int cardIdMultiplier = 100;
    @Getter
    private Integer roundPlayer = -1;

    public PhasesStack(List<Integer> playersOrder) {
        saveToCachePlayersOrder(playersOrder);
        playersOrder.forEach((playerId) -> phasesStack.push(new Pair<>(GamePhase.Waiting, playerId)));
    }

    public PhasesStack(List<Integer> playersOrder, GamePhase phase, int playerId) {
        saveToCachePlayersOrder(playersOrder);
        phasesStack.push(new Pair<>(phase, playerId));
    }

    public PhasesStack(List<Integer> playersOrder, List<Pair<GamePhase, Integer>> statesStack) {
        saveToCachePlayersOrder(playersOrder);
        statesStack.forEach(phasesStack::push);
    }

    private void saveToCachePlayersOrder(List<Integer> playersOrder) {
        orderPlayersMap=new ConcurrentHashMap<>();
        playersOrderMap=new ConcurrentHashMap<>();
        for (int i = 0; i < playersOrder.size(); i++) {
            orderPlayersMap.put(i, playersOrder.get(i));
            playersOrderMap.put(playersOrder.get(i), i);
        }
        this.playersOrder=new ArrayList<>(playersOrder);
    }

    public GamePhase getCurrentPhase() {
        return phasesStack.peek().getFirst();
    }

    public int getCurrentPlayer() {
        if (getCurrentPhase() == GamePhase.Waiting || getCurrentPhase() == GamePhase.FirstHandDraw)
            return -1;
        return phasesStack.peek().getSecond();
    }

    public void addObserver(Observer<GamePhaseChanged> gamePhaseChangedObserver) {
        phaseChangedObservable.addObserver(gamePhaseChangedObserver);
    }

    public void addObserverAtBegin(Observer<GamePhaseChanged> gamePhaseChangedObserver) {
        phaseChangedObservable.addObserverAtBegin(gamePhaseChangedObserver);
    }

    public void removeObserver(Observer<GamePhaseChanged> gamePhaseChangedObserver) {
        phaseChangedObservable.removeObserver(gamePhaseChangedObserver);
    }

    public void endPhase(int playerId) {
        if (!isTechnicalPhase())
            endCurrentPhase(playerId);
    }

    private void endCurrentPhase(int playerId) {
        Pair<GamePhase, Integer> previous = phasesStack.peek();
        changePhaseIfNeeded(playerId);
        informObserversAboutPhaseChange(previous);
    }

    private void informObserversAboutPhaseChange(Pair<GamePhase, Integer> previous) {
        if ((previous.getFirst() != phasesStack.peek().getFirst()
                || (!previous.getSecond().equals(phasesStack.peek().getSecond())
                && previous.getFirst() != GamePhase.Waiting && previous.getFirst() != GamePhase.FirstHandDraw))
                && !stopNotifying) {
            GamePhaseChanged tmp = new GamePhaseChanged(previous, phasesStack.peek());
            phaseChangedObservable.notifyAll(tmp);
        }
    }

    private void changePhaseIfNeeded(int playerId) {
        switch (getCurrentPhase()) {
            case FirstHandDraw:
            case Waiting:
                endParallelPhase(playerId);
                break;
            default:
                endStandardPhase(playerId);
                break;
        }
    }

    private void endStandardPhase(int playerId) {
        if (playerId != getCurrentPlayer())
            return;
        Pair<GamePhase, Integer> previousPhase = phasesStack.pop();
        if (phasesStack.size() == 0)
            startNextState(previousPhase);
    }

    private void endParallelPhase(int playerId) {
        Pair<GamePhase, Integer> previousPhase = phasesStack.peek();
        phasesStack.removeIf(gamePhaseIntegerPair -> gamePhaseIntegerPair.getSecond() == playerId);
        if (phasesStack.size() == 0)
            startNextState(previousPhase);
    }

    private void startNextState(Pair<GamePhase, Integer> previousPhase) {
        switch (previousPhase.getFirst()) {
            case Waiting:
                orderPlayersMap.forEach((order, playerId) -> phasesStack.push(new Pair<>(GamePhase.FirstHandDraw, playerId)));
                break;
            case FirstHandDraw:
                phasesStack.push(new Pair<>(GamePhase.Untap, orderPlayersMap.get(0)));
                break;
            case Attack:
                addAllPhasesFromAttackToNextPlayers(previousPhase);
                break;
            default:
                startNextSimpleState(previousPhase);
                break;
        }
        setRoundPlayer();
    }

    private void setRoundPlayer() {
        if (phasesStack.peek().getFirst() == GamePhase.Main)
            roundPlayer = phasesStack.peek().getSecond();
    }

    private void addAllPhasesFromAttackToNextPlayers(Pair<GamePhase, Integer> previousPhase) {
        phasesStack.push(new Pair<>(GamePhase.Untap, orderPlayersMap.get(getNextPlayerOrder(previousPhase.getSecond()))));
        startAllButCurrentPlayersPhase(GamePhase.EndRound, previousPhase.getSecond());
        phasesStack.push(new Pair<>(GamePhase.EndRound, previousPhase.getSecond()));
        phasesStack.push(new Pair<>(GamePhase.AttackExecution, previousPhase.getSecond()));
        startAllButCurrentPlayersPhase(GamePhase.ReactionPhase, previousPhase.getSecond());
        phasesStack.push(new Pair<>(GamePhase.ReactionPhase, previousPhase.getSecond()));
        startAllButCurrentPlayersPhase(GamePhase.Defence, previousPhase.getSecond());
    }

    private void startAllButCurrentPlayersPhase(GamePhase phase, Integer firstPlayer) {
        int playerOrder = playersOrderMap.get(firstPlayer);
        int playersCount = playersOrderMap.size();
        for (int i = playersCount - 1; i >= 1; i--) {
            int playerId = orderPlayersMap.get((playerOrder + i) % playersCount);
            phasesStack.push(new Pair<>(phase, playerId));
        }
    }

    private void startNextSimpleState(Pair<GamePhase, Integer> previousPhase) {
        int nextPlayer = previousPhase.getSecond();
        if (previousPhase.getFirst() == GamePhase.EndRound) {
            nextPlayer = orderPlayersMap.get(getNextPlayerOrder(previousPhase.getSecond()));
        }
        int nextPhaseValue = (previousPhase.getFirst().ordinal() - GamePhase.PlayerRoundPhasesStart.ordinal())
                % (GamePhase.PlayerRoundPhasesEnd.ordinal() - GamePhase.PlayerRoundPhasesStart.ordinal() - 1)
                + GamePhase.PlayerRoundPhasesStart.ordinal() + 1;
        phasesStack.push(new Pair<>(GamePhase.values()[nextPhaseValue], nextPlayer));
    }

    private int getNextPlayerOrder(int previousPlayer) {
        return (playersOrderMap.get(previousPlayer) + 1) % playersOrderMap.size();
    }

    public void startReactionPhasesWithStartingPlayer(int i) {
        Pair<GamePhase, Integer> previous = phasesStack.peek();
        startReactionPhasesWithStartingPlayerWithoutNotify(i);
        informObserversAboutPhaseChange(previous);
    }

    public void startReactionPhasesWithStartingPlayerWithoutNotify(int i) {
        startAllButCurrentPlayersPhase(GamePhase.ReactionPhase, i);
        phasesStack.push(new Pair<>(GamePhase.ReactionPhase, i));
    }

    public void startReactionPhases() {
        Pair<GamePhase, Integer> previous = startReactionPhasesWithoutNotify();
        informObserversAboutPhaseChange(previous);
    }

    public Pair<GamePhase, Integer> startReactionPhasesWithoutNotify() {
        Pair<GamePhase, Integer> previous = phasesStack.peek();
        if (getCurrentPhase() != GamePhase.ReactionPhase) {
            addNewReactionsQueue();
        } else
            addReactionsForReaction();
        return previous;
    }

    private void addNewReactionsQueue() {
        int player = getLivePlayer();
        phasesStack.push(new Pair<>(GamePhase.ReactionPhase, player));
        startAllButCurrentPlayersPhase(GamePhase.ReactionPhase, player);
    }

    private int getLivePlayer() {
        int player = getCurrentPlayer();
        if (player > cardIdMultiplier) {
            for (int i = phasesStack.size() - 1; i >= 0; i--) {
                if (phasesStack.get(i).getSecond() < cardIdMultiplier) {
                    player = phasesStack.get(i).getSecond();
                    break;
                }
            }
        }
        return player;
    }

    private void addReactionsForReaction() {
        int currentPlayer = getCurrentPlayer();
        while (getCurrentPhase() == GamePhase.ReactionPhase)
            phasesStack.pop();
        int playerOrder = playersOrderMap.get(currentPlayer);
        int playersCount = playersOrderMap.size();
        for (int i = playersCount - 1; i >= 0; i--) {
            int playerId = orderPlayersMap.get((playerOrder + i) % playersCount);
            phasesStack.push(new Pair<>(GamePhase.ReactionPhase, playerId));
        }
    }

    public void startTriggerPhase(TriggerType type, int cardId) {
        var previous = phasesStack.peek();
        phasesStack.push(new Pair<>(GamePhase.TriggerPhase, getCodedCardTrigger(type, cardId)));
        informObserversAboutPhaseChange(previous);
    }

    private int getCodedCardTrigger(TriggerType type, int cardId) {
        return cardId * cardIdMultiplier + type.ordinal();
    }

    public int getDecodedCardId() {
        if (phasesStack.peek().getSecond() < cardIdMultiplier)
            return -1;
        else return phasesStack.peek().getSecond() / cardIdMultiplier;
    }

    public TriggerType getDecodedTriggerType() {
        if (phasesStack.peek().getSecond() < cardIdMultiplier || phasesStack.peek().getSecond() % cardIdMultiplier >= TriggerType.values().length)
            return TriggerType.UndefinedTriggerType;
        else return TriggerType.values()[phasesStack.peek().getSecond() % cardIdMultiplier];
    }

    public void endTriggerPhase(TriggerType type, int cardId) {
        endPhase(getCodedCardTrigger(type, cardId));
    }

    public void startReactionPhasesInsteadTriggerPhase(TriggerType triggerType, int cardId) {
        endTriggerPhase(triggerType, cardId);
        startReactionPhasesWithStartingPlayer(getLivePlayer());
    }

    public void endTechnicalPhase() {
        if (isTechnicalPhase())
            endCurrentPhase(phasesStack.peek().getSecond());
    }

    private boolean isTechnicalPhase() {
        return getCurrentPhase().equals(GamePhase.AttackExecution);
    }

    public void removePlayer(int player){
        phasesStack.removeIf(gamePhaseIntegerPair -> gamePhaseIntegerPair.getSecond()==player);
        playersOrder.removeIf(integer -> integer==player);
        saveToCachePlayersOrder(playersOrder);
    }

}
