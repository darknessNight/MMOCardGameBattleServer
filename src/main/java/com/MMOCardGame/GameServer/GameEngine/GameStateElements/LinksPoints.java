package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader.CardInfo.Costs;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerLinkPointsChangedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"triggers", "owner"})
public class LinksPoints implements Serializable {
    int neutral = 0;
    int raiders = 0;
    int scanners = 0;
    int psycho = 0;
    int whiteHats = 0;
    int owner = -1;
    transient Triggers triggers = null;

    public LinksPoints(int owner) {
        this.owner = owner;
    }

    public LinksPoints(int neutral, int raiders, int scanners, int psycho, int whiteHats) {
        this.neutral = neutral;
        this.raiders = raiders;
        this.scanners = scanners;
        this.psycho = psycho;
        this.whiteHats = whiteHats;
    }

    public LinksPoints(LinksPoints linksPoints) {
        neutral = linksPoints.neutral;
        raiders = linksPoints.raiders;
        scanners = linksPoints.scanners;
        psycho = linksPoints.psycho;
        whiteHats = linksPoints.whiteHats;
    }

    public boolean removePointsIfPossible(LinksPoints points) {
        Costs previousPoints = getCostsFromPoints();

        if (neutral >= points.neutral && raiders >= points.raiders && scanners >= points.scanners
                && psycho >= points.psycho && whiteHats >= points.whiteHats) {
            neutral -= points.neutral;
            raiders -= points.raiders;
            scanners -= points.scanners;
            psycho -= points.psycho;
            whiteHats -= points.whiteHats;
            invokeTrigger(previousPoints);
            return true;
        }
        if (raiders >= points.raiders && scanners >= points.scanners
                && psycho >= points.psycho && whiteHats >= points.whiteHats
                && (raiders + scanners + psycho + whiteHats) >= points.neutral) {
            raiders -= points.raiders;
            scanners -= points.scanners;
            psycho -= points.psycho;
            whiteHats -= points.whiteHats;


            int tmp = raiders;
            raiders -= Math.min(tmp, points.neutral);
            points.neutral -= Math.min(tmp, points.neutral);

            tmp = scanners;
            scanners -= Math.min(tmp, points.neutral);
            points.neutral -= Math.min(tmp, points.neutral);

            tmp = psycho;
            psycho -= Math.min(tmp, points.neutral);
            points.neutral -= Math.min(tmp, points.neutral);

            tmp = whiteHats;
            whiteHats -= Math.min(tmp, points.neutral);
            points.neutral -= Math.min(tmp, points.neutral);
            invokeTrigger(previousPoints);
            return true;
        }
        return false;
    }

    private void invokeTrigger(Costs previousPoints) {
        if (triggers != null)
            triggers.invokeTrigger(new PlayerLinkPointsChangedEvent(owner, previousPoints, getCostsFromPoints()));
    }

    private Costs getCostsFromPoints() {
        Costs costs = new Costs();
        costs.setNeutral(neutral);
        costs.setPsychotronnics(psycho);
        costs.setRaiders(raiders);
        costs.setScanners(scanners);
        costs.setWhiteHats(whiteHats);
        return costs;
    }

    public void addPoints(LinksPoints points) {
        Costs previousPoints = getCostsFromPoints();
        neutral += points.neutral;
        raiders += points.raiders;
        scanners += points.scanners;
        psycho += points.psycho;
        whiteHats += points.whiteHats;
        invokeTrigger(previousPoints);
    }

    public LinksPoints getDifference(LinksPoints linksPoints) {
        LinksPoints results = new LinksPoints();
        results.neutral = linksPoints.neutral - neutral;
        results.raiders = linksPoints.raiders - raiders;
        results.whiteHats = linksPoints.whiteHats - whiteHats;
        results.psycho = linksPoints.psycho - psycho;
        results.scanners = linksPoints.scanners - scanners;
        return results;
    }

    public void reset() {
        Costs previousPoints = getCostsFromPoints();
        neutral = 0;
        raiders = 0;
        whiteHats = 0;
        psycho = 0;
        scanners = 0;
        invokeTrigger(previousPoints);
    }
}
