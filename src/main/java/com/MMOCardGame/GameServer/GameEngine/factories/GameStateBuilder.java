package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardFactory;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;

import java.util.*;

public class GameStateBuilder {
    private GameSessionSettings gameSessionSettings;
    private CardFactory cardFactory;
    private PlayerFactory heroFactory;
    private Random randomGenerator;

    public GameStateBuilder setCardFactory(CardFactory cardFactory) {
        this.cardFactory = cardFactory;
        return this;
    }

    public GameStateBuilder setRandomGenerator(Random randomGenerator) {
        this.randomGenerator = randomGenerator;
        return this;
    }

    public GameStateBuilder setGameSessionSettings(GameSessionSettings gameSessionSettings) {
        this.gameSessionSettings = gameSessionSettings;
        return this;
    }

    public GameStateBuilder setPlayerFactory(PlayerFactory heroFactory) {
        this.heroFactory = heroFactory;
        return this;
    }

    public GameState build() {
        if (cardFactory == null || heroFactory == null || gameSessionSettings == null || randomGenerator == null)
            throw new NullPointerException();

        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(gameSessionSettings.getListOfUsers());
        gameState = initCardsAndHeros(gameState);
        gameState.setPlayersOrder(getRandomPlayerOrder());

        return gameState;
    }

    private GameState initCardsAndHeros(GameState gameState) {
        for (String username : gameSessionSettings.getListOfUsers()) {
            int playerId = gameState.getPlayerId(username);
            Collection<Card> cards = cardFactory.createListOfCards(gameSessionSettings.getUsersCards().get(username));
            Player player = heroFactory.createHero(gameSessionSettings.getSelectedHeros().get(username), playerId);
            gameState.setCardsForUser(playerId, cards);
            gameState.setHeroForUser(playerId, player);
        }
        return gameState;
    }

    private int[] getRandomPlayerOrder() {
        List<Integer> list = getRandomPlayerOrderList();
        return changeListToArray(list);
    }

    private int[] changeListToArray(List<Integer> list) {
        int[] playerOrder = new int[gameSessionSettings.getListOfUsers().size()];
        for (int i = 0; i < gameSessionSettings.getListOfUsers().size(); i++)
            playerOrder[i] = list.get(i);
        return playerOrder;
    }

    private List<Integer> getRandomPlayerOrderList() {
        List<Integer> list = new ArrayList<>(gameSessionSettings.getListOfUsers().size());
        for (int i = 0; i < gameSessionSettings.getListOfUsers().size(); i++)
            list.add(i);
        Collections.shuffle(list, randomGenerator);
        return list;
    }
}
