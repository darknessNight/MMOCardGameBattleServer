package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@AllArgsConstructor
public class PlayerFactory {
    private Map<Integer, PlayerInfo> playersInfo;

    public Player createHero(int heroId, int userId) {
        if (!playersInfo.containsKey(heroId))
            throw new NotFoundException();
        PlayerInfo info = playersInfo.get(heroId);
        return new Player(heroId, userId, GameConstants.getStartingLife(), info.getAbilitiesCooldown(), info.getAbilitiesCooldownChangeOnUse(), info.getCosts());
    }

    @Data
    @AllArgsConstructor
    public static class PlayerInfo {
        private final int id;
        @SerializedName("startingCooldown")
        private final int[] abilitiesCooldown;
        @SerializedName("cooldown")
        private final int[] abilitiesCooldownChangeOnUse;
        @SerializedName("abilityCosts")
        private final int[] costs;
    }
}
