package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardFactoryBuilder;
import dagger.Component;

@Component(modules = CardFactoryBuilderProvider.class)
public interface CardFactoryFactory {
    CardFactoryBuilder getCardFactoryBuilder();
}

