package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.HeroInfoReader;
import dagger.Module;
import dagger.Provides;

import java.io.IOException;
import java.util.Map;

@Module(includes = HeroInfoReaderProvider.class)
public class PlayerFactoryProvider {
    @Provides
    PlayerFactory getPlayerFactory(HeroInfoReader heroInfoReader, AppConfiguration configuration) {
        Map<Integer, PlayerFactory.PlayerInfo> infoMap = null;
        try {
            infoMap = heroInfoReader.readFromFile(configuration.getHeroInfoPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new PlayerFactory(infoMap);
    }
}
