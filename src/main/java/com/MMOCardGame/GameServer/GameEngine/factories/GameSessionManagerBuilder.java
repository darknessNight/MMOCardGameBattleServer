package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.common.Observer;

public interface GameSessionManagerBuilder {
    GameSessionManagerBuilder setUserConnectionsManager(UserConnectionsManager userConnectionsManager);

    GameSessionManagerBuilder setGameSessionFactory(GameSessionFactory gameSessionFactory);

    GameSessionManagerBuilder setInfoObserver(Observer<UserSessionEndInfo> infoObserver);

    GameSessionManager build();
}

