package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.StatisticsChange;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardRevealedEvent;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;

import java.util.List;

public abstract class ProtoMessagesHelper {
    public static CardRevealed getCardRevealedMessageFromEvent(CardRevealedEvent event) {
        return getCardRevealedMessageFromCardAndPosition(event.getCard(), event.getPosition());
    }

    public static CardRevealed getCardRevealedMessageFromCardAndPosition(Card card, CardPosition position) {
        return CardRevealed.newBuilder()
                .setInstanceId(card.getInstanceId())
                .setCardId(card.getId())
                .setAttack(card.getCurrentStatistics().getAttack())
                .setDefence(card.getCurrentStatistics().getDefence())
                .setOverloadedPoints(card.getOverloadPoints())
                .setOwner(position.getOwner())
                .setPositionValue(position.getPosition().ordinal())
                .setPositionIndex(position.getIndex())
                .setCardCosts(CardCosts.newBuilder()
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .build())
                .build();
    }

    public static HandChanged getHandChangedDrawFromCards(List<Integer> selectedCards, int player, CardChangeType type) {
        return HandChanged.newBuilder()
                .addAllCardInstances(selectedCards)
                .setType(type)
                .setPlayer(player)
                .setCount(selectedCards.size())
                .build();
    }

    public static HandChanged getHandChangedDrawWithSize(int size, int player, CardChangeType type) {
        return HandChanged.newBuilder()
                .setType(type)
                .setPlayer(player)
                .setCount(size)
                .build();
    }


    public static PhaseChanged getPhaseChangedFromGamePhaseChanged(GamePhaseChanged gamePhaseChanged) {
        return PhaseChanged.newBuilder()
                .setNewPhaseValue(gamePhaseChanged.getCurrent().getFirst().ordinal())
                .setNewPlayer(gamePhaseChanged.getCurrent().getSecond())
                .setOldPhaseValue(gamePhaseChanged.getPrevious().getFirst().ordinal())
                .setOldPlayer(gamePhaseChanged.getPrevious().getSecond())
                .build();
    }

    public static CardStatisticsChanged getOverloadPointsChangeMessage(Card card) {
        return CardStatisticsChanged.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setOverloadPoints(card.getOverloadPoints())
                .build();
    }

    public static CardMoved getCardMovedMessageFromEvent(CardMovedEvent event) {
        return CardMoved.newBuilder()
                .setCardInstanceId(event.getMovedCard().getInstanceId())
                .setNewOwner(event.getNewPosition().getOwner())
                .setNewPositionValue(event.getNewPosition().getPosition().ordinal())
                .setOldOwner(event.getOldPosition().getOwner())
                .setOldPositionValue(event.getOldPosition().getPosition().ordinal())
                .build();
    }

    public static CardStatisticsChanged getCardStatisticsChangeMessage(StatisticsChange cardStatistics, CardPosition cardPosition) {
        return CardStatisticsChanged.newBuilder()
                .setCardInstanceId(cardStatistics.getInstanceId())
                .setOverloadPoints(cardStatistics.getOverloadPoints())
                .setAttackValue(cardStatistics.getAttack())
                .setDefenceValue(cardStatistics.getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(cardStatistics.getRaidersCost())
                        .setNeutral(cardStatistics.getNeutralCost())
                        .setPsychos(cardStatistics.getPsychoCost())
                        .setScanners(cardStatistics.getScannersCost())
                        .setWhiteHats(cardStatistics.getWhiteHatCost())
                        .build())
                .setOwner(cardPosition.getOwner())
                .setPositionValue(cardPosition.getPosition().ordinal())
                .build();
    }
}
