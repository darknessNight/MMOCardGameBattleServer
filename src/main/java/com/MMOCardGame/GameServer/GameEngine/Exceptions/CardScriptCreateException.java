package com.MMOCardGame.GameServer.GameEngine.Exceptions;

public class CardScriptCreateException extends RuntimeException {
    public CardScriptCreateException() {
    }

    public CardScriptCreateException(String message) {
        super(message);
    }

    public CardScriptCreateException(String message, Throwable cause) {
        super(message, cause);
    }

    public CardScriptCreateException(Throwable cause) {
        super(cause);
    }

    public CardScriptCreateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
