package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.ArrayList;
import java.util.List;

public class SelectedOptionsMessages {
    private List<TransactionMessage> targetsOtherThanOptionsMessages = new ArrayList<>();
    private List<TransactionMessage> optionsMessages = new ArrayList<>();

    public List<TransactionMessage> getMessagesWithSelectedOptions(){
        if(!targetsOtherThanOptionsMessages.isEmpty()){
            return targetsOtherThanOptionsMessages;
        } else {
            return optionsMessages;
        }
    }

    public void addToSelectedTargetsMessages(TransactionMessage selectedTargetsMessages) {
        var optionsType = selectedTargetsMessages.getOptionsInfo().getOptionsType();
        if(optionsType.equals(TargetSelectingTransaction.OptionsType.Target) ||
           optionsType.equals(TargetSelectingTransaction.OptionsType.CardInHand)){
            targetsOtherThanOptionsMessages.add(selectedTargetsMessages);
        } else {
            optionsMessages.clear();
            optionsMessages.add(selectedTargetsMessages);
        }
    }
}
