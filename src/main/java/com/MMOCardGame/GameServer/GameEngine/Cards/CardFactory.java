package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Exceptions.CardScriptCreateException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader.CardInfo;

public class CardFactory {
    private final static CardFactoryBuilder cardFactoryBuilder = new CardFactoryBuilder();
    private final Map<Integer, Class<? extends CardScript>> namedAbilityScriptsMap;
    private final Map<Integer, CardInfo> cardInfoMap;
    private final Map<Integer, Class<? extends CardScript>> cardScriptMap;
    private final static AtomicInteger instanceCounter = new AtomicInteger(0);
    private final Random randomGenerator;

    public CardFactory(Map<Integer, CardInfo> cardInfoMap, Map<Integer, Class<? extends CardScript>> cardScriptMap,
                       Map<Integer, Class<? extends CardScript>> namedAbilityScriptsMap, Random random) {
        this.randomGenerator = random;
        this.cardInfoMap = cardInfoMap;
        this.cardScriptMap = cardScriptMap;
        this.namedAbilityScriptsMap = namedAbilityScriptsMap;
    }

    public static CardFactoryBuilder builder() {
        return cardFactoryBuilder;
    }

    public Card createCard(int id) throws NotFoundCardException, CardScriptCreateException {
        if (cardInfoMap.containsKey(id)) {
            CardInfo cardInfo = cardInfoMap.get(id);
            int instanceId = instanceCounter.incrementAndGet();
            CardStatistics primaryStatistics = new CardStatistics(cardInfo.statistics.defence, cardInfo.statistics.attack,
                    cardInfo.cost.neutral, cardInfo.cost.raiders, cardInfo.cost.psychotronnics, cardInfo.cost.whiteHats,
                    cardInfo.cost.scanners, 0);
            Card cardObject = new Card(id, instanceId, primaryStatistics, cardInfo.abilities, cardInfo.cardType, cardInfo.cardSubtype,
                    cardInfo.cardClass, cardInfo.subclass, createScriptForCard(id));
            cardObject.setOverloadPoints(cardInfo.cost.overload);
            cardObject.setAbilitiesCosts(cardInfo.activatedAbilities);
            return cardObject;
        }
        throw new NotFoundCardException();
    }

    public List<Card> createListOfCards(List<Integer> ids) throws NotFoundCardException {
        List<Card> cards = ids.stream().map(this::createCard).collect(Collectors.toList());
        Collections.shuffle(cards, randomGenerator);
        return cards;
    }

    public CardScript createNameAbilityScript(int id) {
        return null;
    }

    public CardScript createScriptForCard(int id) throws CardScriptCreateException {
        if (cardScriptMap.containsKey(id)) {
            return createCardScriptInstance(id);
        } else {
            return createDefaultCardScript(id);
        }
    }

    private CardScript createDefaultCardScript(int id) {
        CardScript cardScript = new CardScript();
        cardScript.addAllSubscripts(getNamedScriptsForCard(id));
        return cardScript;
    }

    private CardScript createCardScriptInstance(int id) throws CardScriptCreateException {
        CardScript cardScript = createInstanceOfCardScriptClass(cardScriptMap.get(id));
        cardScript.addAllSubscripts(getNamedScriptsForCard(id));
        return cardScript;
    }

    private CardScript createInstanceOfCardScriptClass(Class<? extends CardScript> aClass) {
        try {
            return aClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CardScriptCreateException(e);
        }
    }

    private Set<CardScript> getNamedScriptsForCard(int id) {
        Set<CardScript> result = new HashSet<>();
        for (int abilityId : cardInfoMap.get(id).abilities) {
            if (namedAbilityScriptsMap.containsKey(abilityId))
                result.add(createInstanceOfCardScriptClass(namedAbilityScriptsMap.get(abilityId)));
        }
        return result;
    }
}
