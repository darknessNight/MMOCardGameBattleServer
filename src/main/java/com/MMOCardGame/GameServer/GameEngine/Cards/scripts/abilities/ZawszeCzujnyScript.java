package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.GameState;

@Script(ids = 1, type= Script.ScriptType.NamedAbility)
public class ZawszeCzujnyScript extends CardScript {

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return e -> parentCard.setOverloadPoints(0);
    }

}
