package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode
public class CardsInHandStepDefinition extends StepDefinition {
    private Function<Card, Boolean> filter;

    public CardsInHandStepDefinition(Function<Card, Boolean> filter,
                                     BiFunction<List<TargetSelectingOption>, GameState, Integer> numberOfOptionsProvider) {
        super(TargetSelectingTransaction.OptionsType.CardInHand, numberOfOptionsProvider);
        this.filter = filter;
    }

    public CardsInHandStepDefinition(Function<Card, Boolean> filter, int numberOfOptionsToSelect) {
        super(TargetSelectingTransaction.OptionsType.CardInHand, numberOfOptionsToSelect);
        this.filter = filter;
    }

    @Override
    protected List<TargetSelectingOption> getPossibleOptions(GameState gameState, int instanceId) {
        var allCardsInHand = getCardsInHand(gameState);
        var filteredCards = allCardsInHand.filter(card->filter.apply(card));
        return createOptionsList(filteredCards);
    }

    private List<TargetSelectingOption> createOptionsList(Stream<Card> cards) {
        return cards.map(card ->
                    TargetSelectingOption.newBuilder()
                        .setId(card.getId())
                        .setTargetType(TargetType.Player)
                        .build())
                .collect(Collectors.toList());
    }

    private Stream<Card> getCardsInHand(GameState gameState) {
        return gameState
                    .getAllCardsPositions()
                    .stream()
                    .filter(pair -> pair.getSecond().getPosition() == CardPosition.Position.Hand)
                    .map(Pair::getFirst);
    }

}
