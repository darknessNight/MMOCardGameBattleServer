package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public abstract class StepDefinition {

    private TargetSelectingTransaction.OptionsType optionsType;
    private BiFunction<List<TargetSelectingOption>, GameState, Integer> numberOfOptionsProvider;

    public StepDefinition(TargetSelectingTransaction.OptionsType optionsType,
                          BiFunction<List<TargetSelectingOption>, GameState, Integer> numberOfOptionsProvider){
        this.optionsType = optionsType;
        this.numberOfOptionsProvider = numberOfOptionsProvider;
    }

    public StepDefinition(TargetSelectingTransaction.OptionsType optionsType, int numberOfOptionsToSelect){
        this.optionsType = optionsType;
        this.numberOfOptionsProvider = (optionsList, gameState) -> numberOfOptionsToSelect;
    }

    public OptionsInfo getPossibleOptionsInfo(GameState gameState, int instanceId){
        var result = new OptionsInfo();
        var possibleTargets = getPossibleOptions(gameState, instanceId);
        result.setCountOfTargets(numberOfOptionsProvider.apply(possibleTargets, gameState));
        result.setOptionsType(optionsType);
        result.setOptions(possibleTargets);
        return result;
    }

    protected abstract List<TargetSelectingOption> getPossibleOptions(GameState gameState, int instanceId);

    public BiConsumer<List<TargetSelectingOption>, StepEnvironment> getSelectedOptionsHandler() {
        return (selectedOptions, stepEnvironment) -> { };
    }
}
