package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.LinksPoints;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;

import java.util.List;

public abstract class LinkCardBaseScript extends CardScript {

    @Override
    public boolean canDie(){
        return false;
    }

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        gameState.moveCardToBattlefield(parentCard.getInstanceId());
        return null;
    }

    @Override
    protected CardAbilityTask getAbilityAction(GameState gameState, int transactionId) {
        int playerId = gameState.getCardOwner(parentCard.getInstanceId());
        Player player = gameState.getPlayer(playerId);
        parentCard.setOverloadPoints(1);
        player.getLinksPoints().addPoints(getPointsTooAdd());
        return null;
    }

    public List<GamePhase> getAvailablePhasesForAbility() {
        List<GamePhase> list = getSubscriptsAvailablePhases(CardScript::getAvailablePhasesForAbility);
        list.add(GamePhase.Untap);
        list.add(GamePhase.Main);
        list.add(GamePhase.Attack);
        list.add(GamePhase.Defence);
        list.add(GamePhase.EndRound);
        list.add(GamePhase.ReactionPhase);
        return list;
    }

    protected abstract LinksPoints getPointsTooAdd();

}
