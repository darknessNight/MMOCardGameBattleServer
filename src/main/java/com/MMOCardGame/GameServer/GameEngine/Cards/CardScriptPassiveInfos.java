package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.TargetType;
import com.MMOCardGame.GameServer.common.CallbackTaskWithReturn;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter(AccessLevel.PROTECTED)
public class CardScriptPassiveInfos implements Serializable {
    transient Set<CardScript> subscripts = new HashSet<>();
    protected Card parentCard;

    void setParentCard(Card parentCard) {
        this.parentCard = parentCard;
        subscripts.forEach(cardScript -> cardScript.setParentCard(parentCard));
    }

    public boolean canDie() {
        return checkConditionForSubscripts(CardScript::canDie);
    }

    public boolean canAttack() {
        return checkConditionForSubscripts(CardScript::canAttack) && parentCard.isAlive();
    }

    public boolean canBeAttackedBy(Card card) {
        return checkConditionForSubscripts(cardScript -> cardScript.canBeAttackedBy(card));
    }

    private boolean checkConditionForSubscripts(CallbackTaskWithReturn<Boolean, CardScript> task) {
        for (CardScript cardScript : subscripts)
            if (!task.run(cardScript))
                return false;
        return true;
    }

    public boolean canBeBlockedBy(Card card) {
        return checkConditionForSubscripts(cardScript -> cardScript.canBeBlockedBy(card));
    }

    public boolean canBeAffectedByAbility(Card card, int abilityId) {
        return checkConditionForSubscripts(cardScript -> cardScript.canBeAffectedByAbility(card, abilityId));
    }

    public boolean canBeAffectedByUserAbility(int abilityId) {
        return checkConditionForSubscripts(cardScript -> cardScript.canBeAffectedByUserAbility(abilityId));
    }

    public void addSubscript(CardScript cardScript) {
        subscripts.add(cardScript);
        cardScript.setParentCard(parentCard);
    }

    public void addAllSubscripts(Set<CardScript> cardScript) {
        subscripts.addAll(cardScript);
        cardScript.forEach(cardScript1 -> cardScript1.setParentCard(parentCard));
    }

    public void removeSubscript(Class<? extends CardScript> cardScript) {
        subscripts.removeIf(cardScript1 -> {
            var result = cardScript1.getClass() == cardScript;
            if(result) cardScript1.setParentCard(null);
            return result;
        });
    }

    public Set<CardScript> getSubscripts() {
        return subscripts;
    }

    public boolean isAlwaysAttackingTarget() {
        return false;
    }

    public double getAttackMultiplier(int id, TargetType type) {
        return 1.0;
    }
}
