package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OptionsInfo {

    private List<TargetSelectingOption> options = new ArrayList<>();
    private int countOfTargets = 0;
    private TargetSelectingTransaction.OptionsType optionsType = TargetSelectingTransaction.OptionsType.Target;

}
