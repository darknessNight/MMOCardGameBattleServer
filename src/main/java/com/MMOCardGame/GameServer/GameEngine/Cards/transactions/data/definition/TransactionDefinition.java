package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.PrefixesForReactionStackDescriptions;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.StepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Getter
public class TransactionDefinition {

    private TargetSelectingTransaction.StartTransactionFor startedFor;
    private List<StepDefinition> stepsDefinitions = new ArrayList<>();
    private Function<TransactionMessage, String> finalMessageDescriptionProvider;

    public TransactionDefinition(TargetSelectingTransaction.StartTransactionFor startedFor,
                                 Function<TransactionMessage, String> finalMessageDescriptionProvider){
        this.startedFor = startedFor;
        this.finalMessageDescriptionProvider = finalMessageDescriptionProvider;
    }

    public TransactionDefinition(TargetSelectingTransaction.StartTransactionFor startedFor){
        this.startedFor = startedFor;
        finalMessageDescriptionProvider = tm -> PrefixesForReactionStackDescriptions.CARD_DESCRIPTION;
    }

    public String getFinalMessageDescription(TransactionMessage lastMessage){
        return finalMessageDescriptionProvider.apply(lastMessage);
    }

    public StepDefinition getFirstStepDefinition()
    {
        if(hasAnySteps()) {
            return stepsDefinitions.get(0);
        }else{
            return null;
        }
    }

    public boolean hasAnySteps(){
        return !stepsDefinitions.isEmpty();
    }

    public void addStep(StepDefinition step){
        stepsDefinitions.add(step);
    }
}
