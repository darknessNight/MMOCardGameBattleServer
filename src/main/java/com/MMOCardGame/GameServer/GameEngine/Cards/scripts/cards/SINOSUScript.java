package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.UnitBaseScript;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.PhaseEndedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PhaseEndedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import com.MMOCardGame.GameServer.common.Pair;

import java.util.Collection;
import java.util.Collections;

@Script(ids = 9, type = Script.ScriptType.Card)
public class SINOSUScript extends UnitBaseScript implements PhaseEndedScript {

    @Override
    public void phaseEnded(PhaseEndedEvent event) {
        if(event.getNewPhase().equals(GamePhase.Untap) && event.getOldPhase().equals(GamePhase.EndRound)
            && event.getGameState().getCardPosition(parentCard.getInstanceId()).getPosition() == CardPosition.Position.Battlefield){
            addTriggerPhaseForMe(PhaseEndedScript.super.getTriggerType(), event);
        }
    }

    @Override
    protected Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> getTriggerTask(TriggerType type, TriggerEvent event, int transactionId) {
        if(type==PhaseEndedScript.super.getTriggerType())
        return new Pair<>(gameState -> event.getGameState().moveCardToGraveyard(parentCard.getInstanceId()),
                Collections.singletonList(TargetSelectingTransaction.newBuilder()
                        .setTransactionId(transactionId)
                        .setType(TargetSelectingTransactionType.Result)
                        .setDestinationTargetType(TargetType.Card)
                        .setDestinationTargetId(parentCard.getInstanceId())
                        .setOptionsType(TargetSelectingTransaction.OptionsType.Target)
                        .addOptions(TargetSelectingOption.newBuilder()
                                .setTargetType(TargetType.Card)
                                .setId(parentCard.getInstanceId())
                                .build())
                        .build()));
        else return null;
    }

}
