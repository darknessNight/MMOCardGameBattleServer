package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public interface CardInfoReader {

    default List<CardInfo> readFromFile(String path) throws IOException, IllegalArgumentException {
        try (FileInputStream fileStream = new FileInputStream(path)) {
            return readFromStream(fileStream);
        }
    }

    List<CardInfo> readFromStream(InputStream stream) throws IOException, IllegalArgumentException;

    @Data
    class CardInfo {
        int id;
        Statistics statistics;
        Costs cost;
        List<Integer> abilities;
        String type;
        String subtype;
        CardType cardType;
        CardSubtype cardSubtype;
        List<ActivatedAbility> activatedAbilities;
        @SerializedName("class")
        String cardClass;
        String subclass;

        void prepareToUse() {
            convertStringsToEnums();
            setDefaultValues();
        }

        void convertStringsToEnums() {
            if (type == null || type.isEmpty())
                cardType = CardType.Undefined;
            else cardType = CardType.valueOf(type);
            if (subtype == null || subtype.isEmpty())
                cardSubtype = CardSubtype.Basic;
            else cardSubtype = CardSubtype.valueOf(subtype);
        }

        void setDefaultValues() {
            if (cost == null) cost = new Costs();
            if (statistics == null) statistics = new Statistics();
            if (abilities == null) abilities = new ArrayList<>();
        }

        @Data
        static class Statistics {
            int defence;
            int attack;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Costs {
            int neutral;
            int raiders;
            int scanners;
            int whiteHats;
            int psychotronnics;
            int overload;
        }

        @Data
        static class ActivatedAbility {
            Costs cost;
            int id;
        }
    }
}
