package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.EnumMap;

@Script(ids = 27, type = Script.ScriptType.Card)
public class LiderSiatkiScript extends PassiveAbilityCardScript {

    private static final int ATTACK_INC = 1;
    private static final int DEFENCE_INC = 1;

    private static final String PRZESLADOWCA_SUBCLASS = "Prześladowca";
    private static final String KAWALERIA_SUBCLASS = "Kawaleria";

    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState.moveCardToBattlefield(parentCard.getInstanceId());
                parentCard.setOverloadPoints(1);
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    private boolean isPrzesladowcaOrKawaleriaUnit(Card card) {
        return card.getType().equals(CardType.Unit) &&
                (card.getSubclassName().equals(PRZESLADOWCA_SUBCLASS) ||
                        card.getSubclassName().equals(KAWALERIA_SUBCLASS));
    }

    @Override
    protected void addEffectToCard(Card card) {
        if(isPrzesladowcaOrKawaleriaUnit(card)) {
            changeAttack(card, ATTACK_INC);
            changeDefence(card, DEFENCE_INC);
        }
    }

    @Override
    protected void removeEffectFromCard(Card card) {
        if(isPrzesladowcaOrKawaleriaUnit(card)) {
            changeAttack(card, -ATTACK_INC);
            changeDefence(card, -DEFENCE_INC);
        }
    }

    private void changeAttack(Card card, int change) {
        card.getBaseStatistics().setAttack(card.getBaseStatistics().getAttack() + change);
        card.changeAttack(change);
    }

    private void changeDefence(Card card, int change) {
        card.getBaseStatistics().setDefence(card.getBaseStatistics().getDefence() + change);
        card.changeDefence(change);
    }

    @Override
    public boolean canDie(){
        return true;
    }

    @Override
    public boolean canAttack() {
        return parentCard.isAlive();
    }

    @Override
    public boolean isAlwaysAttackingTarget(){
        return false;
    }

    @Override
    protected EnumMap getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var attackTransactionDefinition = getTransactionDefinitionForAttack();
        definitions.put(attackTransactionDefinition.getStartedFor(), attackTransactionDefinition);

        var defendTransactionDefinition = getTransactionDefinitionForDefend();
        definitions.put(defendTransactionDefinition.getStartedFor(), defendTransactionDefinition);

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForAttack(){
        var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);
        attackTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> player.getUserId() != parentCard.getOwner(),
                (gameState, card) -> card.getOwner() != parentCard.getOwner()
                        && gameState.getCardPosition(card.getInstanceId()).getPosition()== CardPosition.Position.Battlefield
                        && card.getType() == CardType.ComputingUnit,
                1));
        return attackTransactionDefinition;
    }

    private TransactionDefinition getTransactionDefinitionForDefend(){
        var defendTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Defend);
        defendTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> false,
                (gameState, card) -> gameState
                        .getAttackersForPlayer(gameState.getRoundPlayer())
                        .containsKey(card.getInstanceId()),
                1));
        return defendTransactionDefinition;
    }
}
