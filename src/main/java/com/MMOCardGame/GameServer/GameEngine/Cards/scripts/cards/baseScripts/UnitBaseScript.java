package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.EnumMap;

public class UnitBaseScript extends CardScript {

    @Override
    public boolean canDie(){
        return true;
    }

    @Override
    public boolean canAttack() {
        return parentCard.isAlive();
    }

    @Override
    public boolean isAlwaysAttackingTarget(){
        return false;
    }

    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState.moveCardToBattlefield(parentCard.getInstanceId());
                parentCard.setOverloadPoints(1);
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    @Override
    protected EnumMap getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var attackTransactionDefinition = getTransactionDefinitionForAttack();
        definitions.put(attackTransactionDefinition.getStartedFor(), attackTransactionDefinition);

        var defendTransactionDefinition = getTransactionDefinitionForDefend();
        definitions.put(defendTransactionDefinition.getStartedFor(), defendTransactionDefinition);

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForAttack(){
        var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);
        attackTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> player.getUserId() != parentCard.getOwner(),
                (gameState, card) -> card.getOwner() != parentCard.getOwner()
                        && gameState.getCardPosition(card.getInstanceId()).getPosition()== CardPosition.Position.Battlefield
                        && card.getType() == CardType.ComputingUnit,
                1));
        return attackTransactionDefinition;
    }

    private TransactionDefinition getTransactionDefinitionForDefend(){
        var defendTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Defend);
        defendTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> false,
                (gameState, card) -> gameState
                                    .getAttackersForPlayer(gameState.getRoundPlayer())
                                    .containsKey(card.getInstanceId()),
                1));
        return defendTransactionDefinition;
    }

}
