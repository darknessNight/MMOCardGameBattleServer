package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.TargetType;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.common.Pair;

import java.util.*;

public class CardScript extends CardScriptAbilityBehaviours {
    transient EnumMap<TriggerType, TriggerEvent> triggerEventMap = new EnumMap<>(TriggerType.class);

    protected void addTriggerPhaseForMe(TriggerType triggerType, TriggerEvent event) {
        event.getGameState().getPhasesStack().startTriggerPhase(triggerType, getParentCard().getInstanceId());
        triggerEventMap.put(triggerType, event);
    }

    public boolean isTransactionNeeded(TriggerType triggerType) {
        return false;
    }

    public void startTriggerActions(TriggerType type, int transactionId) {
        TriggerEvent event = triggerEventMap.get(type);
        triggerEventMap.remove(type);
        Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> task = getTriggerTask(type, event, transactionId);
        pushReactionStackEvent(task, type, event);
    }

    protected Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> getTriggerTask(TriggerType type, TriggerEvent event, int transactionId/*-1 if none*/) {
        return new Pair<>(g -> {}, new ArrayList<>());
    }

    private void pushReactionStackEvent(Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> task, TriggerType triggerType, TriggerEvent event) {
        event.getReactionStackController().pushTriggerAction(task.getFirst(), getParentCard(), triggerType, task.getSecond());
    }

    public int getTriggerTransactionPlayer(TriggerType triggerType) {
        return -1;
    }
}
