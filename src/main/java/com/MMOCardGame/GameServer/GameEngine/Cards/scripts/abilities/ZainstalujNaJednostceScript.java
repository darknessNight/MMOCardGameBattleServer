package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.CardMovedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;

import java.util.ArrayList;
import java.util.EnumMap;

@Script(ids = 7, type= Script.ScriptType.NamedAbility)
public class ZainstalujNaJednostceScript extends CardScript implements CardMovedScript {

    protected int installedOnCardInstanceId;

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                saveCardWhichIamInstalled(transactionId);
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    protected void saveCardWhichIamInstalled(int transactionId) {
        var selectedTargets = new ArrayList<TargetSelectingOption>();
        for (var transactionMessage : getSelectedOptionsDuringTransaction(transactionId)){
            selectedTargets.addAll(transactionMessage.getOptionsList());
        }
        if(selectedTargets.size() != 1){
            return;
        }
        var selectedTarget = selectedTargets.get(0);
        if(selectedTarget.getTargetType().equals(TargetType.Card)){
            installedOnCardInstanceId = selectedTarget.getId();
        }
    }

    @Override
    protected EnumMap getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var playTransactionDefinition = getTransactionDefinitionForPlay();
        definitions.put(playTransactionDefinition.getStartedFor(), playTransactionDefinition);

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForPlay(){
        var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Play);
        attackTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> false,
                (gameState, card) ->    card.getType().equals(CardType.Unit) &&
                                        gameState.getCardPosition(card.getInstanceId()).getPosition() == CardPosition.Position.Battlefield,
                1));
        return attackTransactionDefinition;
    }

    @Override
    public void cardMoved(CardMovedEvent event) {
        if(event.getMovedCard().getInstanceId() == installedOnCardInstanceId &&
           event.getOldPosition().getPosition() == CardPosition.Position.Battlefield &&
           event.getNewPosition().getPosition() != CardPosition.Position.Battlefield){
            event.getGameState().moveCardToGraveyard(parentCard.getInstanceId());
        }
    }
}
