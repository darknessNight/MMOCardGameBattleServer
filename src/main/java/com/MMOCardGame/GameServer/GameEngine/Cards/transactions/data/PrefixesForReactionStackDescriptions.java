package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data;

public class PrefixesForReactionStackDescriptions {

    public static final String CARD_ABILITY = "CardAbility.";
    public static final String NAMED_ABILITY = "NamedAbility.";
    public static final String CARD_DESCRIPTION= "CardDescription";

}
