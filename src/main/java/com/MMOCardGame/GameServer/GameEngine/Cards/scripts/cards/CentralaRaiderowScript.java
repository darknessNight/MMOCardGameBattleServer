package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.PrefixesForReactionStackDescriptions;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.CustomOptionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.OptionsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.LinksPoints;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.factories.DaggerCardFactoryFactory;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.ArrayList;
import java.util.EnumMap;

@Script(ids = 19, type = Script.ScriptType.Card)
public class CentralaRaiderowScript extends CardScript {

    private boolean createZarzadcaCentraliUsed = false;
    private static int LINK_POINTS_ABILITY_ID;
    private static int NEW_CARD_ABILITY_ID;
    private static int LINK_POINTS_ABILITY_JSON_ID = 1;
    private static int NEW_CARD_ABILITY_JSON_ID = 2;

    private static ArrayList<CustomOptionDefinition> possibleAbilities = null;

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        gameState.moveCardToBattlefield(parentCard.getInstanceId());
        parentCard.setOverloadPoints(4);
        return null;
    }

    @Override
    protected CardAbilityTask getAbilityAction(GameState gameState, int transactionId) {
        int selectedAbilityId = getSelectedAbilityId(transactionId);
        if(selectedAbilityId == LINK_POINTS_ABILITY_ID){
            int playerId = gameState.getCardOwner(parentCard.getInstanceId());
            Player player = gameState.getPlayer(playerId);
            player.getLinksPoints().addPoints(new LinksPoints(0, 1, 0, 0, 0));
            parentCard.changeOverloadPoints(1);
            return null;
        }
        if(selectedAbilityId == NEW_CARD_ABILITY_ID && !createZarzadcaCentraliUsed)
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                int selectedAbilityId = getSelectedAbilityId(transactionId);

                if(selectedAbilityId == NEW_CARD_ABILITY_ID && !createZarzadcaCentraliUsed) {
                    CardFactory cardFactory = DaggerCardFactoryFactory.create().getCardFactoryBuilder().build();
                    Card card = cardFactory.createCard(-1);
                    gameState.insertCard(card, new CardPosition(parentCard.getOwner(), CardPosition.Position.Battlefield));
                    createZarzadcaCentraliUsed = true;
                }
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
        else return null;
    }

    private int getSelectedAbilityId(int transactionId) {
        var selectedTargets = new ArrayList<TargetSelectingOption>();
        for (var transactionMessage : getSelectedOptionsDuringTransaction(transactionId)){
            selectedTargets.addAll(transactionMessage.getOptionsList());
        }

        if(selectedTargets.size() != 1){
            return -1;
        }

        return selectedTargets.get(0).getId();
    }

    @Override
    protected EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var abilityTransactionDefinition = getTransactionDefinitionForAbility();
        definitions.put(abilityTransactionDefinition.getStartedFor(), abilityTransactionDefinition);

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForAbility(){
        OptionsStepDefinition step = getPossibleAbilitiesStep();

        var abilityTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Ability,
                transactionMessage -> {
                    var selectedTargets = new ArrayList<TargetSelectingOption>(transactionMessage.getOptionsInfo().getOptions());

                    if(selectedTargets.size() != 1){
                        return "";
                    }
                    int selectedAbilityId = selectedTargets.get(0).getId();
                    if(selectedAbilityId == LINK_POINTS_ABILITY_ID){
                        return PrefixesForReactionStackDescriptions.CARD_ABILITY +  "1";
                    } else if (selectedAbilityId == NEW_CARD_ABILITY_ID){
                        return PrefixesForReactionStackDescriptions.CARD_ABILITY +  "2";
                    }
                    return "";
                });

        abilityTransactionDefinition.addStep(step);
        return abilityTransactionDefinition;
    }

    private OptionsStepDefinition getPossibleAbilitiesStep() {
        ArrayList<CustomOptionDefinition> possibleOptions = getPossibleAbilities();
        return new OptionsStepDefinition(possibleOptions, possibleOptions.size());
    }

    private static ArrayList<CustomOptionDefinition> getPossibleAbilities() {
        if(possibleAbilities == null) {
            possibleAbilities = new ArrayList<>();
            CustomOptionDefinition addLinkOption =
                    new CustomOptionDefinition(
                            "Dodaj punkt łącza szkoły rajdowców");
            LINK_POINTS_ABILITY_ID = addLinkOption.getId();
            possibleAbilities.add(addLinkOption);
            CustomOptionDefinition newCardOption =
                    new CustomOptionDefinition("Stwórz Zarządce Centrali",
                            (gameState, instanceId) ->
                                    !((CentralaRaiderowScript)gameState.getCard(instanceId).getCardScript()).isCreateZarzadcaCentraliUsed());
            NEW_CARD_ABILITY_ID = newCardOption.getId();
            possibleAbilities.add(newCardOption);
        }
        return possibleAbilities;
    }

    public boolean isCreateZarzadcaCentraliUsed(){
        return createZarzadcaCentraliUsed;
    }

    @Override
    public int getAbilityOverloadCost(int transactionId) {
        var selectedAbilityId = getSelectedAbilityId(transactionId);
        if(selectedAbilityId == LINK_POINTS_ABILITY_ID){
            return parentCard.getAbilityCosts(LINK_POINTS_ABILITY_JSON_ID).getOverload();
        }
        else if(selectedAbilityId == NEW_CARD_ABILITY_ID){
            return parentCard.getAbilityCosts(NEW_CARD_ABILITY_JSON_ID).getOverload();
        }
        return super.getAbilityOverloadCost(transactionId);
    }

    @Override
    public CardInfoReader.CardInfo.Costs getAbilityCosts(int transactionId) {
        var selectedAbilityId = getSelectedAbilityId(transactionId);
        if(selectedAbilityId == LINK_POINTS_ABILITY_ID){
            return parentCard.getAbilityCosts(LINK_POINTS_ABILITY_JSON_ID);
        }
        else if(selectedAbilityId == NEW_CARD_ABILITY_ID){
            return parentCard.getAbilityCosts(NEW_CARD_ABILITY_JSON_ID);
        }
        return super.getAbilityCosts(transactionId);
    }
    
}
