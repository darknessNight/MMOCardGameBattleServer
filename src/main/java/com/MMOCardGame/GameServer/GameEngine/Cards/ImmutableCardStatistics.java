package com.MMOCardGame.GameServer.GameEngine.Cards;

import java.io.Serializable;

public class ImmutableCardStatistics extends CardStatistics implements Serializable {
    public ImmutableCardStatistics(CardStatistics basic) {
        super(basic);
    }

    @Override
    public void setDefence(int defence) {
    }

    @Override
    public void setAttack(int attack) {
    }

    @Override
    public void setNeutralCost(int neutralCost) {
    }

    @Override
    public void setRaidersCost(int raidersCost) {
    }

    @Override
    public void setPsychoCost(int psychoCost) {
    }

    @Override
    public void setWhiteHatCost(int whiteHatCost) {
    }

    @Override
    public void setScannersCost(int scannersCost) {
    }

    @Override
    public void setOverloadPoints(int overloadPoints) {
    }

    @Override
    public void resetTo(CardStatistics other) {
    }
}
