package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.UnitBaseScript;

@Script(ids = 10, type = Script.ScriptType.Card)
public class ScriptKiddieScript extends UnitBaseScript {

    @Override
    public int getAbilityOverloadCost(int transactionId) {
        return parentCard.getAbilityCosts(1).getOverload();
    }

    @Override
    public CardInfoReader.CardInfo.Costs getAbilityCosts(int transactionId) {
        return parentCard.getAbilityCosts(1);
    }

}
