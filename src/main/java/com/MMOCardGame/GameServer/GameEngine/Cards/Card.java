package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.MMOCardGame.GameServer.common.Observable;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(exclude = {"cardScript", "statisticsChangeObservable", "owner", "position"})
@ToString(of = {"instanceId", "revealed"})
public class Card implements Serializable {
    @Getter
    private final ImmutableCardStatistics primaryStatistics;
    @Getter
    private int id;
    @Getter
    private int instanceId;
    private CardStatistics baseStatistics;
    private CardStatistics currentStatistics;
    @Getter
    private List<Integer> namedAbilities;
    @Setter
    @Getter
    private CardType type;
    @Setter
    @Getter
    private CardSubtype subtype;
    @Getter
    private String className;
    @Getter
    private String subclassName;
    @Getter
    private CardScript cardScript;
    @Getter @Setter
    private int owner;
    @Getter @Setter
    private CardPosition.Position position;
    @Getter @Setter
    private boolean revealed = false;
    private transient ObservableNotifier<StatisticsChange> statisticsChangeObservable = new ObservableNotifier<>();
    Map<Integer, CardInfoReader.CardInfo.Costs> activatedAbilitiesCosts=new HashMap<>();

    public Card(int id, int instanceId, CardStatistics primaryStatistics, List<Integer> namedAbilities, CardType type, CardSubtype subtype, String className, String subclassName, CardScript cardScript) {
        this.id = id;
        this.instanceId = instanceId;
        this.primaryStatistics = new ImmutableCardStatistics(primaryStatistics);
        this.baseStatistics = new CardStatistics(primaryStatistics);
        this.currentStatistics = new CardStatistics(primaryStatistics);
        this.namedAbilities = namedAbilities;
        this.type = type;
        this.subtype = subtype;
        this.className = className;
        this.subclassName = subclassName;
        this.cardScript = cardScript;
        this.cardScript.setParentCard(this);
    }

    public synchronized void changeOverloadPoints(int value) {
        int newValue = changeOverloadPointsSilent(value);
        setOverloadPoints(newValue);
    }

    private int changeOverloadPointsSilent(int value) {
        int newValue = currentStatistics.getOverloadPoints() + value;
        if (newValue < 0)
            newValue = 0;
        currentStatistics.setOverloadPoints(newValue);
        return newValue;
    }

    private void notifyAllAboutChange() {
        statisticsChangeObservable.notifyAll(new StatisticsChange(this));
    }

    public synchronized void decrementOverloadPoints() {
        changeOverloadPoints(-1);
    }

    public void decrementOverloadPointsSilent() {
        changeOverloadPointsSilent(-1);
    }

    public synchronized void incrementOverloadPoints() {
        changeOverloadPoints(1);
    }

    public synchronized void incrementOverloadPointsSilent() {
        changeOverloadPointsSilent(1);
    }

    public synchronized boolean isAlive() {
        return getDefence() > 0;
    }

    public synchronized void changeAttack(int changeValue) {
        int value = currentStatistics.getAttack();
        currentStatistics.setAttack(value + changeValue);
        notifyAllAboutChange();
    }

    public synchronized void changeDefence(int changeValue) {
        int value = currentStatistics.getDefence();
        currentStatistics.setDefence(value + changeValue);
        if (!cardScript.canDie() && currentStatistics.getDefence() <= 0)
            currentStatistics.setDefence(1);
        notifyAllAboutChange();
    }

    public synchronized int getAttack() {
        return currentStatistics.getAttack();
    }

    public synchronized int getDefence() {
        return currentStatistics.getDefence();
    }

    public synchronized int getOverloadPoints() {
        return currentStatistics.getOverloadPoints();
    }

    public synchronized void setOverloadPoints(int overloadPoints) {
        currentStatistics.setOverloadPoints(overloadPoints);
        notifyAllAboutChange();
    }

    public synchronized void resetStatiticsToBase() {
        currentStatistics.resetTo(baseStatistics);
        notifyAllAboutChange();
    }

    public synchronized CardStatistics getBaseStatistics() {
        return baseStatistics;
    }

    public synchronized void setBaseStatistics(CardStatistics statistics) {
        baseStatistics = statistics;
        notifyAllAboutChange();
    }

    public synchronized CardStatistics getCurrentStatistics() {
        return currentStatistics;
    }

    public synchronized void setCurrentStatistics(CardStatistics statistics) {
        currentStatistics = statistics;
        notifyAllAboutChange();
    }

    public synchronized void resetBaseStatisticsToPrimary() {
        baseStatistics.resetTo(primaryStatistics);
        notifyAllAboutChange();
    }

    public synchronized void setClassName(String className) {
        this.className = className;
        notifyAllAboutChange();
    }

    public synchronized void setSubclassName(String subclassName) {
        this.subclassName = subclassName;
        notifyAllAboutChange();
    }

    public synchronized void setCosts(int neutral, int psycho, int raiders, int whiteHats, int scanners) {
        this.currentStatistics.setNeutralCost(neutral);
        currentStatistics.setPsychoCost(psycho);
        currentStatistics.setRaidersCost(raiders);
        currentStatistics.setWhiteHatCost(whiteHats);
        currentStatistics.setScannersCost(scanners);
        notifyAllAboutChange();
    }

    public synchronized Observable<StatisticsChange> getObservable() {
        return this.statisticsChangeObservable;
    }

    public CallbackTask<GameState> getDefeatAction() {
        var action = cardScript.getDeathAction();
        if (action == null)
            return gameState -> gameState.moveCardToGraveyard(instanceId);
        else return action;
    }

    public void setAbilitiesCosts(List<CardInfoReader.CardInfo.ActivatedAbility> activatedAbilities) {
        if(activatedAbilities!=null)
            activatedAbilities.forEach(activatedAbility -> activatedAbilitiesCosts.put(activatedAbility.id,activatedAbility.cost));
    }

    public CardInfoReader.CardInfo.Costs getAbilityCosts(int id){
        return activatedAbilitiesCosts.get(id);
    }
}
