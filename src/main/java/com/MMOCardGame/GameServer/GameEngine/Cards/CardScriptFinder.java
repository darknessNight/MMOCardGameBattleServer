package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import com.google.common.reflect.ClassPath;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class CardScriptFinder {
    private Collection<String> searchPackages;
    private ClassPath classPath;

    public CardScriptFinder(Collection<String> searchPackages) throws IOException {
        this.searchPackages = searchPackages;
        this.classPath = getClassPath();
    }

    private ClassPath getClassPath() throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        return ClassPath.from(classLoader);
    }

    public Map<Integer, Class<? extends CardScript>> findCardScripts() {
        return findClassesWithType(Script.ScriptType.Card);
    }

    public Map<Integer, Class<? extends CardScript>> findNamedAbilitiesScripts() {
        return findClassesWithType(Script.ScriptType.NamedAbility);
    }

    private Map<Integer, Class<? extends CardScript>> findClassesWithType(Script.ScriptType type) {
        Map<Integer, Class<? extends CardScript>> result = new ConcurrentHashMap<>();
        for (String packageName : searchPackages) {
            findClassesInPackage(type, packageName).forEach(aClass -> {
                int[] ids = getScriptAnnotation(aClass).ids();
                for (int id : ids) {
                    result.put(id, (Class<? extends CardScript>) aClass);
                }
            });
        }
        return result;
    }

    private Script getScriptAnnotation(Class<?> aClass) throws NotFoundException {
        var stream = Arrays.stream(aClass.getAnnotations())
                .filter(annotation -> annotation.annotationType().equals(Script.class))
                .findAny();
        if (stream.isPresent())
            return (Script) stream.get();
        else throw new NotFoundException();
    }

    private Stream<? extends Class<?>> findClassesInPackage(final Script.ScriptType type, String packageName) {
        Set<ClassPath.ClassInfo> classInfoSet = classPath.getTopLevelClasses(packageName);
        return classInfoSet.parallelStream().map(ClassPath.ClassInfo::load)
                .filter(aClass -> CardScript.class.isAssignableFrom(aClass)
                        && isScriptAnnotated(aClass, type));
    }

    private boolean isScriptAnnotated(Class<?> aClass, Script.ScriptType type) {
        return Arrays.stream(aClass.getAnnotations()).anyMatch(annotation -> annotation.annotationType().equals(Script.class)
                && ((Script) annotation).type() == type);
    }
}
