package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode
public class TargetsStepDefinition extends StepDefinition {
    private BiFunction<GameState, Player, Boolean> playersFilter;
    private BiFunction<GameState, Card, Boolean> cardsFilter;

    public TargetsStepDefinition(BiFunction<GameState, Player, Boolean> playersFilter,
                                 BiFunction<GameState, Card, Boolean> cardsFilter,
                                 BiFunction<List<TargetSelectingOption>, GameState, Integer> numberOfOptionsProvider) {
        super(TargetSelectingTransaction.OptionsType.Target, numberOfOptionsProvider);
        this.playersFilter = playersFilter;
        this.cardsFilter = cardsFilter;
    }

    public TargetsStepDefinition(BiFunction<GameState, Player, Boolean> playersFilter,
                                 BiFunction<GameState, Card, Boolean> cardsFilter,
                                 int numberOfOptionsToSelect) {
        super(TargetSelectingTransaction.OptionsType.Target, numberOfOptionsToSelect);
        this.playersFilter = playersFilter;
        this.cardsFilter = cardsFilter;
    }

    @Override
    protected List<TargetSelectingOption> getPossibleOptions(GameState gameState, int instanceId) {
        List<TargetSelectingOption> possibleTargets = new ArrayList<>();
        var cards = getCards(gameState);
        var players = getPlayers(gameState);
        var cardsTargets = getCardsTargets(cards);
        possibleTargets.addAll(cardsTargets);
        var playersTargets = getPlayersTargets(players);
        possibleTargets.addAll(playersTargets);

        return possibleTargets;
    }

    private List<TargetSelectingOption> getCardsTargets(Stream<Card> cards) {
        return cards.map(card ->
                    TargetSelectingOption.newBuilder()
                        .setId(card.getInstanceId())
                        .setTargetType(TargetType.Card)
                        .build())
                .collect(Collectors.toList());
    }

    private List<TargetSelectingOption> getPlayersTargets(Stream<Player> players) {
        return players.map(player ->
                    TargetSelectingOption.newBuilder()
                        .setId(player.getUserId())
                        .setTargetType(TargetType.Player)
                        .build())
                .collect(Collectors.toList());
    }

    private Stream<Player> getPlayers(GameState gameState) {
        return gameState
                    .getPlayers()
                    .values()
                    .stream()
                    .filter(player->playersFilter.apply(gameState, player));
    }

    private Stream<Card> getCards(GameState gameState) {
        return gameState
                    .getAllCards()
                    .stream()
                    .filter(card -> cardsFilter.apply(gameState, card));
    }
}
