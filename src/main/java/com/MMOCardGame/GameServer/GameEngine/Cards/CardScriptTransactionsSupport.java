package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionsDefinitionsCollection;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.HandlersFactory;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection.TransactionsCollection;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;

import java.util.*;
import java.util.stream.Collectors;

public class CardScriptTransactionsSupport extends CardScriptPassiveInfos {

    private transient TransactionsCollection transactions = new TransactionsCollection();
    private transient TransactionsDefinitionsCollection transactionsDefinitions = new TransactionsDefinitionsCollection();

    public CardScriptTransactionsSupport(){
        for(var transactionDefinition : getTransactionsDefinitions().values())
        {
            transactionsDefinitions.addDefinition(transactionDefinition);
        }
        for (CardScript cardScript : subscripts) {
            for(var transactionDefinition : cardScript.getTransactionsDefinitions().values())
            {
                transactionsDefinitions.addDefinition(transactionDefinition);
            }
        }
    }

    protected Map<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> getTransactionsDefinitions()
    {
        return new HashMap<>();
    }

    protected TransactionsCollection getTransactions(){
        return transactions;
    }

    public TargetSelectingTransaction runTransactionAction(TargetSelectingTransaction grpcMessage, GameState gameState) {
        var message = new TransactionMessage(grpcMessage);
        if(parentCard.getOverloadPoints() != 0){
            var response = message.getResponseTemplate();
            response.setType(TargetSelectingTransactionType.Result);
            return response.getGrpcFormat();
        }
        var messageHandler = HandlersFactory.produce(message);
        var transactionEnvironment = new TransactionEnvironment(gameState,
                                                                transactions,
                                                                transactionsDefinitions,
                                                                parentCard == null ? -1 : parentCard.getInstanceId());
        var result = messageHandler.handle(message, transactionEnvironment);
        return result.getGrpcFormat();
    }

    public Collection<TargetSelectingTransaction> getSelectedOptionsDuringTransaction(int transactionId){
        if(transactionId==-1)
            return new ArrayList<>();
        Transaction rememberedTransaction = null;

        for (CardScript cardScript : subscripts) {
            if(cardScript.getTransactions().isRemembered(transactionId)){
                rememberedTransaction = cardScript.getTransactions().getTransaction(transactionId);
            }
        }
        if(transactions.isRemembered(transactionId)) {
            rememberedTransaction = transactions.getRememberedTransaction(transactionId);
        }


        if(rememberedTransaction != null) {
            return rememberedTransaction
                    .getMessagesWithSelectedOptions()
                    .stream()
                    .map(TransactionMessage::getGrpcFormat)
                    .collect(Collectors.toList());
        }

        throw new BadRequestException("Finished transaction with this id does not exist: " + transactionId);
    }

}
