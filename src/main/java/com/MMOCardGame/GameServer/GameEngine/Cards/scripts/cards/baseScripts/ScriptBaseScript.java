package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;

import java.util.ArrayList;
import java.util.List;

public class ScriptBaseScript extends CardScript {

    @Override
    public List<GamePhase> getAvailablePhasesForPlay() {
        var list = new ArrayList<GamePhase>();
        list.add(GamePhase.Main);
        list.add(GamePhase.Attack);
        list.add(GamePhase.Defence);
        list.add(GamePhase.EndRound);
        list.add(GamePhase.ReactionPhase);
        return list;
    }

}
