package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.PassiveAbilityCardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;

@Script(ids = 8, type = Script.ScriptType.Card)
public class SiecStalkerowScript extends PassiveAbilityCardScript {

    private static final String PRZESLADOWCA_SUBCLASS = "Prześladowca";
    private static final int ATTACK_INC = 1;

    @Override
    public boolean canDie(){
        return false;
    }
    
    @Override
    protected void addEffectToCard(Card card) {
        if(isPrzesladowcaUnit(card)) {
            changeAttack(card, ATTACK_INC);
        }
    }

    private boolean isPrzesladowcaUnit(Card card) {
        return card.getType().equals(CardType.Unit) && card.getSubclassName().equals(PRZESLADOWCA_SUBCLASS);
    }

    @Override
    protected void removeEffectFromCard(Card card) {
        if(isPrzesladowcaUnit(card)) {
            changeAttack(card, -ATTACK_INC);
        }
    }

    private void changeAttack(Card card, int change) {
        card.getBaseStatistics().setAttack(card.getBaseStatistics().getAttack() + change);
        card.changeAttack(change);
    }
}
