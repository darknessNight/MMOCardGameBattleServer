package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.MessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;

public class OptionsSelectedByClientMessageHandler implements MessageHandler {

    @Override
    public TransactionMessage handle(TransactionMessage request,
                                     TransactionEnvironment transactionEnvironment) {
        var response = request.getResponseTemplate();

        var transactions = transactionEnvironment.getTransactions();
        var transaction = transactionEnvironment.getTransactions().getTransaction(request.getTransactionId());
        var instanceId = transactionEnvironment.getInstanceId();

        handleSelectionAndGoToNextTransactionStep(request, transactionEnvironment, transaction);

        if(transactions.isRunning(transaction.getId())){
            return generateResponseWithOptions(transactionEnvironment.getGameState(), response, transaction, instanceId);
        } else {
            return generateResultResponse(response, transaction);
        }
    }

    private void handleSelectionAndGoToNextTransactionStep(TransactionMessage request, TransactionEnvironment transactionEnvironment, Transaction transaction) {
        validateSelectedTargets(request, transaction);

        var stepEnvironment = new StepEnvironment(transaction, transactionEnvironment.getGameState());
        transaction.handleOptionsSelection(request, stepEnvironment);
        transaction.goToNextStep(transactionEnvironment.getTransactions());
    }

    private void validateSelectedTargets(TransactionMessage request, Transaction transaction) {
        var valid = transaction.validateOptions(request.getOptionsInfo().getOptions());
        if(!valid){
            String message = invalidOptionsMessage(transaction, request);
            throw new BadRequestException(message);
        }
    }

    private TransactionMessage generateResultResponse(TransactionMessage response, Transaction transaction) {
        response.setOptionsInfo(transaction.getLastOptionsSelectedByClient());
        response.setDescription("Result of transaction: " + transaction.getId());
        response.setType(TargetSelectingTransactionType.Result);
        return response;
    }

    private TransactionMessage generateResponseWithOptions(GameState gameState,
                                                           TransactionMessage response,
                                                           Transaction transaction,
                                                           int instanceId) {
        response.setDescription("Step " + transaction.getCurrentStep() + " of transaction: " + transaction.getId());
        var possibleOptions = transaction.getCurrentStepPossibleOptions(gameState, instanceId);
        transaction.setLastOptionsSentToClient(possibleOptions);
        response.setOptionsInfo(possibleOptions);
        response.setType(TargetSelectingTransactionType.AvailableOptions);
        return response;
    }

    private String invalidOptionsMessage(Transaction transaction, TransactionMessage request) {
        return "Invalid options selected by client during transaction. Was: " +
                request.getOptionsInfo() +
                " expected: " +
                transaction.getLastOptionsSentToClient();
    }
}
