package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.UnitBaseScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.PrefixesForReactionStackDescriptions;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;

import java.util.ArrayList;
import java.util.EnumMap;

@Script(ids = 11, type = Script.ScriptType.Card)
public class ArcherScript extends UnitBaseScript {

    private static final int ABILITY_DAMAGE = 1;

    @Override
    protected CardAbilityTask getAbilityAction(GameState gs, int transactionId) {
        return gameState -> {
                var selectedTargets = new ArrayList<TargetSelectingOption>();
                for (var transactionMessage : getSelectedOptionsDuringTransaction(transactionId)){
                    selectedTargets.addAll(transactionMessage.getOptionsList());
                }
                if(selectedTargets.size() != 1){
                    return;
                }
                var selectedTarget = selectedTargets.get(0);
                if(selectedTarget.getTargetType().equals(TargetType.Card)){
                    gameState.getCard(selectedTarget.getId()).changeDefence(-ABILITY_DAMAGE);
                } else if(selectedTarget.getTargetType().equals(TargetType.Player)){
                    gameState.getPlayer(selectedTarget.getId()).changeLife(-ABILITY_DAMAGE);
                }
            };
    }

    @Override
    protected EnumMap getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var abilityTransactionDefinition = getTransactionDefinitionForAbility();
        definitions.put(abilityTransactionDefinition.getStartedFor(), abilityTransactionDefinition);
        definitions.putAll(super.getTransactionsDefinitions());

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForAbility(){
        var abilityTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Ability,
                transactionMessage -> PrefixesForReactionStackDescriptions.CARD_ABILITY + "1");
        abilityTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> player.getUserId() != parentCard.getOwner(),
                (gameState, card) -> card.getOwner() != parentCard.getOwner() &&
                                    (card.getType() == CardType.Unit || card.getType() == CardType.ComputingUnit) &&
                                    gameState.getCardPosition(card.getInstanceId()).getPosition() == CardPosition.Position.Battlefield,
                1));
        return abilityTransactionDefinition;
    }
    
}
