package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.StepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import lombok.Data;

import java.util.List;

@Data
public class RunningTransactionStep {

    private StepDefinition definition;

    public RunningTransactionStep(StepDefinition definition){
        this.definition = definition;
    }

    public OptionsInfo getOptionsInfo(GameState gameState, int instanceId)
    {
        return definition.getPossibleOptionsInfo(gameState, instanceId);
    }

    public void handleSelectedTargets(  List<TargetSelectingOption> selectedTargets,
                                        StepEnvironment stepEnvironment)
    {
        definition.getSelectedOptionsHandler().accept(selectedTargets, stepEnvironment);
    }

}
