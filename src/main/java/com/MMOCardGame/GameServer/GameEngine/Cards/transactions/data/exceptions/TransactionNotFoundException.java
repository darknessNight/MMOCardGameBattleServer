package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions;

public class TransactionNotFoundException extends RuntimeException {
    public TransactionNotFoundException(int id){
        super(Integer.toString(id));
    }

    public TransactionNotFoundException(int id, String message){
        super("transaction id: " + id + " message: " + message);
    }
}
