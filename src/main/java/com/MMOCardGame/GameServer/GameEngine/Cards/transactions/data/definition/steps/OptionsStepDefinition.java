package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.CustomOptionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class OptionsStepDefinition extends StepDefinition {
    private Map<Integer, CustomOptionDefinition> possibleOptions;

    public OptionsStepDefinition(List<CustomOptionDefinition> possibleTargets,
                                 BiFunction<List<TargetSelectingOption>, GameState, Integer> numberOfOptionsProvider) {
        super(TargetSelectingTransaction.OptionsType.Option, numberOfOptionsProvider);
        possibleOptions = new HashMap<>();
        possibleTargets.forEach(option -> possibleOptions.put(option.getId(), option));
    }


    public OptionsStepDefinition(List<CustomOptionDefinition> possibleTargets, int numberOfOptionsToSelect) {
        super(TargetSelectingTransaction.OptionsType.Option, numberOfOptionsToSelect);
        possibleOptions = new HashMap<>();
        possibleTargets.forEach(option -> possibleOptions.put(option.getId(), option));
    }

    @Override
    protected List<TargetSelectingOption> getPossibleOptions(GameState gameState, int instanceId) {
        return possibleOptions.values().stream()
                .filter(o -> o.canBeChosen(gameState, instanceId))
                .map(option ->
                    TargetSelectingOption.newBuilder()
                            .setTargetType(TargetType.UndefinedType)
                            .setDescription(option.getDescription())
                            .setId(option.getId())
                            .build())
                .collect(Collectors.toList());
    }


    @Override
    public BiConsumer<List<TargetSelectingOption>, StepEnvironment> getSelectedOptionsHandler()
    {
        return (selectedOptions, stepEnvironment) ->
        {
            for (var selectedOption : selectedOptions)
            {
                possibleOptions.get(selectedOption.getId()).getActionInvokedWhenSelected().accept(stepEnvironment);
            }
        };
    }

}
