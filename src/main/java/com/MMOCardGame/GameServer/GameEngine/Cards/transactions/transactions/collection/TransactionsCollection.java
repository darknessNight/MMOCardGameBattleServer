package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions.CantStartTransactionException;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions.TransactionNotFoundException;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;

import java.util.HashMap;
import java.util.Map;

public class TransactionsCollection {
    private Map<Integer, Transaction> runningTransactions = new HashMap<>();
    private RememberedTransactions rememberedTransactions = new RememberedTransactions();

    public Transaction getTransaction(int id) {
        if (isRemembered(id)) {
            return rememberedTransactions.getTransaction(id);
        } else if(isRunning(id)) {
            return runningTransactions.get(id);
        }
        throw new TransactionNotFoundException(id);
    }

    public boolean isRunning(int id)
    {
        return runningTransactions.containsKey(id);
    }

    public boolean isRemembered(int id) {
        return rememberedTransactions.contains(id);
    }

    public Transaction getRememberedTransaction(int id){
        if(isRemembered(id)){
            return rememberedTransactions.getTransaction(id);
        }
        throw new TransactionNotFoundException(id, "searched in finished transactions.");
    }

    public Transaction tryStartAndGetNewTransaction(TransactionDefinition definition)
    {
        if(!definition.hasAnySteps()){
            throw new CantStartTransactionException();
        }
        var newTransaction = new Transaction(definition);
        runningTransactions.put(newTransaction.getId(), newTransaction);
        return newTransaction;
    }

    public void finishTransaction(int id)
    {
        var transactionToFinish = runningTransactions.get(id);
        rememberedTransactions.rememberTransaction(transactionToFinish);
        runningTransactions.remove(id);
    }

    public void abortTransaction(int id)
    {
        runningTransactions.remove(id);
    }

    public void rememberEmptyTransaction(TransactionMessage notDefinedResponse) {
        rememberedTransactions.rememberTransaction(Transaction.getEmpty(notDefinedResponse));
    }
}
