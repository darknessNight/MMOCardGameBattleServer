package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.Data;

import java.util.function.BiFunction;
import java.util.function.Consumer;

@Data
public class CustomOptionDefinition {
    private int id;
    private String description;
    private Consumer<StepEnvironment> actionInvokedWhenSelected;
    private BiFunction<GameState, Integer, Boolean> canBeChosen;

    public CustomOptionDefinition(String description, BiFunction<GameState, Integer, Boolean> canBeChosen){
        id = generateId();
        this.actionInvokedWhenSelected = e -> {};
        this.description = description;
        this.canBeChosen = canBeChosen;
    }

    public CustomOptionDefinition(String description){
        this(description, e -> {});
    }

    public CustomOptionDefinition(String description,
                                  Consumer<StepEnvironment> actionInvokedWhenSelected){
        id = generateId();
        this.description = description;
        this.actionInvokedWhenSelected = actionInvokedWhenSelected;
        canBeChosen = (gs, id) -> true;
    }

    public boolean canBeChosen(GameState gameState, int instanceId){
        return canBeChosen.apply(gameState, instanceId);
    }

    private static int optionId = 1;

    private static int generateId(){
        return optionId++;
    }

}
