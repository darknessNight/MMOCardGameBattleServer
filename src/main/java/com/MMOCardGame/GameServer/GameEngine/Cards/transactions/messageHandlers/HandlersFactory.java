package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations.AbortMessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations.OptionsSelectedByClientMessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations.StartTransactionForTriggerMessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations.StartTransactionMessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;

public class HandlersFactory {

    public static MessageHandler produce(TransactionMessage request){

        var type = request.getType();
        var startedFor = request.getStartedFor();

        switch(type){
            case SelectingOption:
                return new OptionsSelectedByClientMessageHandler();
            case Abort:
                return new AbortMessageHandler();
            case StartTransaction:
                return getStartTransactionMessageHandler(type, startedFor);
            default:
                throw new BadRequestException("Invalid transaction type received " + type);
        }

    }

    private static MessageHandler getStartTransactionMessageHandler(TargetSelectingTransactionType type, TargetSelectingTransaction.StartTransactionFor startedFor) {
        switch(startedFor){
            case Trigger:
                return new StartTransactionForTriggerMessageHandler();
            case Play: case Attack:
            case Defend: case Ability:
                return new StartTransactionMessageHandler();
            default:
                throw new BadRequestException("Invalid transaction type received " + type);
        }
    }

}
