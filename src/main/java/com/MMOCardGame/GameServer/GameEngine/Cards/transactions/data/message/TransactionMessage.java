package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import lombok.Data;

@Data
public class TransactionMessage {

    private int transactionId;
    private TargetSelectingTransactionType type;
    private TargetSelectingTransaction.StartTransactionFor startedFor;
    private String description;
    private OptionsInfo optionsInfo = new OptionsInfo();
    private int destinationTargetId;
    private TargetType destinationTargetType;

    public TransactionMessage(){

    }

    public TransactionMessage(TargetSelectingTransaction grpcMessage){
        description = grpcMessage.getDescription();
        destinationTargetId = grpcMessage.getDestinationTargetId();
        destinationTargetType = grpcMessage.getDestinationTargetType();
        transactionId = grpcMessage.getTransactionId();
        startedFor = grpcMessage.getTransactionStartedFor();
        type = grpcMessage.getType();

        optionsInfo = new OptionsInfo();
        optionsInfo.setOptions(grpcMessage.getOptionsList());
        optionsInfo.setCountOfTargets(grpcMessage.getCountOfOptionsToPick());
        optionsInfo.setOptionsType(grpcMessage.getOptionsType());
    }

    public TargetSelectingTransaction getGrpcFormat(){
        var result = TargetSelectingTransaction.newBuilder();

        if(type != null) result.setType(type);
        if(description != null) result.setDescription(description);
        result.setTransactionId(transactionId);
        if(startedFor != null) result.setTransactionStartedFor(startedFor);
        result.setDestinationTargetId(destinationTargetId);
        if(destinationTargetType != null) result.setDestinationTargetType(destinationTargetType);

        result.setCountOfOptionsToPick(optionsInfo.getCountOfTargets());
        if(optionsInfo.getOptions() != null) result.addAllOptions(optionsInfo.getOptions());
        if(optionsInfo.getOptionsType() != null &&
                !optionsInfo.getOptionsType().equals(TargetSelectingTransaction.OptionsType.UNRECOGNIZED))
            result.setOptionsType(optionsInfo.getOptionsType());

        return result.build();
    }

    public TransactionMessage getResponseTemplate(){
        var responseTemplate = new TransactionMessage();
        responseTemplate.transactionId = this.transactionId;
        responseTemplate.destinationTargetId = this.destinationTargetId;
        responseTemplate.destinationTargetType = this.destinationTargetType;
        responseTemplate.startedFor = this.startedFor;
        responseTemplate.optionsInfo = new OptionsInfo();
        return responseTemplate;
    }

}
