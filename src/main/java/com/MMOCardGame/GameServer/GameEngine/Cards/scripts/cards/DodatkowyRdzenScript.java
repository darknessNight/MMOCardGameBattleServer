package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities.ZainstalujNaJednostceScript;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import com.MMOCardGame.GameServer.common.CallbackTask;

import java.util.ArrayList;

@Script(ids = 13, type = Script.ScriptType.Card)
public class DodatkowyRdzenScript extends ZainstalujNaJednostceScript {

    private static final int DEFENCE_INC = 1;
    private static final int ATTACK_INC = 1;

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState.moveCardToBattlefield(parentCard.getInstanceId());
                saveCardWhichIamInstalled(transactionId);
                var selectedTargets = new ArrayList<TargetSelectingOption>();
                for (var transactionMessage : getSelectedOptionsDuringTransaction(transactionId)){
                    selectedTargets.addAll(transactionMessage.getOptionsList());
                }
                if(selectedTargets.size() != 1){
                    return;
                }
                var selectedTarget = selectedTargets.get(0);
                if(selectedTarget.getTargetType().equals(TargetType.Card)){
                    var card = gameState.getCard(installedOnCardInstanceId);
                    changeAttack(card, ATTACK_INC);
                    changeDefence(card, DEFENCE_INC);
                }
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    private void changeAttack(Card card, int value){
        card.getBaseStatistics().setAttack(card.getBaseStatistics().getAttack() + value);
        card.changeAttack(value);
    }

    private void changeDefence(Card card, int value){
        card.getBaseStatistics().setDefence(card.getBaseStatistics().getDefence() + value);
        card.changeDefence(value);
    }

    @Override
    public CallbackTask<GameState> getDeathAction() {
        return gameState -> {
                var card = gameState.getCard(installedOnCardInstanceId);
                changeAttack(card, -ATTACK_INC);
                changeDefence(card, -DEFENCE_INC);
            };
    }

}
