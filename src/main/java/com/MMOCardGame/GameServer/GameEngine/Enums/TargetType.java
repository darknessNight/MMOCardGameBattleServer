package com.MMOCardGame.GameServer.GameEngine.Enums;

public enum TargetType {
    UndefinedType,
    Player,
    Card,
}
