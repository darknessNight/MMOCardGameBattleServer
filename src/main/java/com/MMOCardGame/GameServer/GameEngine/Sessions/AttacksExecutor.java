package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.AttackExecution;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class AttacksExecutor {
    private GameState gameState;
    private Set<UserConnection> activeConnections;
    private List<Pair<Integer, Pair<Integer, TargetType>>> fightsToExecute = null;
    private Iterator<Pair<Integer, Pair<Integer, TargetType>>> fightsToExecuteIterator = null;
    private Set<Card> cardsWithAddedOverload = new HashSet<>();
    private boolean preparedInThisRound = false;

    public AttacksExecutor(GameState gameState, Set<UserConnection> activeConnections) {
        this.gameState = gameState;
        this.activeConnections = activeConnections;
    }


    public void prepareAttacks(Integer attackerPlayerId) {
        if (preparedInThisRound) return;
        preparedInThisRound = true;
        var defendersPlayers = gameState.getListOfActiveUsersIds().stream()
                .filter(integer -> !integer.equals(attackerPlayerId))
                .collect(Collectors.toList());

        var defendersCards = getDefendersCards(defendersPlayers);
        var attackersCardsIterator = getAttackersCardsIterator(attackerPlayerId);
        fightsToExecute = getFightsForAttackers(defendersCards, attackersCardsIterator);
        fightsToExecuteIterator = fightsToExecute.iterator();
    }

    private List<Pair<Integer, Pair<Integer, TargetType>>> getFightsForAttackers(Map<Integer, List<Pair<Integer, Integer>>> defendersCards, Iterator<Pair<Integer, Pair<Integer, TargetType>>> attackersCardsIterator) {
        List<Pair<Integer, Pair<Integer, TargetType>>> fights = new ArrayList<>();

        while (attackersCardsIterator.hasNext()) {
            var attacker = attackersCardsIterator.next();
            addFightsForAttacker(defendersCards, fights, attacker);
        }
        return fights;
    }

    private void addFightsForAttacker(Map<Integer, List<Pair<Integer, Integer>>> defendersCards, List<Pair<Integer, Pair<Integer, TargetType>>> fights, Pair<Integer, Pair<Integer, TargetType>> attacker) {
        int attackerId = attacker.getFirst();
        if (defendersCards.containsKey(attackerId)) {
            addDefendingFightAndRemoveDefenderFromList(defendersCards, fights, attacker, attackerId);
        } else {
            fights.add(attacker);
        }
    }

    private void addDefendingFightAndRemoveDefenderFromList(Map<Integer, List<Pair<Integer, Integer>>> defendersCards, List<Pair<Integer, Pair<Integer, TargetType>>> fights, Pair<Integer, Pair<Integer, TargetType>> attacker, int attackerId) {
        var defendersForAttacker = defendersCards.get(attackerId);
        int defenderId = defendersForAttacker.get(0).getFirst();
        defendersForAttacker.remove(defendersForAttacker.get(0));
        if (defendersForAttacker.isEmpty()) {
            defendersCards.remove(attackerId);
            if (gameState.getCard(attackerId).getCardScript().isAlwaysAttackingTarget())
                fights.add(attacker);
        }
        fights.add(new Pair<>(attackerId, new Pair<>(defenderId, TargetType.Card)));
    }

    private Iterator<Pair<Integer, Pair<Integer, TargetType>>> getAttackersCardsIterator(Integer attackerPlayerId) {
        return gameState.getAttackersForPlayer(attackerPlayerId)
                .entrySet().stream()
                .map(entry -> new Pair<>(entry.getKey(), gameState.
                        getCard(entry.getKey())
                        .getCardScript()
                        .getSelectedOptionsDuringTransaction(entry.getValue()))
                )
                .flatMap(pair -> pair.getSecond()
                        .stream()
                        .flatMap(transaction -> transaction.getOptionsList().stream()
                                .map(option -> new Pair<>(pair.getFirst(), new Pair<>(option.getId(), option.getTargetType())))
                        )
                )
                .iterator();
    }

    private Map<Integer, List<Pair<Integer, Integer>>> getDefendersCards(List<Integer> defendersPlayers) {
        return defendersPlayers.stream()
                .flatMap(integer -> gameState.getAttackersForPlayer(integer).entrySet().stream())
                .map(entry -> new Pair<>(entry.getKey(), gameState.
                        getCard(entry.getKey())
                        .getCardScript()
                        .getSelectedOptionsDuringTransaction(entry.getValue()))
                ).flatMap(pair -> pair.getSecond()
                        .stream()
                        .flatMap(transaction -> transaction.getOptionsList().stream()
                                .map(option -> new Pair<>(option.getId(), option.getTargetType()))
                        )
                        .filter(target -> target.getSecond().equals(TargetType.Card))
                        .map(target -> new Pair<>(pair.getFirst(), target.getFirst()))
                )
                .collect(Collectors.groupingBy(Pair::getSecond));
    }

    public void endPhaseIfNoMoreAttacks() {
        if (fightsToExecuteIterator == null || !fightsToExecuteIterator.hasNext())
            gameState.getPhasesStack().endTechnicalPhase();
    }

    public void executeAttacksWhileInPhase() {
        if (isNotCorrectState())
            return;
        performAttackWhilePossible();
    }

    private void performAttackWhilePossible() {
        while (isPossibleToPerformAttack()) {
            var fight = fightsToExecuteIterator.next();
            notifyAllAboutAttack(fight);
            performAttack(fight);
        }
    }

    private boolean isPossibleToPerformAttack() {
        return gameState.getCurrentPhase().equals(GamePhase.AttackExecution) && fightsToExecuteIterator.hasNext();
    }

    private boolean isNotCorrectState() {
        if (fightsToExecute == null
                || fightsToExecuteIterator == null)
            return true;
        if (!fightsToExecuteIterator.hasNext()) {
            fightsToExecuteIterator = null;
            fightsToExecute = null;
            return true;
        }
        return false;
    }

    private void performAttack(Pair<Integer, Pair<Integer, TargetType>> fight) {
        var attackerCard = gameState.getCard(fight.getFirst());
        var attackerId = fight.getFirst();
        var defenderId = fight.getSecond().getFirst();
        if (fight.getSecond().getSecond().equals(TargetType.Card)) {
            performAttackCardCard(fight, attackerCard, attackerId, defenderId);
        } else {
            performAttackCardPlayer(fight, attackerCard, defenderId);
        }
        addOverloadPointsToCardsIfNeeded(attackerCard, attackerCard.getCardScript().getAttackOverloadCost());
    }

    private void performAttackCardPlayer(Pair<Integer, Pair<Integer, TargetType>> fight, Card attackerCard, Integer defenderId) {
        var defenderPlayer = gameState.getPlayer(fight.getSecond().getFirst());
        var attackMultiplier = attackerCard.getCardScript().getAttackMultiplier(defenderId, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Player);
        int attackPower = (int) (attackerCard.getAttack() * attackMultiplier);
        defenderPlayer.changeLife(-attackPower);
    }

    private void performAttackCardCard(Pair<Integer, Pair<Integer, TargetType>> fight, Card attackerCard, Integer attackerId, Integer defenderId) {
        Card defenderCard = decreaseDefenderDefence(attackerCard, defenderId);
        decreaseAttackerDefence(attackerCard, attackerId, defenderCard);
        addOverloadPointsToCardsIfNeeded(defenderCard, defenderCard.getCardScript().getDefendOverloadCost());
    }

    private void addOverloadPointsToCardsIfNeeded(Card card, int cost) {
        if (!cardsWithAddedOverload.contains(card)) {
            cardsWithAddedOverload.add(card);
            card.changeOverloadPoints(cost);
        }
    }

    private void decreaseAttackerDefence(Card attackerCard, Integer attackerId, Card defenderCard) {
        decreaseLifeOfCard(defenderCard, attackerId, attackerCard);
    }

    private Card decreaseDefenderDefence(Card attackerCard, Integer defenderId) {
        var defenderCard = gameState.getCard(defenderId);
        decreaseLifeOfCard(attackerCard, defenderId, defenderCard);
        return defenderCard;
    }

    private void decreaseLifeOfCard(Card attackerCard, Integer defenderId, Card defenderCard) {
        var attackMultiplier = attackerCard.getCardScript().getAttackMultiplier(defenderId, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        int attackPower = (int) (attackerCard.getAttack() * attackMultiplier);
        defenderCard.changeDefence(-attackPower);
    }

    private void notifyAllAboutAttack(Pair<Integer, Pair<Integer, TargetType>> fight) {
        var message = AttackExecution.newBuilder()
                .setAttackerId(fight.getFirst())
                .setDefenderId(fight.getSecond().getFirst())
                .setDefenderType(fight.getSecond().getSecond())
                .build();
        activeConnections.forEach(userConnection -> userConnection.sendMessage(message));
    }

    public void cleanGameState() {
        gameState.getListOfActiveUsersIds().forEach(
                integer -> gameState.getAttackersForPlayer(integer).clear()
        );
        cardsWithAddedOverload.clear();
        preparedInThisRound = false;
    }
}
