package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameReferee;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerDefeatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.GameEngine.factories.GameRefereeBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GameSessionController extends GameSession {
    EventsController eventsController;
    protected GameReferee gameReferee;

    public GameSessionController(GameSessionSettings gameSessionSettings, ObservableNotifier<UserSessionEndInfo> sessionEndObservable,
                                 GameSessionManager gameSessionManager, GameStateBuilder gameStateBuilder, GameRefereeBuilder gameRefereeBuilder) {
        super(gameSessionSettings, sessionEndObservable, gameSessionManager, gameStateBuilder, gameRefereeBuilder);
        eventsController = new EventsController(gameState, activeConnections, this);
        eventsController.setObserversOnGameSessionObjects();
        this.gameReferee = gameRefereeBuilder.setGameSettings(gameSessionSettings).setGameSession(this).build();
        gameState.getTriggers().setReactionStackController(eventsController.getReactionStackController());
        gameReferee.setReactionStackController(eventsController.getReactionStackController());
    }

    @Override
    public void registerConnection(UserConnection connection) {
        super.registerConnection(connection);
        gameReferee.addConnection(connection);
    }

    @Override
    public GameState getAllGameState() {
        return gameState;
    }

    @Override
    public void surrender(int player) {
        gameState.getTriggers().invokeTrigger(new PlayerDefeatedEvent(player));
    }

    @Override
    public CardUsed useCard(CardUsed message, int userId) {
        if (gameState.getCurrentPlayer() == userId) {
            CardUsed result = gameReferee.useCardIfPossible(message, userId);
            if (result == null)
                throw new BadRequestException();
            return result;
        } else throw new InconsistentPlayerCommand();
    }

    @Override
    public void endPhase(int player, PhaseChanged message) throws InconsistentPlayerCommand {
        if (isUserInFirstHandDrawPhase(player))
            keepStartingHand(player);
        else {
            doEndPhase(player, message);
        }
    }

    private void doEndPhase(int player, PhaseChanged message) {
        checkIsPlayerTryEndCorrectPhase(player, message);
        gameState.endPhase(player);
    }

    private void checkIsPlayerTryEndCorrectPhase(int player, PhaseChanged message) {
        if (message.getOldPhaseValue() != gameState.getCurrentPhase().ordinal()
                || (player != gameState.getCurrentPlayer() && gameState.getCurrentPhase() != GamePhase.Waiting
                && gameState.getCurrentPhase() != GamePhase.FirstHandDraw))
            throw new InconsistentPlayerCommand();
    }

    @Override
    public Integer useHeroAbility(HeroAbilityUsed message, CardCreated.Builder cardCreatedOrBuilder) {
        return null;
    }

    @Override
    public void selectAttackersDefenders(int player, AttackersDefendersSelected message) {
        checkIsCorrectAttackersDefendersMessage(player, message);
        Map<Integer, Integer> attackers = getAttackersMap(player, message).entrySet().stream()
                .filter(entry->{
                    var card=gameState.getCard(entry.getKey());
                    if(card==null)
                        return false;
                    return card.getOverloadPoints() <= 0;
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        notifyAllAboutSelectingAttackers(attackers);
        saveAttackersMapAndEndPhase(player, attackers);
    }

    private void notifyAllAboutSelectingAttackers(Map<Integer, Integer> attackers) {
        var notifyMessage= AttackersDefendersSelected.newBuilder();
        addAttackersToMessage(attackers, notifyMessage);
        activeConnections.forEach(userConnection -> userConnection.sendMessage(notifyMessage.build()));
    }

    private void addAttackersToMessage(Map<Integer, Integer> attackers, AttackersDefendersSelected.Builder notifyMessage) {
        for (var attacker: attackers.entrySet()) {
            var cardScript=gameState.getCard(attacker.getKey()).getCardScript();
            var transaction=cardScript.getSelectedOptionsDuringTransaction(attacker.getValue());
            addAttackerInfoToMessage(notifyMessage, attacker, transaction);
        }
    }

    private void addAttackerInfoToMessage(AttackersDefendersSelected.Builder notifyMessage, Map.Entry<Integer, Integer> attacker, Collection<TargetSelectingTransaction> transaction) {
        notifyMessage.addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                .setAttackerId(attacker.getKey())
                .setTransactionId(attacker.getValue())
                .addAllTransaction(transaction)
                .build());
    }

    private void saveAttackersMapAndEndPhase(int player, Map<Integer, Integer> attackers) {
        gameState.getAttackersForPlayer(player).putAll(attackers);
        gameState.endPhase(player);
    }

    private Map<Integer, Integer> getAttackersMap(int player, AttackersDefendersSelected message) {
        Map<Integer, Integer> attackers=new HashMap<>();

        for (var target: message.getTargetsList()) {
            ifCorrectCardPutToAttackersMap(player, attackers, target);
        }
        return attackers;
    }

    private void ifCorrectCardPutToAttackersMap(int player, Map<Integer, Integer> attackers, AttackersDefendersSelected.AttackDefenceTarget target) {
        int attackerId=target.getAttackerId();
        int cardOwner=gameState.getCardOwner(attackerId);
        if(cardOwner!=player)
            throw new BadRequestException();
        attackers.put(attackerId,target.getTransactionId());
    }

    private void checkIsCorrectAttackersDefendersMessage(int player, AttackersDefendersSelected message) {
        checkIsPlayerInDefendOrAttackPhase(player);
    }

    private void checkIsPlayerInDefendOrAttackPhase(int player) {
        if (!isPlayerInDefendOrAttackPhase(player))
            throw new InconsistentPlayerCommand();
    }

    private boolean isPlayerInDefendOrAttackPhase(int player) {
        return (gameState.getCurrentPhase() == GamePhase.Attack || gameState.getCurrentPhase() == GamePhase.Defence)
                && gameState.getCurrentPlayer() == player;
    }

    @Override
    public HandChanged drawCards(HandChanged message) {
        return null;
    }

    @Override
    public HandChanged discardCards(HandChanged message) {
        return null;
    }

    @Override
    public void muligan(int player) {
        checkIsUserInFirstHandDrawPhase(player);
        gameReferee.muligan(player);
    }

    @Override
    public void replaceCards(HandChanged message) {
        checkIsUserInFirstHandDrawPhase(message.getPlayer());
        if ((message.getCount() != 0 && message.getCount() != message.getCardInstancesList().size())
                || message.getCardInstancesList().size() == 0)
            throw new InconsistentPlayerCommand();
        gameReferee.replaceCards(message);
    }

    private void checkIsUserInFirstHandDrawPhase(int player) {
        if (!isUserInFirstHandDrawPhase(player))
            throw new InconsistentPlayerCommand();
    }

    private boolean isUserInFirstHandDrawPhase(int player) {
        return gameState.getCurrentPhase() == GamePhase.FirstHandDraw
                && !gameState.getPlayer(player).isAcceptedHand();
    }

    @Override
    public void keepStartingHand(int player) {
        checkIsUserInFirstHandDrawPhase(player);
        gameState.endPhase(player);
        gameState.getPlayers().get(player).acceptHand();
    }

    @Override
    public void doTriggerReaction(TriggerActivated message) {
        checkIsTriggerPhase();
        Card card = gameState.getCard(message.getCardInstanceId());
        checkTriggerReactionMessageIsCorrect(message, card);
        var triggerType = TriggerType.values()[message.getTriggerType().ordinal()];

        card.getCardScript().startTriggerActions(triggerType, message.getTransactionId());
    }

    private void checkTriggerReactionMessageIsCorrect(TriggerActivated message, Card card) {
        if (card == null || message.getTransactionId() == -1 || message.getTriggerType().equals(com.MMOCardGame.GameServer.Servers.ProtoBuffers.TriggerType.UndefinedTriggerType_))
            throw new BadRequestException("Incorrect message format");
    }

    private void checkIsTriggerPhase() {
        if (!gameState.getCurrentPhase().equals(GamePhase.TriggerPhase))
            throw new InconsistentPlayerCommand();
    }

    @Override
    public TargetSelectingTransaction doTargetSelectingTransaction(TargetSelectingTransaction message) {
        checkIsCorrectTransaction(message);
        return runTransactionOnTarget(message);
    }

    private TargetSelectingTransaction runTransactionOnTarget(TargetSelectingTransaction message) {
        if (message.getDestinationTargetType().equals(TargetType.Card))
            return runTransactionOnCard(message);
        else throw new BadRequestException();
    }

    private TargetSelectingTransaction runTransactionOnCard(TargetSelectingTransaction message) {
        Card card = gameState.getCard(message.getDestinationTargetId());
        if (card == null)
            throw new NotFoundException();
        return card.getCardScript().runTransactionAction(message, gameState);
    }

    private void checkIsCorrectTransaction(TargetSelectingTransaction message) {
        var allowedTypes = Arrays.asList(TargetSelectingTransactionType.StartTransaction, TargetSelectingTransactionType.SelectingOption, TargetSelectingTransactionType.Abort);
        var allowedTargetTypes = Arrays.asList(TargetType.Card);

        if (!allowedTypes.contains(message.getType())
                || !allowedTargetTypes.contains(message.getDestinationTargetType())
                || message.getDestinationTargetId() == 0)
            throw new BadRequestException();
    }
}
