package com.MMOCardGame.GameServer.GameEngine.Sessions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSessionEndInfo {
    public Result result;
    public String additionalInfo;
    public String userLogin;
    public int sessionId;

    public enum Result{
        WIN,
        LOST,
        KICKED
    }
}
