package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;

public interface GameSessionService {
    GameState getAllGameState();

    void surrender(int player);

    CardUsed useCard(CardUsed message, int userId);

    void endPhase(int player, PhaseChanged message) throws InconsistentPlayerCommand;

    Integer useHeroAbility(HeroAbilityUsed message, CardCreated.Builder cardCreatedOrBuilder);

    void selectAttackersDefenders(int userId, AttackersDefendersSelected message);

    HandChanged drawCards(HandChanged message);

    HandChanged discardCards(HandChanged message);

    void muligan(int player);

    void replaceCards(HandChanged message);

    void keepStartingHand(int player);

    void doTriggerReaction(TriggerActivated message);

    void dumpToFile(Throwable throwable);

    TargetSelectingTransaction doTargetSelectingTransaction(TargetSelectingTransaction message);
}
