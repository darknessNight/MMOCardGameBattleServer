package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.factories.GameRefereeBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.ProgramConstants;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public abstract class GameSession implements GameSessionService {
    protected Logger logger = LoggerFactory.getLogger(GameSession.class);
    @Getter
    protected Set<UserConnection> activeConnections = ConcurrentHashMap.newKeySet();
    protected GameState gameState;
    protected ObservableNotifier<UserSessionEndInfo> sessionEndObservable;
    protected int sessionId;
    GameSessionManager gameSessionManager;

    public GameSession(GameSessionSettings gameSessionSettings, ObservableNotifier<UserSessionEndInfo> sessionEndObservable,
                       GameSessionManager gameSessionManager, GameStateBuilder gameStateBuilder, GameRefereeBuilder gameRefereeBuilder) {
        this.gameState = gameStateBuilder.setGameSessionSettings(gameSessionSettings).build();
        this.sessionId = gameSessionSettings.getSessionId();
        this.sessionEndObservable = sessionEndObservable;
        this.gameSessionManager = gameSessionManager;
    }

    public void registerConnection(UserConnection connection) {
        checkIsActiveUser(connection);
        connection.registerSessionService(this);
        activeConnections.add(connection);
        connection.setUserId(gameState.getPlayerId(connection.getUserName()));
    }

    protected void checkIsActiveUser(UserConnection connection) {
        if (!gameState.getListOfActiveUsers().contains(connection.getUserName()))
            throw new NotFoundException("Not found user: " + connection.getUserName() + " in list of active users");
    }

    public void removeConnection(UserConnection connection) {
        if (!activeConnections.contains(connection))
            throw new NotFoundException("Connection is not part of this session");
        connection.kickPlayer("Undefined reason");
        removeUserFromSession(connection, UserSessionEndInfo.Result.KICKED, "Undefined reason");
    }

    protected void removeUserFromSession(UserConnection connection, UserSessionEndInfo.Result result, String reason) {
        connection.removeSessionService();
        gameState.setUserAsInactive(connection.getUserName());
        activeConnections.remove(connection);
        gameSessionManager.removeConnectionOnSessionDemand(connection);
        sendSessionEndInfo(connection.getUserName(), result, reason);
    }

    protected void sendSessionEndInfo(String userName, UserSessionEndInfo.Result result, String reason) {
        UserSessionEndInfo info = new UserSessionEndInfo();
        info.result = result;
        info.userLogin = userName;
        info.additionalInfo = reason;
        info.sessionId = sessionId;

        sessionEndObservable.notifyAll(info);
    }

    public void terminate() {
        activeConnections.forEach((userConnection -> removeUserFromSession(userConnection, UserSessionEndInfo.Result.KICKED, "Session terminates")));
    }

    @Override
    public void dumpToFile(Throwable causedBy) {
        String dumpFilename = Instant.now().toString() + "_" + (new Random()).nextInt() + ".dump";
        try (FileOutputStream fileOutputStream = new FileOutputStream(ProgramConstants.getDumpDir() + "/" + dumpFilename)) {
            dumpToStream(causedBy, fileOutputStream);
        } catch (IOException e) {
            logger.error("Cannot write dump to file", e);
        }
    }

    public void dumpToStream(Throwable causedBy, OutputStream stream) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(stream)) {
            GameSessionDump gameSessionDump = new GameSessionDump();
            gameSessionDump.setGameState(gameState);
            gameSessionDump.setConnectedUsers(activeConnections.stream().map(UserConnection::getUserName).collect(Collectors.toSet()));
            gameSessionDump.setReason(causedBy.getClass().getTypeName() + ": " + causedBy.getMessage());

            objectOutputStream.writeObject(gameSessionDump);
        } catch (IOException e) {
            logger.error("Cannot create game session dump", e);
        }
    }
}
