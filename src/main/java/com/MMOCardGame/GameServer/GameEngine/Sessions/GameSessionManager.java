package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.Observer;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GameSessionManager {
    private final Logger logger = LoggerFactory.getLogger(GameSessionManager.class);
    private final ObservableNotifier<UserSessionEndInfo> sessionEndObservable = new ObservableNotifier<>();
    private final Map<String, GameSession> userGameSessionMap = new ConcurrentHashMap<>();
    private final GameSessionFactory gameSessionFactory;

    public GameSessionManager(GameSessionFactory gameSessionFactory) {
        this.gameSessionFactory = gameSessionFactory;
    }

    public synchronized void endSessions() {
        userGameSessionMap.forEach((s, gameSession) -> gameSession.terminate());
        userGameSessionMap.clear();
    }

    public synchronized void registerConnectionToSession(UserConnection connection) throws NotFoundException {
        checkExistsSessionForUser(connection);
        registerConnectionInSession(connection);
    }

    private void registerConnectionInSession(UserConnection connection) {
        userGameSessionMap.get(connection.getUserName()).registerConnection(connection);
    }

    private void checkExistsSessionForUser(UserConnection connection) {
        if (!userGameSessionMap.containsKey(connection.getUserName()))
            throw new NotFoundException();
    }

    public synchronized void removeConnectionFromSession(UserConnection connection) {
        checkExistsSessionForUser(connection);
        userGameSessionMap.get(connection.getUserName()).removeConnection(connection);
        userGameSessionMap.remove(connection.getUserName());
    }

    protected synchronized void removeConnectionOnSessionDemand(UserConnection connection) {
        checkExistsSessionForUser(connection);
        userGameSessionMap.remove(connection.getUserName());
    }

    public synchronized void registerNewGameSession(GameSessionSettings gameSessionSettings) {
        try {
            tryRegisterNewGameSession(gameSessionSettings);
        } catch (Throwable e) {
            logger.error("Exception during session initialization", e);
            throw e;
        }
    }

    private void tryRegisterNewGameSession(GameSessionSettings gameSessionSettings) {
        GameSession gameSession = gameSessionFactory.create(gameSessionSettings, sessionEndObservable, this);
        for (String username : gameSessionSettings.getListOfUsers()) {
            userGameSessionMap.put(username, gameSession);
        }
    }

    public void addUserSessionEndCallback(Observer<UserSessionEndInfo> callback) {
        sessionEndObservable.addObserver(callback);
    }

    public void removeUserSessionEndCallback(Observer<UserSessionEndInfo> callback) {
        sessionEndObservable.removeObserver(callback);
    }

    public void dumpAllSessions() {
        userGameSessionMap.values().stream().distinct().forEach(gameSession -> gameSession.dumpToFile(new Exception("Dump on demand")));
    }
}
