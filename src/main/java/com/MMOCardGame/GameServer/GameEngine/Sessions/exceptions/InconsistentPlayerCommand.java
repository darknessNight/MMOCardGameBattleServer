package com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions;

public class InconsistentPlayerCommand extends RuntimeException {
    public InconsistentPlayerCommand() {
    }

    public InconsistentPlayerCommand(String message) {
        super(message);
    }

    public InconsistentPlayerCommand(String message, Throwable cause) {
        super(message, cause);
    }

    public InconsistentPlayerCommand(Throwable cause) {
        super(cause);
    }
}
