package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TriggerActivatedEvent extends TriggerEvent {
    private final Class<? extends TriggerEvent> eventType;

    public TriggerActivatedEvent(Class<? extends TriggerEvent> eventType) {
        this.eventType = eventType;
    }
}
