package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerLinkPointsChangedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface PlayerLinkPointsChangedScript {
    void playerLinkPointsChanged(PlayerLinkPointsChangedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.PlayerLinkPointsChanged;
    }
}
