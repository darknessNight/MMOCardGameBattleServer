package com.MMOCardGame.GameServer.GameEngine.Triggers;

import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.ReactionStackController;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerActivatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerEvent;
import com.MMOCardGame.GameServer.common.CallbackTask2;
import com.MMOCardGame.GameServer.common.Observable;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.ReflectionHelper;
import lombok.EqualsAndHashCode;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@EqualsAndHashCode(callSuper = true)
public class Triggers extends TriggersMechanism {
    protected transient static Set<TriggerCacheElement> cachedTriggersTypes = null;
    protected transient Map<Class<?>, Trigger> eventsMap = new HashMap<>();
    @Setter
    protected transient ReactionStackController reactionStackController = null;
    private AtomicInteger aliveTriggers = new AtomicInteger(0);
    private transient ObservableNotifier<Boolean> observableNotifier = new ObservableNotifier<>();

    public Triggers(GameState gameState) {
        super(gameState);
        if (cachedTriggersTypes == null)
            findAndCacheTriggersTypes();
        cachedTriggersTypes.forEach(this::addTrigger);
    }

    private void findAndCacheTriggersTypes() {
        ReflectionHelper reflectionHelper = new ReflectionHelper("com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts");
        cachedTriggersTypes = reflectionHelper.findAnnatatedTypes(TriggerScript.class)
                .stream()
                .map(aClass -> new TriggerCacheElement(aClass))
                .filter(triggerCacheElement -> triggerCacheElement.task != null)
                .collect(Collectors.toSet());
    }

    private <U, E extends TriggerEvent> void addTrigger(TriggerCacheElement<U, E> cacheElement) {
        addTrigger(cacheElement.task, cacheElement.eventType, cacheElement.scriptType);
    }

    private <U, E extends TriggerEvent> void addTrigger(CallbackTask2<U, E> task, Class<E> eventType, Class<U> triggerType) {
        Trigger<U, E> trigger = new Trigger<>(task);
        triggersMap.put(triggerType, trigger);
        eventsMap.put(eventType, trigger);
    }

    @Override
    public String toString() {
        return "Triggers{" +
                "triggersMap=" + triggersMap +
                ", gameState=" + gameState +
                '}';
    }

    public <E extends TriggerEvent> void invokeTrigger(E event) {
        aliveTriggers.incrementAndGet();
        runTrigger(event);
        informIfTriggersStopped(aliveTriggers.decrementAndGet());
    }

    private <E extends TriggerEvent> void runTrigger(E event) {
        prepareEvent(event);
        if (eventsMap.containsKey(event.getClass())) {
            eventsMap.get(event.getClass()).run(event);
            triggerActivated(event.getClass());
        }
    }

    private <E extends TriggerEvent> void prepareEvent(E event) {
        event.setGameState(gameState);
        event.setReactionStackController(reactionStackController);
    }

    private void triggerActivated(Class<? extends TriggerEvent> eventType) {
        if (eventType.equals(TriggerActivatedEvent.class)) return;
        invokeTrigger(new TriggerActivatedEvent(eventType));
    }


    private void informIfTriggersStopped(int decrementAndGet) {
        if (decrementAndGet <= 0)
            observableNotifier.notifyAll(true);
    }

    public Observable<Boolean> getObservable() {
        return this.observableNotifier;
    }
}