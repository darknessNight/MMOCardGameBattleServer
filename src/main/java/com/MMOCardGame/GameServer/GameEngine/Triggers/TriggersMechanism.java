package com.MMOCardGame.GameServer.GameEngine.Triggers;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerEvent;
import com.MMOCardGame.GameServer.common.CallbackTask2;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.*;

@EqualsAndHashCode
@ToString
public abstract class TriggersMechanism implements Serializable {
    protected transient Map<Class<?>, Trigger> triggersMap = new HashMap<>();
    protected transient GameState gameState;

    public TriggersMechanism(GameState gameState) {
        this.gameState = gameState;
    }

    public Set<Class<?>> getSetOfSupportedTriggersClasses() {
        return triggersMap.keySet();
    }

    public void registerSimpleScript(Object script) {
        registerOneScript(script);
    }

    public void registerCardScript(CardScript cardScript) {
        List<CardScript> cardScriptsList = getScriptAndSubscriptsList(cardScript);
        registerCardScriptsList(cardScriptsList);
    }

    private List<CardScript> getScriptAndSubscriptsList(CardScript cardScript) {
        List<CardScript> cardScriptList = new ArrayList<>();
        cardScriptList.add(cardScript);
        for (int i = 0; i < cardScriptList.size(); i++)
            cardScriptList.addAll(cardScriptList.get(i).getSubscripts());
        return cardScriptList;
    }

    private void registerCardScriptsList(List<CardScript> cardScriptsList) {
        for (CardScript cardScript : cardScriptsList) {
            registerOneScript(cardScript);
        }
    }

    private void registerOneScript(Object cardScript) {
        for (Class<?> interfaceClass : findAllImplementingInterfaces(cardScript.getClass())) {
            if (triggersMap.containsKey(interfaceClass))
                triggersMap.get(interfaceClass).add(cardScript);
        }
    }

    private Set<Class<?>> findAllImplementingInterfaces(Class<?> classToSearch) {
        Set<Class<?>> results = new HashSet<>();
        while (!classToSearch.equals(Object.class)) {
            results.addAll(Arrays.asList(classToSearch.getInterfaces()));
            classToSearch = classToSearch.getSuperclass();
        }
        return results;
    }

    public void unregisterCardScript(CardScript cardScript) {
        List<CardScript> cardScriptsList = getScriptAndSubscriptsList(cardScript);
        unregisterCardScriptsList(cardScriptsList);
    }

    private void unregisterCardScriptsList(List<CardScript> cardScriptsList) {
        for (CardScript cardScript : cardScriptsList)
            unregisterOneCardScript(cardScript);
    }

    private void unregisterOneCardScript(CardScript cardScript) {
        for (Class<?> interfaceClass : findAllImplementingInterfaces(cardScript.getClass())) {
            if (triggersMap.containsKey(interfaceClass))
                triggersMap.get(interfaceClass).remove(cardScript);
        }
    }
}

@EqualsAndHashCode(exclude = "runEventTask")
class Trigger<U, E extends TriggerEvent> implements Serializable {
    List<U> scripts = new ArrayList<>();
    private CallbackTask2<U, E> runEventTask;

    public Trigger(CallbackTask2<U, E> task) {
        runEventTask = task;
    }

    void add(U script) {
        if (!scripts.contains(script))
            scripts.add(script);
    }

    void remove(U script) {
        scripts.remove(script);
    }

    void run(E event) {
        for (int i = scripts.size() - 1; i >= 0; i--) {
            runEventTask.run(scripts.get(i), event);
            if (event.isStopFurtherTriggersExecution())
                break;
        }
    }
}