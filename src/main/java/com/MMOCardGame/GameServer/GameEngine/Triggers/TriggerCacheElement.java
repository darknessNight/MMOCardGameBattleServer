package com.MMOCardGame.GameServer.GameEngine.Triggers;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerEvent;
import com.MMOCardGame.GameServer.common.CallbackTask2;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@EqualsAndHashCode
class TriggerCacheElement<S, E extends TriggerEvent> {
    Logger logger = LoggerFactory.getLogger(TriggerCacheElement.class);
    Class<S> scriptType;
    Class<E> eventType;
    CallbackTask2<S, E> task = null;

    TriggerCacheElement(Class<?> aClass) {
        scriptType = (Class<S>) aClass;
        TriggerScript triggerScript = aClass.getAnnotation(TriggerScript.class);
        loadScriptMethod(aClass, triggerScript);
    }

    private void loadScriptMethod(Class<?> aClass, TriggerScript triggerScript) {
        try {
            if (tryLoadMethod(aClass, triggerScript)) return;
        } catch (Exception e) {
            logger.error("Cannot find script method", e);
        }
    }

    private boolean tryLoadMethod(Class<?> aClass, TriggerScript triggerScript) {
        Method scriptMethod = getScriptMethod(aClass, triggerScript);
        if (scriptMethod == null) return true;
        eventType = (Class<E>) scriptMethod.getParameterTypes()[0];
        prepareTask(scriptMethod);
        return false;
    }

    private void prepareTask(Method scriptMethod) {
        task = (object, event) -> {
            try {
                scriptMethod.invoke(object, event);
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error("Cannot invoke method", e);
            }
        };
    }

    private Method getScriptMethod(Class<?> aClass, TriggerScript triggerScript) {
        if (triggerScript.method().equals(""))
            return Arrays.stream(aClass.getMethods())
                    .filter(method -> isCorrectScriptMethod(method))
                    .findAny().orElse(null);
        return Arrays.stream(aClass.getMethods())
                .filter(method -> isCorrectScriptMethod(method)
                        && method.getName().equals(triggerScript.method()))
                .findAny().orElse(null);
    }

    private boolean isCorrectScriptMethod(Method method) {
        return method.getParameterTypes().length == 1 && TriggerEvent.class.isAssignableFrom(method.getParameterTypes()[0]);
    }
}
