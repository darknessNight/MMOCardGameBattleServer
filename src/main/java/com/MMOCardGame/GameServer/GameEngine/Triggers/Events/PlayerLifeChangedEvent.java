package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerLifeChangedEvent extends TriggerEvent {
    int player;
    int oldLife;
    int newLife;
}
