package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PhaseEndedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface PhaseEndedScript {
    void phaseEnded(PhaseEndedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.PhaseEnded;
    }
}

