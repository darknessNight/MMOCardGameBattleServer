package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.MainPhaseEndedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface MainPhaseEndedScript {
    void phaseEnded(MainPhaseEndedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.MainPhaseEnded;
    }
}
