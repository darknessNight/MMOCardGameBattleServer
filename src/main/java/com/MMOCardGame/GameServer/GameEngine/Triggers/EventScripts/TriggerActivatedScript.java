package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerActivatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface TriggerActivatedScript {
    void triggerActivated(TriggerActivatedEvent eventType);

    default TriggerType getTriggerType() {
        return TriggerType.TriggerActivated;
    }
}
