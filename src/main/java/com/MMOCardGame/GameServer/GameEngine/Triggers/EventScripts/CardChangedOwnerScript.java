package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardChangedOwnerEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardChangedOwnerScript {
    void cardChangedOwner(CardChangedOwnerEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardChangedOwner;
    }
}
