package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardAttackedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardAttackedScript {
    void cardAttacked(CardAttackedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardAttacked;
    }
}
