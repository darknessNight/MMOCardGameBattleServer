package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardRevealedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardRevealedScript {
    void cardRevealed(CardRevealedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardRevealed;
    }
}
