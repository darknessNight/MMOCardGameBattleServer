package com.MMOCardGame.GameServer.GameEngine.Triggers;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface TriggerScript {
    String method() default "";
}
