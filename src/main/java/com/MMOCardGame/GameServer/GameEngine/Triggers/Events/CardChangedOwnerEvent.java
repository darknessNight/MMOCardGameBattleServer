package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;


import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CardChangedOwnerEvent extends TriggerEvent {
    CardPosition oldPosition;
    CardPosition newPosition;
    Card card;

    public CardChangedOwnerEvent(GameState gameState, Card card, CardPosition oldPosition, CardPosition newPosition) {
        super();
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
        this.card = card;
    }
}
