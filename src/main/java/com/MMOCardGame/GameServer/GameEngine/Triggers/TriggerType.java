package com.MMOCardGame.GameServer.GameEngine.Triggers;

public enum TriggerType {
    UndefinedTriggerType,
    CardAttacked,
    CardChangedOwner,
    CardMoved,
    CardRevealed,
    DrawCard,
    PhaseEnded,
    MainPhaseEnded,
    PlayerDefeated,
    PlayerLifeChanged,
    TriggerActivated,
    CardCreated,
    CardStatisticsChanged,
    PlayerLinkPointsChanged,
    ;
}
