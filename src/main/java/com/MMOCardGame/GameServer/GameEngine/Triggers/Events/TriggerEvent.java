package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.ReactionStackController;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.Data;

@Data
public class TriggerEvent {
    GameState gameState;
    ReactionStackController reactionStackController = null;
    boolean stopFurtherTriggersExecution = false;
}
