package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerDefeatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface PlayerDefeatedScript {
    void playerDefeated(PlayerDefeatedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.PlayerDefeated;
    }
}

