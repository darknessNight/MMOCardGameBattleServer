package com.MMOCardGame.GameServer.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.exceptions.ArgumentParsingException;
import org.apache.commons.cli.CommandLine;

import java.lang.reflect.Field;
import java.util.Properties;

public class AppConfigurationCommandLineParsing extends AppConfiguration {

    public AppConfigurationCommandLineParsing() {
        super();
    }

    public AppConfigurationCommandLineParsing(Settings settings) {
        super(settings);
    }

    @Override
    public void initFromConsoleParams(CommandLine commandLine) throws ArgumentParsingException {
        Properties properties = commandLine.getOptionProperties("D");
        properties = readSettingsValuesFromArgsAndCleanArgs(properties);
        checkIsAllArgsRead(properties);
    }

    private void checkIsAllArgsRead(Properties properties) throws ArgumentParsingException {
        if (properties.size() > 0)
            throw new ArgumentParsingException("Non existing property for \"D\" argument: " + getListOfIncorrectProperties(properties));
    }

    private Properties readSettingsValuesFromArgsAndCleanArgs(Properties properties) throws ArgumentParsingException {
        Field[] fields = Settings.class.getFields();
        for (Field field : fields) {
            if (properties.containsKey(field.getName())) {
                setSettingValueAndRemoveArg(properties, field);
            }
        }
        return properties;
    }

    private void setSettingValueAndRemoveArg(Properties properties, Field field) throws ArgumentParsingException {
        try {
            trySetSettingsValueAndRemoveArgsFromList(properties, field);
        } catch (IllegalAccessException e) {
            throw new ArgumentParsingException(e);
        }
    }

    private void trySetSettingsValueAndRemoveArgsFromList(Properties properties, Field field) throws IllegalAccessException, ArgumentParsingException {
        field.set(settings, parsePropertyValueFromString(properties.getProperty(field.getName()), field.getType()));
        properties.remove(field.getName());
    }

    private String getListOfIncorrectProperties(Properties properties) {
        String result = new String();
        for (String property : properties.stringPropertyNames()) {
            result += property + " ";
        }
        return result;
    }

    private Object parsePropertyValueFromString(String property, Class<?> type) throws ArgumentParsingException {
        try {
            if (type.equals(String.class))
                return property;
            else if (type.equals(int.class))
                return Integer.parseInt(property);
            else if (type.equals(boolean.class))
                return Boolean.parseBoolean(property);
            else if (type.equals(double.class))
                return Double.parseDouble(property);
            else
                throw new ArgumentParsingException();
        } catch (Throwable e) {
            throw new ArgumentParsingException("Parsing exception. Cannot parse <" + property + "> to " + type.getName());
        }
    }


}
