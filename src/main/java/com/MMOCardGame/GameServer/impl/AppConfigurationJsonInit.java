package com.MMOCardGame.GameServer.impl;

import com.MMOCardGame.GameServer.ProgramConstants;
import com.google.gson.Gson;

import java.io.*;

public class AppConfigurationJsonInit extends AppConfigurationCommandLineParsing {
    public AppConfigurationJsonInit(InputStream stream) {
        super(readSettingsFromStream(stream));
    }

    public AppConfigurationJsonInit() throws IOException {
        try (InputStream stream = new FileInputStream(ProgramConstants.getAppSettingsFile())) {
            this.settings = readSettingsFromStream(stream);
            initEmptyData();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public static void saveSettingsToStream(OutputStream stream, Settings settings) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(settings);
        stream.write(json.getBytes());
    }

    public static Settings readSettingsFromStream(InputStream stream) {
        Gson gson = new Gson();
        Reader reader = new InputStreamReader(stream);
        return gson.fromJson(reader, Settings.class);
    }
}
