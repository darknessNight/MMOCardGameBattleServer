package com.MMOCardGame.GameServer.Authentication.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;

import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryUserTokenContainer implements UserTokenContainer {
    private Map<String, TokenInfo> tokenMap = new ConcurrentHashMap<>();
    private AppConfiguration configuration;
    private TimeProducer timeProducer;

    public InMemoryUserTokenContainer(AppConfiguration configuration) {
        this.configuration = configuration;
        timeProducer = new TimeProducer();
    }

    public InMemoryUserTokenContainer(AppConfiguration configuration, TimeProducer timeProducer) {
        this.configuration = configuration;
        this.timeProducer = timeProducer;
    }

    @Override
    public void registerNewUserToken(String name, String token) {
        tokenMap.put(name, new TokenInfo(token, timeProducer.getNow()));
    }

    @Override
    public void removeTokenForUser(String name) {
        tokenMap.remove(name);
    }

    @Override
    public void clearUsedTokens() {
        tokenMap.entrySet().removeIf(stringTokenInfoEntry -> stringTokenInfoEntry.getValue().used);
    }

    @Override
    public void clearOldTokens() {
        Date expirationDate = Date.from(timeProducer.getNow().toInstant().minusSeconds(configuration.getVolatileResourcesLifeDuration()));
        tokenMap.entrySet().removeIf(stringTokenInfoEntry -> stringTokenInfoEntry.getValue().registered.before(expirationDate));
    }

    @Override
    public void checkUser(String name, String token) throws AuthenticationException {
        if (!isCorrectUser(name, token))
            throw new AuthenticationException();
    }

    @Override
    public boolean isCorrectUser(String name, String token) {
        TokenInfo tokenInfo = tokenMap.get(name);
        if (tokenInfo == null)
            return false;
        if (!tokenInfo.token.equals(token))
            return false;
        tokenInfo.used = true;
        return true;
    }

    class TokenInfo {
        Date registered;
        boolean used = false;
        String token;

        public TokenInfo(String token, Date registered) {
            this.token = token;
            this.registered = registered;
        }
    }

    class TimeProducer {
        public Date getNow() {
            return Date.from(Instant.now());
        }
    }
}
