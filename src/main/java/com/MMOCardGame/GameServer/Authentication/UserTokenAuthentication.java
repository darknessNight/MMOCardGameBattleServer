package com.MMOCardGame.GameServer.Authentication;

import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;

public interface UserTokenAuthentication {
    void checkUser(String name, String token) throws AuthenticationException;

    boolean isCorrectUser(String name, String token);
}
