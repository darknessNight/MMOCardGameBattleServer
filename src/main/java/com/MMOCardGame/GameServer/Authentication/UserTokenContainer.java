package com.MMOCardGame.GameServer.Authentication;

public interface UserTokenContainer extends UserTokenAuthentication {
    void registerNewUserToken(String name, String token);

    void removeTokenForUser(String name);

    void clearUsedTokens();

    void clearOldTokens();
}
