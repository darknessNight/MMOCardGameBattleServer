package com.MMOCardGame.GameServer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ProgramConstants {
    public static String getAppSettingsFile() {
        return getConfDir() + "/settings.json";
    }

    public static String getConfDir() {
        String confDir = System.getenv("GAME_SERVER_CONF_DIR");
        if (confDir != null) {
            return confDir;
        } else {
            return "./conf";
        }
    }

    public static void initAppTempDir() throws IOException {
        Path tempPath = Paths.get(getAppTempDir());
        if (!Files.exists(tempPath)) {
            Files.createDirectory(tempPath);
        }
    }

    public static String getAppTempDir() {
        return getSystemTempDir() + "/MMOCardGame_GameServer";
    }

    public static String getSystemTempDir() {
        String tempDir = System.getenv("GAME_SERVER_TEMP_DIR");
        if (tempDir != null) {
            return tempDir;
        } else {
            return System.getProperty("java.io.tmpdir");
        }
    }

    public static String getDumpDir() {
        String confDir = System.getenv("GAME_SERVER_DUMP_DIR");
        if (confDir != null) {
            return confDir;
        } else {
            return "./dump";
        }
    }
}
