package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.factories.DaggerCardFactoryFactory;
import com.MMOCardGame.GameServer.GameEngine.factories.DaggerGameSessionManagerFactory;
import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionManagerFactory;
import com.MMOCardGame.GameServer.Servers.ServiceServer;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.UserServer;
import com.MMOCardGame.GameServer.Servers.factories.DaggerServersFactory;
import com.MMOCardGame.GameServer.Servers.factories.ServersFactory;
import com.MMOCardGame.GameServer.factories.DaggerConfigurationModule;
import com.MMOCardGame.GameServer.factories.DaggerUserConnectionsManagerFactory;
import com.MMOCardGame.GameServer.factories.UserConnectionsManagerFactory;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GameServer {
    private final Logger logger = LoggerFactory.getLogger(GameServer.class);
    private ArgumentParser argumentParser = new ArgumentParser();
    private UserServer userServer;
    private ServiceServer serviceServer;
    private boolean serverWorking = false;
    private UserConnectionsManager connectionManager;
    private GameSessionManager sessionsManager;
    private SessionEndNotifier sessionEndNotifier = new SessionEndNotifier(DaggerConfigurationModule.create().getAppConfiguration());
    @Setter
    private UserConnectionsManagerFactory userConnectionsManagerFactory = DaggerUserConnectionsManagerFactory.create();
    @Setter
    private GameSessionManagerFactory gameSessionManagerFactory = DaggerGameSessionManagerFactory.create();

    public GameServer() {
    }

    public boolean isStarted() {
        return serverWorking;
    }

    public void stop() {
        stopServers();
    }

    public void startBlocking(String[] args) {
        init(args);
        if (serverWorking) {
            waitUntilStop();
        }
    }

    private void waitUntilStop() {
        userServer.awaitTermination();
        serviceServer.awaitTermination();
    }

    private void init(String[] args) {
        argumentParser.parse(args);
        if (argumentParser.isServerShouldStart()) {
            preloadAllConfigData();
            initUserSessionsManagers();
            initServers();
        }
    }

    private void preloadAllConfigData() {
        try {
            tryPreloadAllConfigData();
        } catch (Throwable e) {
            logger.error("Error during preloading config data", e);
            throw e;
        }
    }

    private void tryPreloadAllConfigData() {
        DaggerCardFactoryFactory.create().getCardFactoryBuilder();
    }

    private void initUserSessionsManagers() {
        connectionManager = userConnectionsManagerFactory.getUserConnectionManager();
        sessionsManager = gameSessionManagerFactory
                .getGameSessionsManagerBuilder()
                .setUserConnectionsManager(connectionManager)
                .setInfoObserver(object -> sessionEndNotifier.notifyDataServer(object))
                .build();
    }

    private void initServers() {
        createServers();
        startServers();
        setShutdownHook();
    }

    private void setShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            GameServer.this.stopServers();
            logger.info("Shutdown on hook");
        }));
    }

    private void startServers() {
        try {
            userServer.start();
            serviceServer.start();
            serverWorking = true;
        } catch (IOException e) {
            logger.error("Error during starting", e);
            serverWorking = false;
        }
    }

    private void createServers() {
        ServersFactory serversFactory = DaggerServersFactory.create();
        userServer = serversFactory.getUserServerBuilder().setConnectionManager(connectionManager).build();
        serviceServer = serversFactory.getServiceServerBuilder().setGameSessionManager(sessionsManager).build();
    }

    private void stopServers() {
        sessionsManager.endSessions();
        connectionManager.closeAllConnections();
        userServer.stop();
        serviceServer.stop();
    }
}
