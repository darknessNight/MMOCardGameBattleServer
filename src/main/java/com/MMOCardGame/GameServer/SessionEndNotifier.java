package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.google.gson.Gson;

public class SessionEndNotifier {
    private final CallbackRestRequestSender requestSender;
    AppConfiguration configuration;

    public SessionEndNotifier(AppConfiguration configuration) {
        requestSender = new CallbackRestRequestSender(configuration);
    }

    public SessionEndNotifier(CallbackRestRequestSender callbackRestRequestSender) {
        this.requestSender = callbackRestRequestSender;
    }

    public void notifyDataServer(UserSessionEndInfo object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        requestSender.sendCallbackInfo(json);
    }
}
