package com.MMOCardGame.GameServer.Servers;

public final class ErrorCodes {
    public static final int Created = 201;
    public static final int AccessForbidden = 403;
    public static final int InternalError = 500;
    public static final int BadRequest = 400;
    public static final int NotFound = 404;
    public static final int NotAcceptable = 406;
    public static final int InconsistentClientState = 601;
}
