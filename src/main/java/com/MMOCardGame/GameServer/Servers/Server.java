package com.MMOCardGame.GameServer.Servers;

import java.io.IOException;

public interface Server {
    void start() throws IOException;

    void stop();

    void awaitTermination();
}
