package com.MMOCardGame.GameServer.Servers;

import com.MMOCardGame.GameServer.common.Observer;

public interface UserConnectionObserver extends Observer<UserConnection> {
    @Override
    void update(UserConnection connection);
}
