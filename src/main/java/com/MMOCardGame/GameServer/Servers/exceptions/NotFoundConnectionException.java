package com.MMOCardGame.GameServer.Servers.exceptions;

public class NotFoundConnectionException extends RuntimeException {
    public NotFoundConnectionException() {
        super();
    }

    public NotFoundConnectionException(String message) {
        super(message);
    }

    public NotFoundConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundConnectionException(Throwable cause) {
        super(cause);
    }

    protected NotFoundConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
