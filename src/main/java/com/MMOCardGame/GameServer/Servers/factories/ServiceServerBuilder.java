package com.MMOCardGame.GameServer.Servers.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.Servers.ServiceServer;

public interface ServiceServerBuilder {
    ServiceServerBuilder setAppConfiguration(AppConfiguration appConfiguration);

    ServiceServerBuilder setTokenContainer(UserTokenContainer tokenContainer);

    ServiceServerBuilder setGameSessionManager(GameSessionManager gameSessionManager);

    ServiceServer build();
}
