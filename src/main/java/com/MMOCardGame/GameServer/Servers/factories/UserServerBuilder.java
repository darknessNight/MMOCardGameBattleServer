package com.MMOCardGame.GameServer.Servers.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.UserServer;

public interface UserServerBuilder {
    UserServerBuilder setAppConfiguration(AppConfiguration appConfiguration);

    UserServerBuilder setConnectionManager(UserConnectionsManager connectionManager);

    UserServer build() throws IllegalArgumentException, NullPointerException;
}
