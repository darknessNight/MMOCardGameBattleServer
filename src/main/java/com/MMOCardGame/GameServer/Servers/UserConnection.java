package com.MMOCardGame.GameServer.Servers;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionService;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.MMOCardGame.GameServer.common.CallbackTask2;
import com.MMOCardGame.GameServer.common.Pair;
import com.MMOCardGame.GameServer.common.TaskWithReturn;
import com.MMOCardGame.GameServer.factories.ExecutorFactory;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public abstract class UserConnection {
    @Getter
    private final UUID token;
    private ExecutorService executorService = ExecutorFactory.getTaskExecutorService();
    @Setter
    @Getter
    private boolean prepared;
    @Getter
    private String userName;
    @Setter
    @Getter
    private int userId = -1;
    private AtomicReference<GameSessionService> gameSessionService = new AtomicReference<>();

    public UserConnection(String userName) {
        token = UUID.randomUUID();
        this.userName = userName;
    }

    public void startReconnect() {
        prepared = false;
    }

    protected void addNewTaskToExecute(Runnable task) {
        executorService.submit(task);
    }

    public void registerSessionService(GameSessionService gameSessionService) {
        this.gameSessionService.set(gameSessionService);
    }

    public void removeSessionService() {
        this.gameSessionService.set(null);
    }

    public abstract void close();

    public abstract void kickPlayer(String reason);

    public abstract void sendMessage(Object message);

    public void surrender() {
        addNewTaskToExecute(() -> gameSessionService.get().surrender(getUserId()));
    }

    public GameStateMessage getWholeGameState() {
        GameStateMessage.Builder builder = GameStateMessage.newBuilder();
        GameState gameState = gameSessionService.get().getAllGameState();
        builder.setActivePlayer(gameState.getCurrentPlayer());
        builder.setCurrentPhaseValue(gameState.getCurrentPhase().ordinal());
        for (String player : gameState.getListOfActiveUsers()) {
            int id = gameState.getPlayerId(player);
            builder.addPlayers(PlayerState.newBuilder()
                    .setId(id)
                    .setHeroId(gameState.getPlayers().get(id).getHeroId())
                    .setDefence(gameState.getPlayers().get(id).getLife())
                    .setName(player)
                    .addAllKnownCards(getPlayerCards(gameState, id))
                    .setCardsInLibraryCount((int) getCountCardsInLibrary(gameState, id))
                    .setCardsInHandCount((int) getCountCardsInHand(gameState, id))
                    .build());
        }
        return builder.build();
    }

    private long getCountCardsInLibrary(GameState gameState, int id) {
        return getCardsCountInPosition(gameState, id, CardPosition.Position.Library);
    }

    private long getCardsCountInPosition(GameState gameState, int id, CardPosition.Position library) {
        return gameState.getPlayerCardsPositions(id).stream()
                .filter(cardPositionPair -> cardPositionPair.getSecond().equals(library))
                .count();
    }

    private long getCountCardsInHand(GameState gameState, int id) {
        return getCardsCountInPosition(gameState, id, CardPosition.Position.Hand);
    }

    private Iterable<? extends PlayerCard> getPlayerCards(GameState gameState, int playerId) {
        return gameState.getPlayerCardsPositions(playerId).stream()
                .filter(cardPositionPair -> cardPositionPair.getFirst().isRevealed() || playerId == userId)
                .map(cardPositionPair -> {
                    Card card = cardPositionPair.getFirst();
                    return getFullCardInfo(gameState, cardPositionPair, card);
                }).collect(Collectors.toList());
    }

    private PlayerCard getFullCardInfo(GameState gameState, Pair<Card, CardPosition.Position> cardPositionPair, Card card) {
        return PlayerCard.newBuilder()
                .setPositionValue(cardPositionPair.getSecond().ordinal())
                .setPositionIndex(gameState.getCardPosition(card.getInstanceId()).getIndex())
                .setInstanceId(card.getInstanceId())
                .setAttack(card.getAttack())
                .setDefence(card.getDefence())
                .setCardId(card.getId())
                .setOverloadedPoints(card.getOverloadPoints())
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .build();
    }

    public void cancelGame(Runnable successfulTask, CallbackTask<Throwable> errorTask) {
        addNewTaskToExecute(() -> {
            try {
                gameSessionService.get().surrender(getUserId());
                successfulTask.run();
            } catch (Throwable e) {
                errorTask.run(e);
            }
        });
    }

    public void useCard(CardUsed message, CallbackTask<CardUsed> callbackTask, CallbackTask<Throwable> errorTask) {
        var messageSafe=CardUsed.newBuilder(message).setPlayer(userId).build();
        startReturningTask(() -> gameSessionService.get().useCard(messageSafe, userId), callbackTask, errorTask);
    }

    private <T> void startReturningTask(TaskWithReturn<T> task, CallbackTask<T> callbackTask, CallbackTask<Throwable> errorTask) {
        addNewTaskToExecute(() -> {
            try {
                T result = task.run();
                callbackTask.run(result);
            } catch (Throwable e) {
                runErrorTaskAndDumpGameSession(errorTask, e);
            }
        });
    }

    private void runErrorTaskAndDumpGameSession(CallbackTask<Throwable> errorTask, Throwable e) {
        errorTask.run(e);
        //dumpGameSession(e);
    }

    private void dumpGameSession(Throwable throwable) {
        gameSessionService.get().dumpToFile(throwable);
    }

    public void endPhase(PhaseChanged message, Runnable successfulTask, CallbackTask<Throwable> errorTask) {
        startFailingTask(() -> gameSessionService.get().endPhase(userId, message), successfulTask, errorTask);
    }

    private void startFailingTask(Runnable task, Runnable callbackTask, CallbackTask<Throwable> errorTask) {
        addNewTaskToExecute(() -> {
            try {
                task.run();
                callbackTask.run();
            } catch (Throwable e) {
                runErrorTaskAndDumpGameSession(errorTask, e);
            }
        });
    }

    public void useHeroAbility(HeroAbilityUsed message, CallbackTask2<CardCreated, Integer> callbackTask, CallbackTask<Throwable> errorTask) {
        var messageSafe=HeroAbilityUsed.newBuilder(message).setPlayer(userId).build();
        addNewTaskToExecute(() -> {
            try {
                CardCreated.Builder cardCreatedOrBuilder = CardCreated.newBuilder();
                Integer result = gameSessionService.get().useHeroAbility(messageSafe, cardCreatedOrBuilder);
                callbackTask.run(cardCreatedOrBuilder.build(), result);
            } catch (Throwable e) {
                runErrorTaskAndDumpGameSession(errorTask, e);
            }
        });
    }

    public void selectAttackersDefenders(AttackersDefendersSelected message, Runnable task, CallbackTask<Throwable> errorTask) {
        startFailingTask(() -> gameSessionService.get().selectAttackersDefenders(userId, message), task, errorTask);
    }

    public void drawCards(HandChanged message, CallbackTask<HandChanged> callbackTask, CallbackTask<Throwable> errorTask) {
        var messageSafe=HandChanged.newBuilder(message).setPlayer(userId).build();
        startReturningTask(() -> gameSessionService.get().drawCards(messageSafe), callbackTask, errorTask);
    }

    public void discardCards(HandChanged message, CallbackTask<HandChanged> callbackTask, CallbackTask<Throwable> errorTask) {
        var messageSafe=HandChanged.newBuilder(message).setPlayer(userId).build();
        startReturningTask(() -> gameSessionService.get().discardCards(messageSafe), callbackTask, errorTask);
    }

    public void muligan(Runnable callbackTask, CallbackTask<Throwable> errorTask) {
        startFailingTask(() -> gameSessionService.get().muligan(userId), callbackTask, errorTask);
    }

    public void replaceCards(HandChanged message, Runnable callbackTask, CallbackTask<Throwable> errorTask) {
        var messageFixed=HandChanged.newBuilder(message).setPlayer(userId).build();
        startFailingTask(() -> gameSessionService.get().replaceCards(HandChanged.newBuilder(messageFixed).setPlayer(userId).build()), callbackTask, errorTask);
    }

    public void getStartingHand(Runnable successfulTask, CallbackTask<Throwable> errorTask) {
        startFailingTask(() -> gameSessionService.get().keepStartingHand(userId), successfulTask, errorTask);
    }

    public void doTriggerReaction(TriggerActivated message, Runnable callbackTask, CallbackTask<Throwable> errorTask) {
        var messageSafe=TriggerActivated.newBuilder(message).build();
        startFailingTask(() -> gameSessionService.get().doTriggerReaction(messageSafe), callbackTask, errorTask);
    }

    public void doTargetSelectingTransaction(TargetSelectingTransaction message, CallbackTask<TargetSelectingTransaction> callbackTask, CallbackTask<Throwable> errorTask) {
        startReturningTask(() -> gameSessionService.get().doTargetSelectingTransaction(message), callbackTask, errorTask);
    }
}
