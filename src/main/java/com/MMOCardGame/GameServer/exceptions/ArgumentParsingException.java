package com.MMOCardGame.GameServer.exceptions;

public class ArgumentParsingException extends Exception {
    public ArgumentParsingException() {
        super();
    }

    public ArgumentParsingException(Throwable e) {
        super(e);
    }

    public ArgumentParsingException(String message) {
        super(message);
    }
}
