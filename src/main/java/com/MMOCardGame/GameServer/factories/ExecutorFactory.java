package com.MMOCardGame.GameServer.factories;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class ExecutorFactory {
    private static ExecutorService taskExecutorService = new ForkJoinPool();
    private static Executor taskExecutor = new Executor() {
        @Override
        public void execute(Runnable command) {
            taskExecutorService.submit(command);
        }
    };

    public static Executor getTaskExecutor() {
        return taskExecutor;
    }

    public static ExecutorService getTaskExecutorService() {
        return taskExecutorService;
    }
}
