package com.MMOCardGame.GameServer;

public class GameConstants {
    static String callbackUrlValue=null;

    public static int getDeckMinCount() {
        return 45;
    }

    public static int getStartingLife() {
        return 20;
    }

    public static int getStartingHandSize() {
        return 7;
    }

    public static int getMaxLinksInFirstHand() {
        return 5;
    }

    public static int getMinLinksInFirstHand() {
        return 2;
    }

    public static int getMaxMuligansCount() {
        return getStartingHandSize() + 1;
    }

    public static int getDefaultAvailableCountOfLinksToPlayed() {
        return 1;
    }

    public static String getCallbackUrl() {
        if(callbackUrlValue==null){
            callbackUrlValue=System.getenv("GAMESERVER_CALLBACK_URL");
        }
        return callbackUrlValue;
    }
}
