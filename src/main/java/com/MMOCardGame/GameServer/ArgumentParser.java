package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.exceptions.ArgumentParsingException;
import com.MMOCardGame.GameServer.factories.DaggerConfigurationModule;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArgumentParser {
    private final Logger logger = LoggerFactory.getLogger(ArgumentParser.class);
    private boolean serverShouldStart = true;
    private AppConfiguration configuration;

    public ArgumentParser() {
        configuration = DaggerConfigurationModule.create().getAppConfiguration();
    }

    public ArgumentParser(AppConfiguration configuration) {
        this.configuration = configuration;
    }

    public void parse(String[] args) {
        Options options = getOptions();
        parseArgsAndUpdateSettings(args, options);
    }

    private void parseArgsAndUpdateSettings(String[] args, Options options) {
        try {
            tryParseArgsAndUpdateSettings(args, options);
        } catch (ParseException | ArgumentParsingException e) {
            logger.error("Problem during argument parsing", e);
            System.err.println("Problem during argument parsing: " + e.getMessage());
            serverShouldStart = false;
        }
    }

    private void tryParseArgsAndUpdateSettings(String[] args, Options options) throws ParseException, ArgumentParsingException {
        CommandLine commandLine = parseArgs(args, options);
        overrideSettings(commandLine);
        displayHelpIfNeeded(options, commandLine);
        readPreventServerStart(commandLine);
    }

    private CommandLine parseArgs(String[] args, Options options) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    private void readPreventServerStart(CommandLine commandLine) {
        if (commandLine.hasOption("noStartServer")) {
            serverShouldStart = false;
        }
    }

    private void displayHelpIfNeeded(Options options, CommandLine commandLine) {
        if (commandLine.hasOption("?")) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("gameServer", options);
            serverShouldStart = false;
        }
    }

    private void overrideSettings(CommandLine commandLine) throws ArgumentParsingException {
        configuration.initFromConsoleParams(commandLine);
    }

    private Options getOptions() {
        Options options = new Options();

        options.addOption(Option.builder("D")
                .argName("param=value")
                .longOpt("overrideSettingParam")
                .valueSeparator('=')
                .hasArg()
                .numberOfArgs(2)
                .desc("override server settings (names are same as names in config file)")
                .build());

        options.addOption(Option.builder("?")
                .longOpt("help")
                .desc("Display this message")
                .build());

        options.addOption(Option.builder()
                .longOpt("noStartServer")
                .desc("Do only initial task (like: prepare cache, download resources, ect) without starting server")
                .build());
        return options;
    }

    public boolean isServerShouldStart() {
        return serverShouldStart;
    }

    public AppConfiguration getConfiguration() {
        return configuration;
    }
}
