package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.exceptions.ArgumentParsingException;
import com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing;
import org.apache.commons.cli.CommandLine;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AppConfigurationCommandLineParsingTest {
    AppConfiguration.Settings getSampleSettings() {
        AppConfiguration.Settings settings = new AppConfiguration.Settings();
        settings.userServerPort = 23;
        settings.serviceServerPort = 34;
        return settings;
    }

    @Test
    void initFromConsoleParams_emptyProperty_noChanges() throws ArgumentParsingException {
        com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing appConfigurationCommandLineParsing = new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing(getSampleSettings());
        CommandLine commandLine = mock(CommandLine.class);
        when(commandLine.getOptionProperties("D")).thenReturn(new Properties());

        appConfigurationCommandLineParsing.initFromConsoleParams(commandLine);

        assertEquals(getSampleSettings(), appConfigurationCommandLineParsing.settings);
    }

    @Test
    void initFromConsoleParams_OneElementProperty_changedSetting() throws ArgumentParsingException {
        com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing appConfigurationCommandLineParsing = new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing(getSampleSettings());
        AppConfiguration.Settings expectedSettings = getSampleSettings();
        expectedSettings.serviceServerPort = 0;

        Properties properties = new Properties();
        properties.put("serviceServerPort", "0");

        CommandLine commandLine = mock(CommandLine.class);
        when(commandLine.getOptionProperties("D")).thenReturn(properties);

        appConfigurationCommandLineParsing.initFromConsoleParams(commandLine);

        assertEquals(expectedSettings, appConfigurationCommandLineParsing.settings);
    }

    @Test
    void initFromConsoleParams_TwoElementsProperty_changedSetting() throws ArgumentParsingException {
        com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing appConfigurationCommandLineParsing = new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing(getSampleSettings());
        AppConfiguration.Settings expectedSettings = getSampleSettings();
        expectedSettings.serviceServerPort = 0;
        expectedSettings.userServerPort = 10;

        Properties properties = new Properties();
        properties.put("serviceServerPort", "0");
        properties.put("userServerPort", "10");

        CommandLine commandLine = mock(CommandLine.class);
        when(commandLine.getOptionProperties("D")).thenReturn(properties);

        appConfigurationCommandLineParsing.initFromConsoleParams(commandLine);

        assertEquals(expectedSettings, appConfigurationCommandLineParsing.settings);
    }

    @Test
    void initFromConsoleParams_NonExistingProperty_throwArgumentParsingException() {
        com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing appConfigurationCommandLineParsing = new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing(getSampleSettings());

        Properties properties = new Properties();
        properties.put("NonExistingProperty-------", "0");

        CommandLine commandLine = mock(CommandLine.class);
        when(commandLine.getOptionProperties("D")).thenReturn(properties);

        assertThrows(ArgumentParsingException.class, () -> appConfigurationCommandLineParsing.initFromConsoleParams(commandLine));
    }

    @Test
    void initFromConsoleParams_NonParsableProperty_throwArgumentParsingException() {
        com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing appConfigurationCommandLineParsing = new AppConfigurationCommandLineParsing(getSampleSettings());

        Properties properties = new Properties();
        properties.put("serviceServerPort", "abcdef");

        CommandLine commandLine = mock(CommandLine.class);
        when(commandLine.getOptionProperties("D")).thenReturn(properties);

        assertThrows(ArgumentParsingException.class, () -> appConfigurationCommandLineParsing.initFromConsoleParams(commandLine));
    }
}