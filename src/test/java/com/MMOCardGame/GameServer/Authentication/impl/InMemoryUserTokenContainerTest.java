package com.MMOCardGame.GameServer.Authentication.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class InMemoryUserTokenContainerTest {
    @Test
    void checkUser_noRegisteredUser_throwsAuthenticationException() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);

        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("Test", "Test"));
    }

    @Test
    void checkUser_registeredSomeUserAndTestWithIncorrectUser_throwsAuthenticationException() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("NewUser", "Token");

        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("Test", "Test"));
    }

    @Test
    void checkUser_registeredUserTestUserWithWrongToken_throwsAuthenticationException() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");

        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("User", "Test"));
    }

    @Test
    void checkUser_registeredUserTestCorrectUser_noThrows() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");

        tokenContainer.checkUser("User", "Token");
    }

    @Test
    void checkUser_registeredTwoUsersTestCorrectUser_noThrows() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");
        tokenContainer.registerNewUserToken("User2", "Token2");

        tokenContainer.checkUser("User", "Token");
    }

    @Test
    void removeTokenForUser_noUsers_noThrows() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);

        tokenContainer.removeTokenForUser("Test");
    }

    @Test
    void removeTokenForUser_otherUser_noThrows() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");

        tokenContainer.removeTokenForUser("Test");
    }

    @Test
    void removeTokenForUser_correctUser_noLongerPassThisUser() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");

        tokenContainer.removeTokenForUser("User");

        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("User", "Token"));
    }

    @Test
    void removeTokenForUser_correctTwoUser_noLongerPassThisUserAndPassOtherUser() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User", "Token");
        tokenContainer.registerNewUserToken("User2", "Token2");

        tokenContainer.removeTokenForUser("User");
        tokenContainer.checkUser("User2", "Token2");

        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("User", "Token"));
    }

    @Test
    void clearUsedTokens_noTokens_noThrows() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.clearUsedTokens();
    }

    @Test
    void clearUsedTokens_noUsedTokens_noThrowsCanUseTokens() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User1", "Token1");
        tokenContainer.registerNewUserToken("User2", "Token2");

        tokenContainer.clearUsedTokens();

        tokenContainer.checkUser("User1", "Token1");
        tokenContainer.checkUser("User2", "Token2");
    }

    @Test
    void clearUsedTokens_UsedTokenAndNoUsedToken_CanUseNotUsedTokensAndCannotUseUsedTokens() {
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(null);
        tokenContainer.registerNewUserToken("User1", "Token1");
        tokenContainer.registerNewUserToken("User2", "Token2");
        tokenContainer.checkUser("User2", "Token2");

        tokenContainer.clearUsedTokens();

        tokenContainer.checkUser("User1", "Token1");
        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("User2", "Token2"));
    }

    @Test
    void clearOldTokens_noTokens_noThrows() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(configuration);

        tokenContainer.clearOldTokens();
    }

    @Test
    void clearOldTokens_noOldTokens_canUseTokens() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        when(configuration.getVolatileResourcesLifeDuration()).thenReturn(1);
        InMemoryUserTokenContainer.TimeProducer timeProducer = mock(InMemoryUserTokenContainer.TimeProducer.class);
        when(timeProducer.getNow()).thenReturn(Date.from(Instant.ofEpochSecond(0)));
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(configuration, timeProducer);

        tokenContainer.registerNewUserToken("User1", "Token1");
        tokenContainer.registerNewUserToken("User2", "Token2");

        tokenContainer.clearOldTokens();

        tokenContainer.checkUser("User1", "Token1");
        tokenContainer.checkUser("User2", "Token2");
    }

    @Test
    void clearOldTokens_newAndOldTokens_canUseNewTokens() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        when(configuration.getVolatileResourcesLifeDuration()).thenReturn(1);
        InMemoryUserTokenContainer.TimeProducer timeProducer = mock(InMemoryUserTokenContainer.TimeProducer.class);
        when(timeProducer.getNow()).thenReturn(Date.from(Instant.ofEpochSecond(0)));
        InMemoryUserTokenContainer tokenContainer = new InMemoryUserTokenContainer(configuration, timeProducer);

        tokenContainer.registerNewUserToken("User1", "Token1");
        when(timeProducer.getNow()).thenReturn(Date.from(Instant.ofEpochSecond(10)));
        tokenContainer.registerNewUserToken("User2", "Token2");

        tokenContainer.clearOldTokens();


        tokenContainer.checkUser("User2", "Token2");
        assertThrows(AuthenticationException.class, () -> tokenContainer.checkUser("User1", "Token1"));
    }

}