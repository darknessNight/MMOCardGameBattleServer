package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameSessionManagerTest {

    com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory getGameSessionFactory() {
        com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory gameSessionFactory = mock(com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory.class);
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession gameSession = mock(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession.class);
        when(gameSessionFactory.create(any(), any(), any())).thenReturn(gameSession);
        return gameSessionFactory;
    }

    com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory getGameSessionFactory(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession gameSession) {
        com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory gameSessionFactory = mock(GameSessionFactory.class);
        when(gameSessionFactory.create(any(), any(), any())).thenReturn(gameSession);
        return gameSessionFactory;
    }

    @Test
    void registerConnectionToSession_withoutSession_throwsNotFoundException() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory());

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test");

        assertThrows(NotFoundException.class, () -> sessionManager.registerConnectionToSession(connection));
    }

    @Test
    void registerConnectionToSession_withSessionButNotForUser_throwsNotFoundException() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory());
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test");

        assertThrows(NotFoundException.class, () -> sessionManager.registerConnectionToSession(connection));
    }

    @Test
    void registerConnectionToSession_withSessionForUser_invokeRegisterConnection() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession gameSession = mock(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession.class);
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory(gameSession));
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test1");

        sessionManager.registerConnectionToSession(connection);

        verify(gameSession).registerConnection(connection);
    }

    @Test
    void registerConnectionToSession_withTwoSessionOneSessionForUser_invokeRegisterConnection() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession gameSession = mock(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession.class);
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory(gameSession));
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings2 = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings2.setListOfUsers(new ArrayList<>(Arrays.asList("Test3", "Test4")));
        gameSessionSettings2.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings2);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test1");

        sessionManager.registerConnectionToSession(connection);

        verify(gameSession).registerConnection(connection);
    }

    @Test
    void removeConnectionFromSession_withSessionForUser_invokeRemoveConnection() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession gameSession = mock(GameSession.class);
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory(gameSession));
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test1");
        sessionManager.registerConnectionToSession(connection);

        sessionManager.removeConnectionFromSession(connection);

        verify(gameSession).removeConnection(connection);
    }

    @Test
    void removeConnectionFromSession_withSessionButNotForUser_invokeKickedUserMessageInConnection() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager(getGameSessionFactory());
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test1");
        sessionManager.registerConnectionToSession(connection);

        UserConnection connection2 = mock(UserConnection.class);
        when(connection2.getUserName()).thenReturn("Unknown");
        assertThrows(NotFoundException.class, () -> sessionManager.removeConnectionFromSession(connection2));
    }

    @Test
    void removeConnectionFromSession_removeAndTryAddConnectionForUser_throwsNotFoundException() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager sessionManager = new GameSessionManager(getGameSessionFactory());
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(new ArrayList<>(Arrays.asList("Test1", "Test2")));
        gameSessionSettings.setTypeOfMatch(GameSessionSettings.MatchType.Single);
        sessionManager.registerNewGameSession(gameSessionSettings);

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("Test1");
        sessionManager.registerConnectionToSession(connection);

        sessionManager.removeConnectionFromSession(connection);

        assertThrows(NotFoundException.class, () -> sessionManager.registerConnectionToSession(connection));
    }


}