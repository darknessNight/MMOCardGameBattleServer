package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection.TransactionsCollection;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

class AbortMessageHandlerTest {

    private TransactionEnvironment transactionEnvironmentMock = Mockito.mock(TransactionEnvironment.class);
    private TransactionsCollection transactionsMock = Mockito.mock(TransactionsCollection.class);

    @BeforeEach
    void setup(){
        Mockito.when(transactionEnvironmentMock.getTransactions()).thenReturn(transactionsMock);
    }

    @Test
    void handleMessage_shouldReturnAbortTypeResponse(){
        var request = new TransactionMessage();
        request.setType(TargetSelectingTransactionType.Abort);
        var handler = new AbortMessageHandler();

        var response = handler.handle(request, transactionEnvironmentMock);

        assertEquals(TargetSelectingTransactionType.Abort, response.getType());
    }

}