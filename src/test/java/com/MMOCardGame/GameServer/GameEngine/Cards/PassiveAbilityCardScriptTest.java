package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PassiveAbilityCardScriptTestImpl extends PassiveAbilityCardScript {
    List<Card> addEffectList = new ArrayList<>();
    List<Card> removeEffectList = new ArrayList<>();
    List<CardPosition.Position> cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
    boolean twoUsers = false;

    @Override
    protected List<CardPosition> getAffectedPositions() {
        if (twoUsers) {
            List<CardPosition> result = cardPositions.stream().map(position -> new CardPosition(0, position)).collect(Collectors.toList());
            result.addAll(cardPositions.stream().map(position -> new CardPosition(1, position)).collect(Collectors.toList()));
            return result;
        } else {
            return cardPositions.stream().map(position -> new CardPosition(parentPosition.getOwner(), position)).collect(Collectors.toList());
        }
    }

    @Override
    protected void addEffectToCard(Card card) {
        addEffectList.add(card);
    }

    @Override
    protected void removeEffectFromCard(Card card) {
        removeEffectList.add(card);
    }
}

class PassiveAbilityCardScriptTest {
    @Test
    void addEffectToCard_cardWithScriptEnterBattlefield_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3)), script.addEffectList);
    }

    private void assertEqualsListOrderNoMatter(List<Card> expected, List<Card> result) {
        assertEquals(new HashSet<>(expected), new HashSet<>(result));
    }

    private GameState getGameStateWithInitiatedCardsFieldsToBattlefield(PassiveAbilityCardScriptTestImpl script, int cardWithScriptId) {
        GameState gameState = getGameStateWithInitiatedCardsFields(script, cardWithScriptId);
        gameState.moveCardToBattlefield(1);
        gameState.moveCardToBattlefield(3);
        gameState.moveCardToBattlefield(6);
        return gameState;
    }

    private GameState getGameStateWithInitiatedCardsFields(PassiveAbilityCardScriptTestImpl script, int cardWithScriptId) {
        GameState gameState = new GameState(new int[]{0, 1});
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCardWithScript(cardWithScriptId, script), getCard(3)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(6), getCard(4), getCard(5)));
        return gameState;
    }

    private Card getCardWithScript(int id, CardScript cardScript) {
        return new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", cardScript);
    }

    private Card getCard(int id) {
        return new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new CardScript());
    }

    private Card getRevealedCard(int id) {
        Card card = new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new CardScript());
        card.setRevealed(true);
        return card;
    }

    @Test
    void addEffectToCard_cardWithScriptEnterBattlefieldWithRangeOnTwoUsers_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3), getRevealedCard(6)), script.addEffectList);
    }

    @Test
    void addEffectToCard_cardWithScriptOnBattlefield_affectAllCardsMovingToBattlefield() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFields(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToBattlefield(1);
        gameState.moveCardToBattlefield(3);
        gameState.moveCardToBattlefield(6);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3)), script.addEffectList);
    }

    @Test
    void addEffectToCard_cardWithScriptOnBattlefieldWithRangeOnTwoUsers_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFields(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToBattlefield(1);
        gameState.moveCardToBattlefield(3);
        gameState.moveCardToBattlefield(6);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3), getRevealedCard(6)), script.addEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptLeaveBattlefield_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToGraveyard(cardWithScriptId);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3)), script.removeEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptEnterBattlefieldWithRangeOnTwoUsers_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToGraveyard(cardWithScriptId);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3), getRevealedCard(6)), script.removeEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptOnBattlefield_affectAllCardsMovingToBattlefield() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToGraveyard(1);
        gameState.moveCardToGraveyard(3);
        gameState.moveCardToGraveyard(6);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3)), script.removeEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptOnBattlefieldWithRangeOnTwoUsers_affectAllCardsFromSpecifiedPositions() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        gameState.moveCardToGraveyard(1);
        gameState.moveCardToGraveyard(3);
        gameState.moveCardToGraveyard(6);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3), getRevealedCard(6)), script.removeEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptOnBattlefieldWithRangeOnTwoUsersChangeOwner_noAffectCards() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        script.removeEffectList.clear();
        gameState.changeCardOwner(cardWithScriptId, 1);

        assertEqualsListOrderNoMatter(new ArrayList<>(), script.removeEffectList);
    }

    @Test
    void addEffectToCard_cardWithScriptOnBattlefieldWithRangeOnTwoUsersChangeOwner_noAffectCards() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        script.twoUsers = true;
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        script.addEffectList.clear();
        gameState.changeCardOwner(cardWithScriptId, 1);

        assertEqualsListOrderNoMatter(new ArrayList<>(), script.addEffectList);
    }

    @Test
    void removeEffectToCard_cardWithScriptOnBattlefieldWithRangeOnOneUsersChangeOwner_affectCards() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        script.removeEffectList.clear();
        gameState.changeCardOwner(cardWithScriptId, 1);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(1), getRevealedCard(3)), script.removeEffectList);
    }

    @Test
    void addEffectToCard_cardWithScriptOnBattlefieldWithRangeOnOneUsersChangeOwner_affectCards() {
        PassiveAbilityCardScriptTestImpl script = new PassiveAbilityCardScriptTestImpl();
        int cardWithScriptId = 2;
        script.cardPositions = Arrays.asList(CardPosition.Position.Battlefield);
        GameState gameState = getGameStateWithInitiatedCardsFieldsToBattlefield(script, cardWithScriptId);

        gameState.moveCardToBattlefield(cardWithScriptId);
        script.addEffectList.clear();
        gameState.changeCardOwner(cardWithScriptId, 1);

        assertEqualsListOrderNoMatter(Arrays.asList(getRevealedCard(6)), script.addEffectList);
    }
}