package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader.CardInfo;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CardFactoryTest {
    private static final int noExistingId = 10000;
    private static final int linkId = 1;
    private static final int unitId = 5;
    private final int namedAbilityId = 1;
    private Integer namedAbility2Id = 2;

    @BeforeAll
    static void switchOffParallelismForParallelStream() {
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "1");
    }

    @AfterAll
    static void switchOnParallelismForParallelStream() {
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", Integer.toString(Runtime.getRuntime().availableProcessors()));
    }

    private Random getRandomGenerator() {
        Random random = mock(Random.class);
        when(random.nextInt()).thenReturn(1);
        when(random.nextInt(anyInt())).thenReturn(1);
        return random;
    }

    private List<CardInfo> getLinkAndUnitCardInfo() {
        List<CardInfo> cardInfo = new ArrayList<>();
        cardInfo.add(getBasicLinkCard());
        cardInfo.add(getUnitCardInfo());
        return cardInfo;
    }

    private CardInfoReader.CardInfo getUnitCardInfo() {
        CardInfoReader.CardInfo cardInfo = new CardInfoReader.CardInfo();
        cardInfo.id = 5;
        cardInfo.statistics = new CardInfoReader.CardInfo.Statistics();
        cardInfo.statistics.attack = 2;
        cardInfo.statistics.defence = 1;
        cardInfo.abilities = Collections.singletonList(namedAbilityId);
        cardInfo.cost = new CardInfoReader.CardInfo.Costs();
        cardInfo.cost.neutral = 1;
        cardInfo.cost.raiders = 1;
        cardInfo.type = "Unit";
        cardInfo.cardClass = "SI";
        cardInfo.subclass = "Szarza";
        return cardInfo;
    }

    private CardInfoReader.CardInfo getBasicLinkCard() {
        CardInfoReader.CardInfo cardInfo = new CardInfoReader.CardInfo();
        cardInfo.id = 1;
        cardInfo.type = "Link";
        cardInfo.subtype = "Basic";
        return cardInfo;
    }

    private Card getLinkCard(int instanceId) {
        return new Card(linkId, instanceId, new CardStatistics(), new ArrayList<>(), CardType.Link, CardSubtype.Basic, null, null, new CardScript());
    }

    private Card getUnitCard(int instanceId) {
        CardStatistics cardStatistics = new CardStatistics(1, 2, 1, 1, 0, 0, 0, 0);
        return new Card(unitId, instanceId, cardStatistics, Collections.singletonList(1), CardType.Unit, CardSubtype.Basic, "SI", "Szarza", new CardScript());
    }

    @Ignore
    void createCard_hasLinkCardInfo_returnsLinkObject() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(linkId);

        assertEquals(getLinkCard(1), result);
    }

    private CardScriptFinder getCardScriptFinderReturnsNullMock() {
        CardScriptFinder cardScriptFinder = mock(CardScriptFinder.class);
        when(cardScriptFinder.findCardScripts()).thenReturn(new HashMap<>());
        when(cardScriptFinder.findNamedAbilitiesScripts()).thenReturn(new HashMap<>());
        return cardScriptFinder;
    }

    @Ignore
    void createCard_hasFullInfoUnitCardInfo_returnsFullInfoUnitObject() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);

        assertEquals(getUnitCard(1), result);
    }

    @Ignore
    void createListOfCard_hasInfoAboutCards_returnTwoValidObjects() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .setRandom(getRandomGenerator())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        List<Card> result = cardFactory.createListOfCards(Arrays.asList(unitId, linkId));

        List<Card> expected = Arrays.asList(getUnitCard(1), getLinkCard(2));
        assertEquals(expected, result);
    }

    @Test
    void createListOfCard_hasOneIncorrectId_throwsNotFoundCardException() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        assertThrows(NotFoundCardException.class, () -> cardFactory.createListOfCards(Arrays.asList(unitId, linkId, noExistingId)));
    }

    @Test
    void createCard_useNoExistingCardId_throwsNotFoundCardException() {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();
        assertThrows(NotFoundCardException.class, () -> cardFactory.createCard(noExistingId));
    }

    @Test
    void createCard_hasCardInfo_returnsObjectWithBasicCardScript() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderReturnsNullMock())
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);

        assertEquals(CardScript.class, result.getCardScript().getClass());
    }

    @Test
    void createCard_hasCardInfoAndSpecialScript_returnsObjectWithSpecialCardScript() throws NotFoundCardException {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        when(cardScriptFinder.findCardScripts()).thenReturn(Collections.singletonMap(unitId, SpecialScript.class));
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(cardScriptFinder)
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);

        assertEquals(SpecialScript.class, result.getCardScript().getClass());
    }

    @Test
    void createCard_hasCardInfoAndNamedAbilitiesScript_returnsObjectWithNamedAbilityCardScript() throws NotFoundCardException {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        when(cardScriptFinder.findNamedAbilitiesScripts()).thenReturn(Collections.singletonMap(namedAbilityId, SpecialScript.class));
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(cardScriptFinder)
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);

        assertEquals(1, result.getCardScript().getSubscripts().size());
        assertEquals(SpecialScript.class, result.getCardScript().getSubscripts().toArray()[0].getClass());
    }

    @Test
    void createCard_hasCardInfoWithTwoNamedAbilitiesScript_returnsObjectWithTwoNamedAbilityCardScript() throws NotFoundCardException {
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(getCardScriptFinderWithTwoNamedAbilities())
                .convertListOfCardInfo(getCardsInfoWithTwoNamedAbilitiesCard())
                .build();

        Card result = cardFactory.createCard(unitId);

        Set<Class<?>> expectedClasses = new HashSet<>(Arrays.asList(SpecialScript.class, SecondSpecialScript.class));
        Set<Class<?>> resultClasses = result.getCardScript().getSubscripts().stream().map(CardScript::getClass).collect(Collectors.toSet());
        assertEquals(expectedClasses, resultClasses);
    }

    private List<CardInfo> getCardsInfoWithTwoNamedAbilitiesCard() {
        List<CardInfo> cardsInfo = getLinkAndUnitCardInfo();
        cardsInfo.get(1).abilities = Arrays.asList(namedAbilityId, namedAbility2Id);
        return cardsInfo;
    }

    private CardScriptFinder getCardScriptFinderWithTwoNamedAbilities() {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        Map<Integer, Class<? extends CardScript>> namedAbilities = new HashMap<>();
        namedAbilities.put(namedAbilityId, SpecialScript.class);
        namedAbilities.put(namedAbility2Id, SecondSpecialScript.class);
        when(cardScriptFinder.findNamedAbilitiesScripts()).thenReturn(namedAbilities);
        return cardScriptFinder;
    }

    @Test
    void createCard_hasCardInfoWithSpecialScriptAndNamedAbilityScript_returnsObjectWithSpecialScriptWithNamedAbilityCardScript() throws NotFoundCardException {
        CardScriptFinder cardScriptFinder = getCardScriptFinderWithCardScriptAndNamedAbilityScript();
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(cardScriptFinder)
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);

        assertEquals(SpecialScript.class, result.getCardScript().getClass());
        assertEquals(SecondSpecialScript.class, result.getCardScript().getSubscripts().toArray()[0].getClass());
    }

    private CardScriptFinder getCardScriptFinderWithCardScriptAndNamedAbilityScript() {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        when(cardScriptFinder.findCardScripts()).thenReturn(Collections.singletonMap(unitId, SpecialScript.class));
        when(cardScriptFinder.findNamedAbilitiesScripts()).thenReturn(Collections.singletonMap(namedAbilityId, SecondSpecialScript.class));
        return cardScriptFinder;
    }

    @Test
    void createCard_createTwoCardWithBasicScript_shouldHaveDifferentScriptObjects() throws NotFoundCardException {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(cardScriptFinder)
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);
        Card result2 = cardFactory.createCard(unitId);

        assertNotSame(result.getCardScript(), result2.getCardScript());
    }

    @Test
    void createCard_createTwoCardWithSpecialScript_shouldHaveDifferentScriptObjects() throws NotFoundCardException {
        CardScriptFinder cardScriptFinder = getCardScriptFinderReturnsNullMock();
        when(cardScriptFinder.findCardScripts()).thenReturn(Collections.singletonMap(unitId, SpecialScript.class));
        CardFactory cardFactory = CardFactory.builder()
                .setCardScriptFinder(cardScriptFinder)
                .convertListOfCardInfo(getLinkAndUnitCardInfo())
                .build();

        Card result = cardFactory.createCard(unitId);
        Card result2 = cardFactory.createCard(unitId);

        assertNotSame(result.getCardScript(), result2.getCardScript());
    }

    public static class SpecialScript extends CardScript {
        public SpecialScript() {

        }
    }

    public static class SecondSpecialScript extends CardScript {
    }
}