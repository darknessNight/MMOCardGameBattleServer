package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.CustomOptionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.RunningTransactionStep;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class OptionsStepDefinitionTest {

    private GameState gameStateMock;
    private TargetsStepDefinition targetsStepDefinitionMock;
    private Transaction transactionMock;

    @BeforeEach
    void setup(){
        gameStateMock = Mockito.mock(GameState.class);
        targetsStepDefinitionMock = Mockito.mock(TargetsStepDefinition.class);
        transactionMock = Mockito.mock(Transaction.class);
    }

    @Test
    void createOptionsStep_withThreeOptions_shouldReturnThreeOptionsInOptionsInfo(){
        var optionsTargets = new ArrayList<CustomOptionDefinition>();
        var option1 = new CustomOptionDefinition("option1", stepEnvironment -> {});
        optionsTargets.add(option1);
        var option2 = new CustomOptionDefinition("option2", stepEnvironment -> {});
        optionsTargets.add(option2);
        var option3 = new CustomOptionDefinition("option3", stepEnvironment -> {});
        optionsTargets.add(option3);
        var optionsStep = new OptionsStepDefinition(optionsTargets, 1);

        var result = optionsStep.getPossibleOptionsInfo(gameStateMock, -1);

        assertEquals(3, result.getOptions().size());
    }

    @Test
    void selectFirstOption_withFirstOptionAddingStepAfterCurrent_shouldCallAddStep(){
        var optionsTargets = new ArrayList<CustomOptionDefinition>();
        var option1 = new CustomOptionDefinition("option1",
                stepEnvironment -> stepEnvironment
                                        .getTransaction()
                                        .addStepAfterCurrent(new RunningTransactionStep(targetsStepDefinitionMock)));
        optionsTargets.add(option1);
        var optionsStep = new OptionsStepDefinition(optionsTargets, 1);


        var optionsInfo = optionsStep.getPossibleOptionsInfo(gameStateMock, -1);
        ArrayList<TargetSelectingOption> selectedOption = new ArrayList<>();
        selectedOption.add(optionsInfo.getOptions().get(0));
        optionsStep.getSelectedOptionsHandler().accept(selectedOption, new StepEnvironment(transactionMock, gameStateMock));


        verify(transactionMock, times(1)).addStepAfterCurrent(any());
    }

}