package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.CustomOptionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.CardsInHandStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.OptionsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TransactionsIT {

    private GameState gameStateMock;
    private TargetsStepDefinition targetsStepDefinitionMock;
    private Transaction transactionMock;
    private static Card parentCard;

    @BeforeEach
    void setup(){
        gameStateMock = Mockito.mock(GameState.class);
        targetsStepDefinitionMock = Mockito.mock(TargetsStepDefinition.class);
        transactionMock = Mockito.mock(Transaction.class);
        parentCard = Mockito.mock(Card.class);
        Mockito.when(parentCard.getOverloadPoints()).thenReturn(0);
    }

    @Test
    void integrationTest(){

        var cardScript = new TestCardScript();

        var request1 = new TransactionMessage();
        request1.setStartedFor(TargetSelectingTransaction.StartTransactionFor.Attack);
        request1.setType(TargetSelectingTransactionType.StartTransaction);

        var response1 = cardScript.runTransactionAction(request1.getGrpcFormat(), gameStateMock);


        var request2 = new TransactionMessage();
        request2.setStartedFor(response1.getTransactionStartedFor());
        request2.setType(TargetSelectingTransactionType.SelectingOption);
        request2.setTransactionId(response1.getTransactionId());
        var selected2 = new ArrayList<TargetSelectingOption>();
        selected2.add(response1.getOptionsList().get(0));
        request2.getOptionsInfo().setOptions(selected2);

        var response2 = cardScript.runTransactionAction(request2.getGrpcFormat(), gameStateMock);

        var request3 = new TransactionMessage();
        request3.setStartedFor(response2.getTransactionStartedFor());
        request3.setType(TargetSelectingTransactionType.SelectingOption);
        request3.setTransactionId(response2.getTransactionId());

        var response3 = cardScript.runTransactionAction(request3.getGrpcFormat(), gameStateMock);

        var request4 = new TransactionMessage();
        request4.setStartedFor(response3.getTransactionStartedFor());
        request4.setType(TargetSelectingTransactionType.SelectingOption);
        request4.setTransactionId(response3.getTransactionId());

        var response4 = cardScript.runTransactionAction(request4.getGrpcFormat(), gameStateMock);
        var response4JavaType = new TransactionMessage(response4);

        assertEquals(TargetSelectingTransactionType.Result, response4JavaType.getType());
        assertEquals(0, response4JavaType.getOptionsInfo().getOptions().size());
    }

    private class TestCardScript extends CardScript {

        public TestCardScript(){
            super();
            this.parentCard = TransactionsIT.parentCard;
        }

        @Override
        protected Map<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> getTransactionsDefinitions()
        {
            var definitions = new HashMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>();
            definitions.put(TargetSelectingTransaction.StartTransactionFor.Attack, getTransactionDefinitionStartedForAttack());
            return definitions;
        }

        private TransactionDefinition getTransactionDefinitionStartedForAttack()
        {
            var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);

            var stepPossibleOptions = new ArrayList<CustomOptionDefinition>();
            stepPossibleOptions.add(new CustomOptionDefinition("option 1", stepEnvironment ->
            {
                stepEnvironment.getTransaction()
                        .addStepAfterCurrent(
                                new RunningTransactionStep(
                                        new CardsInHandStepDefinition(card -> true, (gameState, targetsList) -> 1
                                        )));
            }));
            stepPossibleOptions.add(new CustomOptionDefinition("option 2", stepEnvironment ->
            {
                stepEnvironment.getTransaction()
                        .addStepAfterCurrent(
                                new RunningTransactionStep(
                                        new CardsInHandStepDefinition(card -> true, (gameState, targetsList) -> 1
                                        )));
            }));
            attackTransactionDefinition.addStep(new OptionsStepDefinition(stepPossibleOptions, (gameState, targetsList) -> 1));

            attackTransactionDefinition.addStep(new CardsInHandStepDefinition(card -> true, (gameState, targetsList) -> 1));
            return attackTransactionDefinition;
        }
    }

    @Test
    void getSelectedOptions_inTransactionWithOnlyOptionSelectionStep_shouldReturnSelectedOptionMessage(){
        var script = new TransactionsIT.OptionsInTransactionTestScript();
        var startRequest = new TransactionMessage();
        startRequest.setType(TargetSelectingTransactionType.StartTransaction);

        startRequest.setStartedFor(TargetSelectingTransaction.StartTransactionFor.Attack);

        var responseWithOptions = script.runTransactionAction(startRequest.getGrpcFormat(), gameStateMock);
        var select1OptionRequest = new TransactionMessage();
        select1OptionRequest.setStartedFor(responseWithOptions.getTransactionStartedFor());
        select1OptionRequest.setType(TargetSelectingTransactionType.SelectingOption);
        select1OptionRequest.setTransactionId(responseWithOptions.getTransactionId());
        var selectedOptions = new ArrayList<TargetSelectingOption>();
        selectedOptions.add(responseWithOptions.getOptions(0));
        var selectedOptionsInfo = new OptionsInfo();
        selectedOptionsInfo.setOptionsType(responseWithOptions.getOptionsType());
        selectedOptionsInfo.setOptions(selectedOptions);
        selectedOptionsInfo.setCountOfTargets(selectedOptions.size());
        select1OptionRequest.setOptionsInfo(selectedOptionsInfo);

        var resultResponse = script.runTransactionAction(select1OptionRequest.getGrpcFormat(), gameStateMock);


        var selectedOptionsMessages = script.getSelectedOptionsDuringTransaction(resultResponse.getTransactionId());


        Assertions.assertEquals(selectedOptionsMessages.size(), 1);
        selectedOptionsMessages.forEach(m ->
                Assertions.assertEquals(TargetSelectingTransaction.OptionsType.Option, m.getOptionsType())
        );
    }

    @Test
    void getSelectedOptions_inTransactionCardsAndPlayersOptionsSelectionStep_shouldNotReturnSelectedOptionsMessages(){
        var script = new TransactionsIT.CardsAndPlayersOptionsInTransactionTestScript();
        var startRequest = new TransactionMessage();
        startRequest.setType(TargetSelectingTransactionType.StartTransaction);
        startRequest.setStartedFor(TargetSelectingTransaction.StartTransactionFor.Attack);

        var responseWithCardsOptions = script.runTransactionAction(startRequest.getGrpcFormat(), gameStateMock);

        var selectCardsOptionRequest = new TransactionMessage();
        selectCardsOptionRequest.setStartedFor(responseWithCardsOptions.getTransactionStartedFor());
        selectCardsOptionRequest.setType(TargetSelectingTransactionType.SelectingOption);
        selectCardsOptionRequest.setTransactionId(responseWithCardsOptions.getTransactionId());
        var selectedOptionsInfo = new OptionsInfo();
        selectedOptionsInfo.setOptionsType(responseWithCardsOptions.getOptionsType());
        selectedOptionsInfo.setOptions(new ArrayList<>());
        selectedOptionsInfo.setCountOfTargets(0);
        selectCardsOptionRequest.setOptionsInfo(selectedOptionsInfo);

        var responseWithCardAndPlayersSelectionOption = script.runTransactionAction(selectCardsOptionRequest.getGrpcFormat(), gameStateMock);

        var selectPlayersAndCardsOptionRequest = new TransactionMessage();
        selectPlayersAndCardsOptionRequest.setStartedFor(responseWithCardsOptions.getTransactionStartedFor());
        selectPlayersAndCardsOptionRequest.setType(TargetSelectingTransactionType.SelectingOption);
        selectPlayersAndCardsOptionRequest.setTransactionId(responseWithCardsOptions.getTransactionId());
        var selectedPaCOptionsInfo = new OptionsInfo();
        selectedPaCOptionsInfo.setOptionsType(responseWithCardAndPlayersSelectionOption.getOptionsType());
        selectedPaCOptionsInfo.setOptions(new ArrayList<>());
        selectedPaCOptionsInfo.setCountOfTargets(0);
        selectPlayersAndCardsOptionRequest.setOptionsInfo(selectedOptionsInfo);

        var resultResponse = script.runTransactionAction(selectPlayersAndCardsOptionRequest.getGrpcFormat(), gameStateMock);


        var selectedOptionsMessages = script.getSelectedOptionsDuringTransaction(resultResponse.getTransactionId());


        Assertions.assertEquals(selectedOptionsMessages.size(), 2);
        selectedOptionsMessages.forEach(m ->
                assertNotEquals(TargetSelectingTransaction.OptionsType.Option, m.getOptionsType())
        );
    }

    private class OptionsInTransactionTestScript extends CardScript{

        public OptionsInTransactionTestScript(){
            super();
            this.parentCard = TransactionsIT.parentCard;
        }

        @Override
        protected Map<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> getTransactionsDefinitions()
        {
            var definitions = new HashMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>();
            definitions.put(TargetSelectingTransaction.StartTransactionFor.Attack, getTransactionDefinitionStartedForAttack());
            return definitions;
        }

        private TransactionDefinition getTransactionDefinitionStartedForAttack()
        {
            var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);

            var stepPossibleOptions = new ArrayList<CustomOptionDefinition>();
            stepPossibleOptions.add(new CustomOptionDefinition("option 1", stepEnvironment -> {}));
            stepPossibleOptions.add(new CustomOptionDefinition("option 2", stepEnvironment -> {}));
            attackTransactionDefinition.addStep(new OptionsStepDefinition(stepPossibleOptions, (gameState, targetsList) -> 1));

            return attackTransactionDefinition;
        }
    }

    private class CardsAndPlayersOptionsInTransactionTestScript extends CardScript{

        public CardsAndPlayersOptionsInTransactionTestScript(){
            super();
            this.parentCard = TransactionsIT.parentCard;
        }

        @Override
        protected Map<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> getTransactionsDefinitions()
        {
            var definitions = new HashMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>();
            definitions.put(TargetSelectingTransaction.StartTransactionFor.Attack, getTransactionDefinitionStartedForAttack());
            return definitions;
        }

        private TransactionDefinition getTransactionDefinitionStartedForAttack()
        {
            var attackTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);

            attackTransactionDefinition.addStep(new CardsInHandStepDefinition(card -> true, 1));
            attackTransactionDefinition.addStep(new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, 1));

            return attackTransactionDefinition;
        }
    }

}
