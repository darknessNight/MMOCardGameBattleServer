package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardCreatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import com.MMOCardGame.GameServer.common.Pair;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameStateTest {

    private final int noExistingCardInstanceId = 1000;

    @Test
    void setListOfActiveUsers_hasTwoUsers_checkUsersHasDifferentIds() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        int player1Id = gameState.getPlayerId("Test1");
        int player2Id = gameState.getPlayerId("Test2");

        assertNotEquals(player1Id, player2Id);
    }

    @Test
    void setListOfActiveUsers_hasTwoUsers_getListOfActiveUsersReturnsSameUsers() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        Collection<String> result = gameState.getListOfActiveUsers();

        assertEquals(new HashSet<>(Arrays.asList("Test1", "Test2")), result);
    }

    @Test
    void setUserAsInactive_hasTwoUsers_getListOfActiveUsersReturnsOneUser() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setUserAsInactive("Test1");

        Collection<String> result = gameState.getListOfActiveUsers();

        assertEquals(new HashSet<>(Collections.singletonList("Test2")), result);
    }

    @Test
    void addCardsForUser_hasCollectionOfCards_allCardsAreInLibrary() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));

        CardPosition[] results = new CardPosition[3];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);
        results[2] = gameState.getCardPosition(3);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(1, CardPosition.Position.Library)
        };
        assertArrayEquals(expected, results);
    }

    Card getCard(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    @Test
    void getCardPosition_noExistingCard_returnNull() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));

        CardPosition result = gameState.getCardPosition(noExistingCardInstanceId);

        assertNull(result);
    }

    @Test
    void addCardsForUser_hasCollectionOfCardsForTwoUsers_allCardAreInLibrary() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));
        gameState.setCardsForUser(2, Arrays.asList(getCard(4), getCard(5), getCard(6)));

        CardPosition[] results = new CardPosition[6];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);
        results[2] = gameState.getCardPosition(3);
        results[3] = gameState.getCardPosition(4);
        results[4] = gameState.getCardPosition(5);
        results[5] = gameState.getCardPosition(6);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void getCard_hasCollectionOfCardsForTwoUsers_canAccessAllCards() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));
        gameState.setCardsForUser(2, Arrays.asList(getCard(4), getCard(5), getCard(6)));

        Card[] results = new Card[6];
        results[0] = gameState.getCard(1);
        results[1] = gameState.getCard(2);
        results[2] = gameState.getCard(3);
        results[3] = gameState.getCard(4);
        results[4] = gameState.getCard(5);
        results[5] = gameState.getCard(6);

        Card[] expected = new Card[]{
                getCard(1),
                getCard(2),
                getCard(3),
                getCard(4),
                getCard(5),
                getCard(6),
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void removeCard_hasCollectionOfCardsForTwoUsers_cardNoLongerHavePosition() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));
        gameState.setCardsForUser(2, Arrays.asList(getCard(4), getCard(5), getCard(6)));

        gameState.removeCard(2);

        CardPosition[] results = new CardPosition[6];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);
        results[2] = gameState.getCardPosition(3);
        results[3] = gameState.getCardPosition(4);
        results[4] = gameState.getCardPosition(5);
        results[5] = gameState.getCardPosition(6);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                null,
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void removeCard_hasCollectionOfCardsForTwoUsers_cardNoLongerExists() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(2), getCard(3)));
        gameState.setCardsForUser(2, Arrays.asList(getCard(4), getCard(5), getCard(6)));

        gameState.removeCard(2);

        Card[] results = new Card[6];
        results[0] = gameState.getCard(1);
        results[1] = gameState.getCard(2);
        results[2] = gameState.getCard(3);
        results[3] = gameState.getCard(4);
        results[4] = gameState.getCard(5);
        results[5] = gameState.getCard(6);

        Card[] expected = new Card[]{
                getCard(1),
                null,
                getCard(3),
                getCard(4),
                getCard(5),
                getCard(6),
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void changeCardOwner_hasCollectionOfCardsForTwoUsers_cardBelongToSecondUser() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.changeCardOwner(2, 1);

        CardPosition[] results = new CardPosition[2];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(1, CardPosition.Position.Library)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void moveCardToBattlefield_hasCollectionOfCardsForTwoUsers_cardPositionIsBattlefield() {
        GameState gameState = new GameState();
        gameState.setPlayersOrder(new int[]{0, 1});
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.moveCardToBattlefield(2);

        CardPosition[] results = new CardPosition[2];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Battlefield)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void moveCardToHand_hasCollectionOfCardsForTwoUsers_cardPositionIsHand() {
        GameState gameState = new GameState();
        gameState.setPlayersOrder(new int[]{0, 1});
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.moveCardToHand(2);

        CardPosition[] results = new CardPosition[2];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Hand)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void moveCardToGraveyard_hasCollectionOfCardsForTwoUsers_cardPositionIsGraveyard() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.moveCardToGraveyard(2);

        CardPosition[] results = new CardPosition[2];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Graveyard)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void moveCardToLibrary_hasCollectionOfCardsForTwoUsers_cardPositionIsLibrary() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));
        gameState.setPlayersOrder(new int[]{0, 1});

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.moveCardToHand(2);
        gameState.moveCardToLibrary(2);

        CardPosition[] results = new CardPosition[2];
        results[0] = gameState.getCardPosition(1);
        results[1] = gameState.getCardPosition(2);

        CardPosition[] expected = new CardPosition[]{
                new CardPosition(1, CardPosition.Position.Library),
                new CardPosition(2, CardPosition.Position.Library)
        };
        assertArrayEquals(expected, results);
    }

    @Test
    void getPlayerCardsPositions_hasTwoUser_returnAllCards() {
        GameState gameState = new GameState();
        Triggers triggers = mock(Triggers.class);
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));
        gameState.setCardsForUser(1, Arrays.asList(getCard(1), getCard(3)));
        gameState.setCardsForUser(2, Arrays.asList(getCard(2), getCard(4)));
        gameState.setTriggers(triggers);

        Collection<Pair<Card, CardPosition.Position>> player1Result = gameState.getPlayerCardsPositions(1);
        Collection<Pair<Card, CardPosition.Position>> player2Result = gameState.getPlayerCardsPositions(2);

        Collection results = Arrays.asList(player1Result.toArray(), player2Result.toArray());
        Collection expected = Arrays.asList(
                Arrays.asList(new Pair<>(getCard(1), CardPosition.Position.Library),
                        new Pair<>(getCard(3), CardPosition.Position.Library)).toArray(),
                Arrays.asList(new Pair<>(getCard(2), CardPosition.Position.Library),
                        new Pair<>(getCard(4), CardPosition.Position.Library)).toArray());
        assertArrayEquals(expected.toArray(), results.toArray());
    }

    @Test
    void serialize_initGameState_checkCorrectlyDeserialize() throws IOException, ClassNotFoundException {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "test2", "Test3"));
        int j = 0;
        for (int i = 0; i < 3; i++) {
            Player player = new Player(1, i, 20, new int[]{i, 0, 1, 0}, new int[]{1, i, 1, 0}, new int[]{0, 1, 0, i});
            gameState.setCardsForUser(i, Arrays.asList(getCard(j++), getCard(j++), getCard(j++)));
            gameState.setHeroForUser(i, player);
        }
        gameState.setPlayersOrder(new int[]{2, 0, 1});


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(gameState);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bis);
        GameState result = (GameState) in.readObject();

        assertEquals(gameState, result);
    }

    @Test
    void serialize_initGameState_canBeUsed() throws IOException, ClassNotFoundException {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "test2", "Test3"));
        int j = 0;
        for (int i = 0; i < 3; i++) {
            Player player = new Player(1, i, 20, new int[]{i, 0, 1, 0}, new int[]{1, i, 1, 0}, new int[]{0, 1, 0, i});
            gameState.setCardsForUser(i, Arrays.asList(getCard(j++), getCard(j++), getCard(j++), getCardWithCustomScript(j++)));
            gameState.setHeroForUser(i, player);
        }
        gameState.setPlayersOrder(new int[]{2, 0, 1});


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(gameState);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bis);
        GameState result = (GameState) in.readObject();

        gameState.endPhase(2);
        gameState.moveCardToGraveyard(3);
        gameState.getCard(3).changeDefence(0);
    }

    private Card getCardWithCustomScript(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CustomCardScript());
    }

    @Test
    void insertCard_hasCollectionOfCardsForTwoUsers_invokeTrigger() {
        GameState gameState = new GameState();
        Triggers triggers = mock(Triggers.class);
        gameState.setTriggers(triggers);
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        gameState.insertCard(getCard(6), new CardPosition(1, CardPosition.Position.Library));

        verify(triggers).invokeTrigger(new CardCreatedEvent(getCard(6), new CardPosition(1, CardPosition.Position.Library)));
    }

    @Test
    void insertCard_tryInsertCardForNotExistingUser_throwNotFoundException() {
        GameState gameState = new GameState();
        Triggers triggers = mock(Triggers.class);
        gameState.setTriggers(triggers);
        gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));

        gameState.setCardsForUser(1, Collections.singletonList(getCard(1)));
        gameState.setCardsForUser(2, Collections.singletonList(getCard(2)));

        assertThrows(NotFoundException.class, () -> gameState.insertCard(getCard(6), new CardPosition(0, CardPosition.Position.Library)));
    }

    private static class CustomCardScript extends CardScript {
        @Override
        public boolean canDie() {
            return false;
        }
    }
}