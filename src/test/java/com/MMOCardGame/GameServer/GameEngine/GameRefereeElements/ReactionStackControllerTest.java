package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.PhasesStack;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.ReactionStack;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ReactionStackControllerTest {

    @Test
    void doAction_noActions_noSendMessage() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        UserConnection connection1 = getUserConnection(0);
        UserConnection connection2 = getUserConnection(1);
        List<UserConnection> connections = Arrays.asList(connection1, connection2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        reactionStackController.doAction();

        verify(connection1, times(0)).sendMessage(any());
        verify(connection2, times(0)).sendMessage(any());
    }

    private UserConnection getUserConnection(int i) {
        UserConnection connection1 = mock(UserConnection.class);
        when(connection1.getUserId()).thenReturn(i);
        when(connection1.getUserName()).thenReturn("Test" + i);
        return connection1;
    }

    @Test
    void doAction_twoActionsAndDoOne_sendMessageToAllPlayers() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0);

        reactionStackController.doAction();

        StackActionCompleted stackAction1 = StackActionCompleted.newBuilder()
                .setStackId(1)
                .build();
        assertEquals(Arrays.asList(stackAction1), answers1);
        assertEquals(Arrays.asList(stackAction1), answers2);
    }

    @Test
    void doAction_twoActionsAndDoTwo_sendMessagesToAllPlayers() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0);

        reactionStackController.doAction();
        reactionStackController.doAction();

        StackActionCompleted stackAction1 = StackActionCompleted.newBuilder()
                .setStackId(1)
                .build();
        StackActionCompleted stackAction2 = StackActionCompleted.newBuilder()
                .setStackId(0)
                .build();
        assertEquals(Arrays.asList(stackAction1, stackAction2), answers1);
        assertEquals(Arrays.asList(stackAction1, stackAction2), answers2);
    }

    private GameState getGameStateWithReactionStack(ReactionStack reactionStack) {
        GameState gameState = mock(GameState.class);
        PhasesStack phasesStack = mock(PhasesStack.class);
        when(gameState.getReactionStack()).thenReturn(reactionStack);
        when(gameState.getPhasesStack()).thenReturn(phasesStack);
        return gameState;
    }

    private List<UserConnection> getUserConnectionsWithReaders(List<Object> answers1, List<Object> answers2) {
        UserConnection connection1 = getUserConnectionWithReader(answers1, 0);
        UserConnection connection2 = getUserConnectionWithReader(answers2, 1);
        return Arrays.asList(connection1, connection2);
    }

    private UserConnection getUserConnectionWithReader(List<Object> answers1, int i) {
        UserConnection connection1 = getUserConnection(i);
        doAnswer(invocationOnMock -> answers1.add(invocationOnMock.getArgument(0))).when(connection1).sendMessage(any());
        return connection1;
    }

    @Test
    void doAction_oneNormalActionAndTwoCanceled_sendOneMessageToAllPlayers() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0).setCanceled(true);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0).setCanceled(true);
        reactionStack.pushAction(() -> {
        }, () -> {
        }, 0).setCanceled(true);

        reactionStackController.doAction();

        StackActionCompleted stackAction1 = StackActionCompleted.newBuilder()
                .setStackId(0)
                .addAllDeletedStackActions(Arrays.asList(3, 2, 1))
                .build();
        assertEquals(Arrays.asList(stackAction1), answers1);
        assertEquals(Arrays.asList(stackAction1), answers2);
    }

    @Test
    void doAction_twoNormalActions_doActionsAfterSendingMessage() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        List<Object> answers1 = new ArrayList<>();

        UserConnection connection1 = getUserConnection(0);
        UserConnection connection2 = getUserConnection(1);
        doAnswer(invocationOnMock -> answers1.add("Send message")).when(connection1).sendMessage(any());
        ReactionStackController reactionStackController = new ReactionStackController(gameState, Arrays.asList(connection1, connection2));
        reactionStack.pushAction(() -> answers1.add("DoAction0"), () -> {
        }, 0);
        reactionStack.pushAction(() -> answers1.add("DoAction1"), () -> {
        }, 0);

        reactionStackController.doAction();

        assertEquals(Arrays.asList("Send message", "DoAction1"), answers1);
    }

    @Test
    void doAction_twoCanceledNormalActions_doActionsAfterSendingMessage() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        List<Object> answers1 = new ArrayList<>();

        UserConnection connection1 = getUserConnection(0);
        UserConnection connection2 = getUserConnection(1);
        doAnswer(invocationOnMock -> answers1.add("Send message")).when(connection1).sendMessage(any());
        ReactionStackController reactionStackController = new ReactionStackController(gameState, Arrays.asList(connection1, connection2));
        reactionStack.pushAction(() -> {
        }, () -> answers1.add("DoAction0"), 0);
        reactionStack.pushAction(() -> {
        }, () -> answers1.add("DoAction1"), 0).setCanceled(true);

        reactionStackController.doAction();

        assertEquals(Arrays.asList("DoAction1", "Send message"), answers1);
    }

    @Test
    void pushCardAction_withoutOptionalInfoAndWithCardOnBattlefield_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Battlefield));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        answers1.add(reactionStackController.pushCardAction(getCard(1), 0, CardCosts.newBuilder().build()));

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardAbilityId(0)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(1)
                .setPlayer(0)
                .build();
        assertEquals(Arrays.asList(cardUsed), answers1);
        assertEquals(Arrays.asList(cardUsed), answers2);
    }

    private Card getCard(int id) {
        return new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new TestCardScript());
    }

    @Test
    void pushCardAction_withoutOptionalInfoAndWithTwoCardOnBattlefield_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Battlefield));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        answers1.add(reactionStackController.pushCardAction(getCard(1), 0, CardCosts.newBuilder().build()));
        answers1.add(reactionStackController.pushCardAction(getCard(2), 0, CardCosts.newBuilder().build()));

        CardUsed cardUsed1 = CardUsed.newBuilder()
                .setCardAbilityId(0)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(1)
                .setPlayer(0)
                .setStackId(0)
                .build();
        CardUsed cardUsed2 = CardUsed.newBuilder()
                .setCardAbilityId(0)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(2)
                .setPlayer(0)
                .setStackId(1)
                .build();
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers1);
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers2);
    }

    @Test
    void pushCardAction_withoutOptionalInfoAndWithTwoCardInHand_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Hand));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        answers1.add(reactionStackController.pushCardAction(getCard(1), 0, CardCosts.newBuilder().build()));
        answers1.add(reactionStackController.pushCardAction(getCard(2), 0, CardCosts.newBuilder().build()));

        CardUsed cardUsed1 = CardUsed.newBuilder()
                .setCardAbilityId(-1)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(1)
                .setPlayer(0)
                .setStackId(0)
                .build();
        CardUsed cardUsed2 = CardUsed.newBuilder()
                .setCardAbilityId(-1)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(2)
                .setPlayer(0)
                .setStackId(1)
                .build();
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers1);
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers2);
    }

    @Test
    void pushCardAction_withoutOptionalInfoAndWithTwoCardInHand_invokeStartReactionPhases() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Hand));
        List<UserConnection> connections = Arrays.asList(getUserConnection(0), getUserConnection(1));
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        reactionStackController.pushCardAction(getCard(1), 0, CardCosts.newBuilder().build());

        PhasesStack phasesStack = gameState.getPhasesStack();
        verify(phasesStack).startReactionPhases();
    }

    @Test
    void pushCardAction_withOptionalInfoAndWithTwoCardOnBattlefield_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Battlefield));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        answers1.add(reactionStackController.pushCardActionWithDecisionAndTargets(getCard(1), 0, 1, CardCosts.newBuilder().build()));
        answers1.add(reactionStackController.pushCardActionWithDecisionAndTargets(getCard(2), 0, 3, CardCosts.newBuilder().build()));

        CardUsed cardUsed1 = CardUsed.newBuilder()
                .setCardAbilityId(1)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(1)
                .setPlayer(0)
                .setStackId(0)
                .build();
        CardUsed cardUsed2 = CardUsed.newBuilder()
                .setCardAbilityId(3)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(2)
                .setPlayer(0)
                .setStackId(1)
                .build();
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers1);
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers2);
    }

    @Test
    void pushCardAction_withOptionalInfoAndWithTwoCardInHand_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Hand));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        answers1.add(reactionStackController.pushCardActionWithDecisionAndTargets(getCard(1), 0, 1, CardCosts.newBuilder().build()));
        answers1.add(reactionStackController.pushCardActionWithDecisionAndTargets(getCard(2), 0, 3, CardCosts.newBuilder().build()));

        CardUsed cardUsed1 = CardUsed.newBuilder()
                .setCardAbilityId(1)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(1)
                .setPlayer(0)
                .setStackId(0)
                .build();
        CardUsed cardUsed2 = CardUsed.newBuilder()
                .setCardAbilityId(3)
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardInstanceId(2)
                .setPlayer(0)
                .setStackId(1)
                .build();
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers1);
        assertEquals(Arrays.asList(cardUsed1, cardUsed2), answers2);
    }

    @Test
    void pushTriggerAction_twoCards_sendMessageToAll() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);

        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        reactionStackController.pushTriggerAction((g) -> {
                }, getCard(1), TriggerType.CardAttacked,
                Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()));
        reactionStackController.pushTriggerAction((g) -> {
                }, getCard(2), TriggerType.CardAttacked,
                Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()));

        TriggerActivated message1 = TriggerActivated.newBuilder()
                .setCardInstanceId(1)
                .setStackId(0)
                .setTriggerTypeValue(TriggerType.CardAttacked.ordinal())
                .addAllTransaction(Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()))
                .build();
        TriggerActivated message2 = TriggerActivated.newBuilder()
                .setCardInstanceId(2)
                .setStackId(1)
                .setTriggerTypeValue(TriggerType.CardAttacked.ordinal())
                .addAllTransaction(Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()))
                .build();
        assertEquals(Arrays.asList(message1, message2), answers1);
        assertEquals(Arrays.asList(message1, message2), answers2);
    }

    @Test
    void pushTriggerAction_twoCards_invokePhaseChangeTwoTimes() {
        ReactionStack reactionStack = new ReactionStack();
        GameState gameState = getGameStateWithReactionStack(reactionStack);

        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        List<UserConnection> connections = getUserConnectionsWithReaders(answers1, answers2);
        ReactionStackController reactionStackController = new ReactionStackController(gameState, connections);

        reactionStackController.pushTriggerAction((g) -> {
                }, getCard(1), TriggerType.CardAttacked,
                Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()));
        reactionStackController.pushTriggerAction((g) -> {
                }, getCard(2), TriggerType.CardAttacked,
                Arrays.asList(TargetSelectingTransaction.newBuilder().getDefaultInstanceForType(), TargetSelectingTransaction.newBuilder().getDefaultInstanceForType()));

        PhasesStack mockedStack = gameState.getPhasesStack();
        verify(mockedStack, times(2)).startReactionPhasesInsteadTriggerPhase(any(), anyInt());
    }

    static class TestCardScript extends CardScript {
        @Override
        public CardAbilityTask playAction(GameState gameState, int decisionId) {
            return new CardAbilityTask() {
                @Override
                public void run(GameState gameState) {
                }

                @Override
                public void runCanceled(GameState gameState) {
                }

                @Override
                public int getAbilityId() {
                    if (decisionId == -1)
                        return -1;
                    else return decisionId;
                }
            };
        }

        @Override
        public CardAbilityTask abilityAction(GameState gameState, int decisionId) {
            return new CardAbilityTask() {
                @Override
                public void run(GameState gameState) {
                }

                @Override
                public void runCanceled(GameState gameState) {
                }

                @Override
                public int getAbilityId() {
                    if (decisionId == -1)
                        return 0;
                    else return decisionId;
                }
            };
        }

        @Override
        public Collection<TargetSelectingTransaction> getSelectedOptionsDuringTransaction(int transactionId){
            return new ArrayList<>();
        }
    }
}