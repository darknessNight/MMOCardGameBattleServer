package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader.CardInfo.Costs;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerLinkPointsChangedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

class LinksPointsTest {
    @Test
    void addPoints_somePoints_invokeTrigger(){
        Triggers triggers=mock(Triggers.class);
        LinksPoints linksPoints=new LinksPoints(2);
        linksPoints.setTriggers(triggers);

        linksPoints.addPoints(new LinksPoints(1,1,1,1,1));

        verify(triggers).invokeTrigger(new PlayerLinkPointsChangedEvent(2, new Costs(0,0,0,0,0,0),
                new Costs(1,1,1,1,1,0)));
    }
    @Test
    void removePointsIfPossible_somePoints_invokeTrigger(){
        Triggers triggers=mock(Triggers.class);
        LinksPoints linksPoints=new LinksPoints(1,1,1,1,1,2,triggers);

        linksPoints.removePointsIfPossible(new LinksPoints(1,1,1,1,1));

        verify(triggers).invokeTrigger(new PlayerLinkPointsChangedEvent(2, new Costs(1,1,1,1,1,0),
                new Costs(0,0,0,0,0,0)));
    }

    @Test
    void removePointsIfPossible_somePointsWithoutNeutral_invokeTrigger(){
        Triggers triggers=mock(Triggers.class);
        LinksPoints linksPoints=new LinksPoints(0,2,1,1,1,2,triggers);
        linksPoints.setTriggers(triggers);

        linksPoints.removePointsIfPossible(new LinksPoints(1,1,1,1,1));

        verify(triggers).invokeTrigger(new PlayerLinkPointsChangedEvent(2, new Costs(0,2,1,1,1,0),
                new Costs(0,0,0,0,0,0)));
    }

    @Test
    void removePointsIfPossible_notEnoughPoints_noInvokeTrigger(){
        Triggers triggers=mock(Triggers.class);
        LinksPoints linksPoints=new LinksPoints(0,2,1,1,1,2,triggers);
        linksPoints.setTriggers(triggers);

        linksPoints.removePointsIfPossible(new LinksPoints(6,1,1,1,1));

        verifyZeroInteractions(triggers);
    }
}