package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.common.Observer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class BasicGameSessionManagerBuilderTest {

    @Test
    void build_allDependencies_returnGameSessionManager() {
        BasicGameSessionManagerBuilder builder = new BasicGameSessionManagerBuilder();
        builder.setUserConnectionsManager(mock(UserConnectionsManager.class));
        builder.setGameSessionFactory(mock(GameSessionFactory.class));
        builder.setInfoObserver(mock(Observer.class));

        GameSessionManager result = builder.build();

        assertTrue(result.getClass().isAssignableFrom(GameSessionManager.class));
    }

    @Test
    void build_allDependencies_registerListenersInConnectionManager() {
        BasicGameSessionManagerBuilder builder = new BasicGameSessionManagerBuilder();
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        builder.setUserConnectionsManager(connectionsManager);
        builder.setGameSessionFactory(mock(GameSessionFactory.class));
        builder.setInfoObserver(mock(Observer.class));

        builder.build();

        verify(connectionsManager).addListenerForCreate(any());
        verify(connectionsManager).addListenerForRemove(any());
    }

    @Test
    public void build_withoutConnectionManager_throwsNullPointerException() {
        BasicGameSessionManagerBuilder builder = new BasicGameSessionManagerBuilder();

        assertThrows(NullPointerException.class, () -> builder.build());
    }
}