package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PlayerFactoryTest {
    @Test
    void create_withoutInfoAboutHero_throwsNotFoundException() {
        PlayerFactory heroFactory = new PlayerFactory(new HashMap<>());

        assertThrows(NotFoundException.class, () -> heroFactory.createHero(1, 1));
    }

    @Test
    void create_withInfoAboutTwoHeros_returnCorrectHero() {
        Map<Integer, PlayerFactory.PlayerInfo> heroMap = new HashMap<>();
        heroMap.put(1, new PlayerFactory.PlayerInfo(1, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 5}, new int[]{2, 3, 4, 5}));
        heroMap.put(2, new PlayerFactory.PlayerInfo(2, new int[]{4, 2, 3, 4}, new int[]{4, 2, 3, 5}, new int[]{0, 3, 4, 5}));
        PlayerFactory heroFactory = new PlayerFactory(heroMap);

        Player result = heroFactory.createHero(2, 1);
        Player expected = new Player(2, 1, 20, new int[]{4, 2, 3, 4}, new int[]{4, 2, 3, 5}, new int[]{0, 3, 4, 5});
        assertEquals(expected, result);
    }
}