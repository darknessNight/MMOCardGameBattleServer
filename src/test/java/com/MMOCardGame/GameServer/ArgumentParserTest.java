package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.exceptions.ArgumentParsingException;
import com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ArgumentParserTest {

    @Test
    void parse_preventServerStart_isServerShouldStartShouldReturnFalse() {
        ArgumentParser argumentParser = new ArgumentParser(new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing());
        String args[] = {"--noStartServer"};

        argumentParser.parse(args);

        assertFalse(argumentParser.isServerShouldStart());
    }

    @Test
    void parse_noArgs_isServerShouldStartShouldReturnTrue() {
        ArgumentParser argumentParser = new ArgumentParser(new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing());
        String args[] = {};

        argumentParser.parse(args);

        assertTrue(argumentParser.isServerShouldStart());
    }

    @Test
    void parse_help_isServerShouldStartShouldReturnFalse() {
        ArgumentParser argumentParser = new ArgumentParser(new com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing());
        String args[] = {"--help"};

        argumentParser.parse(args);

        assertFalse(argumentParser.isServerShouldStart());
    }

    @Test
    void parse_overrideSettings_isServerShouldStartShouldReturnTrue() {
        ArgumentParser argumentParser = new ArgumentParser(new AppConfigurationCommandLineParsing());
        String args[] = {"-DuserServerPort=1"};

        argumentParser.parse(args);

        assertTrue(argumentParser.isServerShouldStart());
    }

    @Test
    void parse_overrideSettings_invokeParseMethodOnAppConfig() throws ArgumentParsingException {
        AppConfiguration appConfiguration = mock(AppConfiguration.class);
        ArgumentParser argumentParser = new ArgumentParser(appConfiguration);
        String args[] = {"-DuserServerPort=1"};

        argumentParser.parse(args);

        verify(appConfiguration).initFromConsoleParams(any());
    }
}