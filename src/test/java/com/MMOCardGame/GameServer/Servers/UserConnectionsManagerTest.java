package com.MMOCardGame.GameServer.Servers;

import com.MMOCardGame.GameServer.Authentication.UserTokenAuthentication;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;
import com.MMOCardGame.GameServer.Servers.exceptions.NotFoundConnectionException;
import com.MMOCardGame.GameServer.Servers.exceptions.UserDuplicationException;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserConnectionsManagerTest {
    @Test
    void createUserConnection_correctUserData_returnsNewConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        assertNotNull(connection);
    }

    private UserConnectionsManager getUserConnectionsManager_AcceptEverything() {
        UserTokenAuthentication fakeUserTokenAuthentication = mock(UserTokenAuthentication.class);
        when(fakeUserTokenAuthentication.isCorrectUser(anyString(), anyString())).thenReturn(true);
        return getConnectionsManager(fakeUserTokenAuthentication);
    }

    private UserConnectionsManager getConnectionsManager(UserTokenAuthentication fakeUserTokenAuthentication) {
        return new UserConnectionsManager(fakeUserTokenAuthentication) {
            @Override
            protected UserConnection createImplementationOfConnection(String name) {
                UUID uuid = UUID.randomUUID();
                UserConnection connection = mock(UserConnection.class);
                when(connection.getUserName()).thenReturn(name);
                when(connection.getToken()).thenReturn(uuid);
                return connection;
            }
        };
    }

    @Test
    void createUserConnection_incorrectUserData_throwsAuthenticationException() {
        UserTokenAuthentication fakeUserTokenAuthentication = mock(UserTokenAuthentication.class);
        when(fakeUserTokenAuthentication.isCorrectUser(anyString(), anyString())).thenReturn(true);
        doThrow(AuthenticationException.class).when(fakeUserTokenAuthentication).checkUser(anyString(), anyString());

        UserConnectionsManager userConnectionsManager = getConnectionsManager(fakeUserTokenAuthentication);


        assertThrows(AuthenticationException.class, () -> userConnectionsManager.createConnection("Test", "Test"));
    }

    @Test
    void createUserConnection_existingUserData_throwsUserDuplicationException() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        assertThrows(UserDuplicationException.class, () -> userConnectionsManager.createConnection("Test", "Test"));
    }

    @Test
    void createUserConnection_twoUserData_returnsDifferentConnections() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection firstConnection = userConnectionsManager.createConnection("Test", "Test");
        UserConnection secondConnection = userConnectionsManager.createConnection("Test2", "Test");

        assertNotEquals(firstConnection, secondConnection);
    }

    @Test
    void createUserConnection_twoUserData_returnsConnectionsWithDifferentTokens() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection firstConnection = userConnectionsManager.createConnection("Test", "Test");
        UserConnection secondConnection = userConnectionsManager.createConnection("Test2", "Test");

        assertNotEquals(firstConnection.getToken(), secondConnection.getToken());
    }

    @Test
    void getConnectionForUser_correctName_returnsCorrectConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        UserConnection connection = userConnectionsManager.getConnectionForUser("Test");

        assertEquals("Test", connection.getUserName());
    }

    @Test
    void getConnectionForUser_incorrectName_throwsNotFoundConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        assertThrows(NotFoundConnectionException.class, () -> userConnectionsManager.getConnectionForUser("Other"));
    }

    @Test
    void hasConnectionForUser_correctName_returnsTrue() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        boolean result = userConnectionsManager.hasConnectionForUser("Test");

        assertTrue(result);
    }

    @Test
    void hasConnectionForUser_noExistingUserName_returnsFalse() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        boolean result = userConnectionsManager.hasConnectionForUser("Other");

        assertFalse(result);
    }

    @Test
    void removeConnection_correctName_hasNoConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        userConnectionsManager.removeConnectionForUser("Test");

        assertFalse(userConnectionsManager.hasConnectionForUser("Test"));
    }

    @Test
    void removeConnection_correctName_invokeCloseOnConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        userConnectionsManager.removeConnectionForUser("Test");

        verify(connection).close();
    }

    @Test
    void removeConnection_incorrectName_throwsNotFoundException() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        userConnectionsManager.createConnection("Test", "Test");

        assertThrows(NotFoundConnectionException.class, () -> userConnectionsManager.removeConnectionForUser("Other"));
    }

    @Test
    void addListenerForCreate_createConnection_invokesUpdateOnListener() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);

        userConnectionsManager.addListenerForCreate(userConnectionListener);

        userConnectionsManager.createConnection("Test", "Test");

        verify(userConnectionListener).update(any(UserConnection.class));
    }

    @Test
    void addListenerForCreate_createTwoConnection_invokesUpdateTwoTimes() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);

        userConnectionsManager.addListenerForCreate(userConnectionListener);

        userConnectionsManager.createConnection("Test", "Test");
        userConnectionsManager.createConnection("Test2", "Test");

        verify(userConnectionListener, times(2)).update(any(UserConnection.class));
    }

    @Test
    void addListenerForCreate_tryCreateExistingConnection_invokesUpdateOneTime() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);

        userConnectionsManager.addListenerForCreate(userConnectionListener);

        userConnectionsManager.createConnection("Test", "Test");

        try {
            userConnectionsManager.createConnection("Test", "Test");
        } catch (UserDuplicationException e) {
        }

        verify(userConnectionListener, times(1)).update(any(UserConnection.class));
    }

    @Test
    void removeListenerForCreate_registerAndRemoveOneListenersAndRegisterSecond_invokesUpdateOneTime() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);

        userConnectionsManager.addListenerForCreate(userConnectionListener);
        userConnectionsManager.createConnection("Test", "Test");

        userConnectionsManager.removeListenerForCreate(userConnectionListener);

        userConnectionsManager.createConnection("Test2", "Test");

        verify(userConnectionListener, times(1)).update(any(UserConnection.class));
    }

    @Test
    void addListenerForRemove_removeConnection_invokesMethod() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);
        userConnectionsManager.createConnection("Test", "Test");

        userConnectionsManager.addListenerForRemove(userConnectionListener);
        userConnectionsManager.removeConnectionForUser("Test");

        verify(userConnectionListener).update(any(UserConnection.class));
    }

    @Test
    void addListenerForRemove_removeTwoConnection_invokesMethodTwoTimes() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);
        userConnectionsManager.createConnection("Test", "Test");
        userConnectionsManager.createConnection("Test2", "Test");

        userConnectionsManager.addListenerForRemove(userConnectionListener);

        userConnectionsManager.removeConnectionForUser("Test");
        userConnectionsManager.removeConnectionForUser("Test2");

        verify(userConnectionListener, times(2)).update(any(UserConnection.class));
    }

    @Test
    void addListenerForRemove_tryRemoveNonExistingConnection_noInvoke() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);

        userConnectionsManager.addListenerForRemove(userConnectionListener);

        try {
            userConnectionsManager.removeConnectionForUser("Test");
        } catch (NotFoundConnectionException e) {
        }

        verify(userConnectionListener, times(0)).update(any(UserConnection.class));
    }

    @Test
    void removeListenerForRemove_invokesMethodOneTime() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnectionObserver userConnectionListener = mock(UserConnectionObserver.class);
        userConnectionsManager.createConnection("Test", "Test");
        userConnectionsManager.createConnection("Test2", "Test");

        userConnectionsManager.addListenerForRemove(userConnectionListener);
        userConnectionsManager.removeConnectionForUser("Test");
        userConnectionsManager.removeListenerForRemove(userConnectionListener);
        userConnectionsManager.removeConnectionForUser("Test2");

        verify(userConnectionListener, times(1)).update(any(UserConnection.class));
    }

    @Test
    void reconnectUserAndGetConnection_incorrectUserData_throwsAuthenticationException() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        assertThrows(AuthenticationException.class, () -> userConnectionsManager.reconnectUserAndGetConnection("Test", ""));
    }

    @Test
    void reconnectUserAndGetConnection_nonExistingUser_throwsNotFoundException() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        assertThrows(NotFoundConnectionException.class, () -> userConnectionsManager.reconnectUserAndGetConnection("NonExisting", connection.getToken().toString()));
    }

    @Test
    void reconnectUserAndGetConnection_correctUserData_returnUnpreparedConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        connection.setPrepared(true);

        UserConnection result = userConnectionsManager.reconnectUserAndGetConnection("Test", connection.getToken().toString());

        assertFalse(connection.isPrepared());
    }

    @Test
    void reconnectUserAndGetConnection_correctUserData_returnsConnection() {
        UserConnectionsManager userConnectionsManager = getUserConnectionsManager_AcceptEverything();
        UserConnection connection = userConnectionsManager.createConnection("Test", "Test");

        UserConnection result = userConnectionsManager.reconnectUserAndGetConnection("Test", connection.getToken().toString());

        assertEquals(connection, result);
    }
}