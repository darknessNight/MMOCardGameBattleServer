#!/bin/bash

echo $START_ARG
echo $JVM_ARGS

$GAME_SERVER_DEFAULT_CONF_DIR/copyConfig.sh
java $JVM_ARGS -jar $(find /usr/bin/mmocardgame -name "GameServer-*-jar-with-dependencies.jar") $START_ARG $@